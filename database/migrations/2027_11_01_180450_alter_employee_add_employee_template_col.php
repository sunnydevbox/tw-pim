<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Sunnydevbox\CebuUnitedRebuilders\Models\Employee;

class AlterEmployeeAddEmployeeTemplateCol extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('employees', function($table) {
            $table->integer('employee_template_id')->nullable()->unsigned();

            $table->index('employee_template_id');
            $table->foreign('employee_template_id')
                ->references('id')
                ->on('employee_templates');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('employees', function($table) {
            $table->dropForeign('employees_employee_template_id_foreign');
            $table->dropIndex('employees_employee_template_id_index');

            $table->dropColumn(['employee_template_id']);
        });
    }
}
