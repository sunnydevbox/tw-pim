<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSssDeductionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {
         Schema::create('sss_deductions', function (Blueprint $table) {
             $table->engine = 'InnoDB';

             $table->increments('id')->unsigned();

             $table->decimal('compensation_from', 19, 2)->nullable();
             $table->decimal('compensation_to', 19, 2)->nullable();
             $table->decimal('monthly_salary_credit', 19, 2);
             $table->decimal('social_security_er', 19, 2);
             $table->decimal('social_security_ee', 19, 2);
             $table->decimal('social_security_total', 19, 2)->nullable();
             $table->decimal('ec_er', 19, 2);
             $table->decimal('total_contribution_er', 19, 2);
             $table->decimal('total_contribution_ee', 19, 2);
             $table->decimal('total_contribution_er_ee', 19, 2)->nullable();
             $table->decimal('total_contribution_se_vm_ofw', 19, 2);
             $table->boolean('status')->default(true);

             $table->timestamps();
             $table->softDeletes();
         });
     }

     /**
      * Reverse the migrations.
      *
      * @return void
      */
     public function down()
     {
         Schema::dropIfExists('sss_deductions');
     }
}
