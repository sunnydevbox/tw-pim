<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTimelogImportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('timelog_imports', function($table) {
            $table->increments('id')->unsigned();

            $table->integer('user_id')->unsigned();
            $table->string('status')->nullable()->default('PENDING');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::table('timelogs', function($table) {
            $table->integer('timelog_import_id')->unsigned()->nullable();

            $table->index('timelog_import_id');
            $table->foreign('timelog_import_id')
                ->references('id')
                ->on('timelog_imports');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('timelogs', function($table) {
            $table->dropForeign('timelogs_timelog_import_id_foreign');
            $table->dropIndex('timelogs_timelog_import_id_index');
            $table->dropColumn(['timelog_import_id']);
        });

        Schema::dropIfExists('timelog_imports');
    }
}
