<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Sunnydevbox\CebuUnitedRebuilders\Models\Employee;

class AlterPayrollPayrolllogId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('payrolls', function($table) {
           $table->integer('payroll_log_id')->unsigned()->nullable();

           $table->index('payroll_log_id');
           $table->foreign('payroll_log_id')
            ->references('id')
            ->on('payroll_logs')
            ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('payrolls', function($table) {
            $table->dropForeign('payrolls_payroll_log_id_foreign');
            $table->dropIndex('payrolls_payroll_log_id_index');
            $table->dropColumn(['payroll_log_id']);
        });
    }
}
