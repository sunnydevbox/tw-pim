<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Sunnydevbox\CebuUnitedRebuilders\Models\Employee;

class AlterTimelogsAddCol extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('timelogs', function($table) {
            $table->boolean('is_auto_approve')->nullable()->default(false);
            $table->boolean('is_rest_day')->nullable()->default(false);
            $table->boolean('has_night_differential')->nullable()->default(false);
            $table->integer('holiday_id')->nullable()->unsigned()->default(null);
            $table->index('holiday_id');
            $table->foreign('holiday_id')
                ->references('id')
                ->on('holidays')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('timelogs', function($table) {
            $table->dropForeign('timelogs_holiday_id_foreign');
            $table->dropIndex('timelogs_holiday_id_index');
            $table->dropColumn(['is_auto_approve', 'is_rest_day', 'has_night_differential', 'holiday_id']);
        });
    }
}
