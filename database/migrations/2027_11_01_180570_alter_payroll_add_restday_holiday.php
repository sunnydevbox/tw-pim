<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Sunnydevbox\CebuUnitedRebuilders\Models\Employee;

class AlterPayrollAddRestdayHoliday extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('payrolls', function($table) {
            $table->decimal('total_restday', 19, 4)->nullable()->default(0);
            $table->decimal('total_holiday', 19, 4)->nullable()->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('payrolls', function ($table) {
            $table->dropColumn(['total_restday', 'total_holiday']);
        });
    }
}
