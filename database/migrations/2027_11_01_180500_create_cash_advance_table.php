<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Sunnydevbox\CebuUnitedRebuilders\Models\Employee;

class CreateCashAdvanceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cash_advances', function($table) {
            $table->increments('id')->unsigned();

            $table->integer('employee_id')->unsigned();
            $table->decimal('total_amount', 19, 2)->default(0);
            $table->decimal('paid_amount', 19, 2)->default(0);
            $table->timestamp('freeze_until')->nullable();
            $table->decimal('payable_per_period', 19, 2)->default(0);
            $table->timestamp('duration_start')->nullable();
            $table->timestamp('duration_end')->nullable();
            $table->string('status')->nullable();
            $table->text('notes')->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->index('employee_id');
            $table->foreign('employee_id')
                ->references('id')
                ->on('employees');
        });

        Schema::create('cash_advances_logs', function($table) {
            $table->increments('id')->unsigned();

            $table->integer('cash_advance_id')->unsigned();
            $table->timestamp('payment_date')->nullable();
            $table->text('notes')->nullable();
            $table->timestamps();

            $table->index('cash_advance_id');
            $table->foreign('cash_advance_id')
                ->references('id')
                ->on('cash_advances');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cash_advances', function($table) {
            $table->dropForeign('cash_advances_employee_id_foreign');
            $table->dropIndex('cash_advances_employee_id_index');
        });

        Schema::table('cash_advances_logs', function($table) {
            $table->dropForeign('cash_advances_logs_cash_advance_id_foreign');
            $table->dropIndex('cash_advances_logs_cash_advance_id_index');
        });

        Schema::dropIfExists('cash_advances_logs');
        Schema::dropIfExists('cash_advances');
    }
}
