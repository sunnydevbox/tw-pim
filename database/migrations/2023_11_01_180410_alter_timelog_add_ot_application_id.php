<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Sunnydevbox\CebuUnitedRebuilders\Models\Employee;

class AlterTimelogAddOtApplicationId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('timelogs', function($table) {
            $table->integer('ot_application_id')->unsigned()->nullable();

            $table->index('ot_application_id');
            $table->foreign('ot_application_id')
                ->references('id')
                ->on('leave_applications')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('timelogs', function($table) {
            $table->dropForeign('timelogs_ot_application_id_foreign');
            $table->dropIndex('timelogs_ot_application_id_index');
            $table->dropColumn(['ot_application_id']);
        });
    }
}
