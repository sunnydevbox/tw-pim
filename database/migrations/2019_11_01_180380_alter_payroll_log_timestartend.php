<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Sunnydevbox\CebuUnitedRebuilders\Models\Employee;

class AlterPayrollLogTimestartend extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('payroll_logs', function($table) {
            $table->timestamp('start')->nullable();
            $table->timestamp('end')->nullable();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('payroll_logs', function($table) {
            $table->dropColumn(['start', 'end']);
        });
    }
}
