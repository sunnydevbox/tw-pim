<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Sunnydevbox\CebuUnitedRebuilders\Models\Employee;


class AlterPayrollItemsAddTagCol extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('payroll_items', function($table) {
            $table->string('tag')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('payroll_items', function ($table) {
            $table->dropColumn([
                'tag',
            ]);
        });
    }
}
