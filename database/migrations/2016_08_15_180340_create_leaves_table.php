<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLeavesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('leave_types', function($table) {
            $table->increments('id')->unsigned();

            $table->string('name');
            $table->string('machine_name');
            $table->string('description')->nullable();
            $table->boolean('system')->default(false)->nullable();

            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('leave_applications', function($table) {
            $table->increments('id')->unsigned();

            $table->integer('leave_type_id')->unsigned();
            $table->integer('employee_id')->unsigned();

            $table->text('purpose')->nullable();

            $table->timestampTz('start')->nullable();
            $table->timestampTz('end')->nullable();

            $table->string('status')->default('PENDING')->nullable();

            $table->timestamps();
            $table->softDeletes();

            $table->index('leave_type_id');
            $table->foreign('leave_type_id')
                ->references('id')
                ->on('leave_types');

            $table->index('employee_id');
            $table->foreign('employee_id')
                ->references('id')
                ->on('employees');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('timelogs', function($table) {
            $table->dropForeign('timelogs_timelog_import_id_foreign');
            $table->dropIndex('timelogs_timelog_import_id_index');
            $table->dropColumn(['timelog_import_id']);
        });

        Schema::dropIfExists('timelog_imports');
    }
}
