<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Sunnydevbox\CebuUnitedRebuilders\Models\Employee;

class AlterLeaveapplicationAddCol extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('leave_applications', function($table) {
            $table->integer('duration_in_minutes')->default(0);
            $table->integer('used_in_minutes')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('leave_applications', function($table) {
            $table->dropColumn(['duration_in_minutes', 'used_in_minutes']);
        });
    }
}
