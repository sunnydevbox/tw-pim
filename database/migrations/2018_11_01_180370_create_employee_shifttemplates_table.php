<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Sunnydevbox\CebuUnitedRebuilders\Models\Employee;

class CreateEmployeeShifttemplatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employees_shift_templates', function($table) {
            $table->integer('employee_id')->unsigned();
            $table->integer('shift_template_id')->unsigned();

            $table->index('employee_id');
            $table->foreign('employee_id')
                ->references('id')
                ->on('employees')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->index('shift_template_id');
            $table->foreign('shift_template_id')
                ->references('id')
                ->on('shift_templates')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });

        Schema::table('employees', function($table) {
            $table->dropForeign('employees_shift_template_id_foreign');
            $table->dropIndex('employees_shift_template_id_index');

            $table->dropColumn('shift_template_id');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('employees_shift_templates', function($table) {
            $table->dropForeign('employees_shift_templates_employee_id_foreign');
            $table->dropIndex('employees_shift_templates_employee_id_index');

            $table->dropForeign('employees_shift_templates_shift_template_id_foreign');
            $table->dropIndex('employees_shift_templates_shift_template_id_index');
        });

        Schema::dropIfExists('employees_shift_templates');

        Schema::table('employees', function($table) {
            $table->integer('shift_template_id')->unsigned()->nullable();

            $table->index('shift_template_id');
            $table->foreign('shift_template_id')
                ->references('id')
                ->on('shift_templates')
                ->onUpdate('cascade');
        });

    }
}
