<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Sunnydevbox\CebuUnitedRebuilders\Models\Employee;

class CreatePhilhealthTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('philhealth', function($table) {
            $table->increments('id')->unsigned();

            $table->integer('salary_bracket');
            $table->decimal('salary_range_from', 19, 2)->nullable();
            $table->decimal('salary_range_to', 19, 2)->nullable();
            $table->decimal('salary_base', 19, 2)->default(0)->nullable();
            $table->decimal('monthly_premium', 19, 2)->default(0)->nullable();
            $table->decimal('employee_share', 19, 2)->default(0)->nullable();
            $table->decimal('employer_share', 19, 2)->default(0)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('philhealth');
    }
}
