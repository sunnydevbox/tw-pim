<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddHourlyrateEmployeeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('employees', function($table) {
            $table->decimal('monthly_rate', 19, 2)->nullable()->default(0);
            $table->decimal('hourly_rate', 19, 2)->nullable()->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('employees', function($table) {
            $table->dropColumn(['hourly_rate', 'monthly_rate']);
        });


        Schema::table('payrolls', function($table) {
            $table->dropForeign('payrolls_employee_id_foreign');
            $table->dropIndex('payrolls_employee_id_index');
        });


        Schema::table('payroll_logs', function($table) {
            $table->dropForeign('payroll_logs_payroll_template_id_foreign');
            $table->dropIndex('payroll_logs_payroll_template_id_index');
        });

        Schema::table('employees_payroll_templates', function($table) {
            $table->dropForeign('employees_payroll_templates_employee_id_foreign');
            $table->dropIndex('employees_payroll_templates_employee_id_index');

            $table->dropForeign('employees_payroll_templates_payroll_template_id_foreign');
            $table->dropIndex('employees_payroll_templates_payroll_template_id_index');
        });
        
        Schema::dropIfexists('payroll_items');
        Schema::dropIfexists('payrolls');
        Schema::dropIfexists('payroll_logs');
        Schema::dropIfexists('payroll_templates');
        Schema::dropIfExists('employees_payroll_templates');

    }
}
