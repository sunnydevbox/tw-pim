<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Sunnydevbox\CebuUnitedRebuilders\Models\Employee;

class UpdateCashAdvanceLogAddcolType extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cash_advances_logs', function($table) {
            $table->string('type')->nullable();
            $table->decimal('amount_debited', 19, 2)->nullable();
        });

        Schema::table('cash_advances', function($table) {
            $table->timestamp('approved_on')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cash_advances_logs', function($table) {
            $table->dropColumn(['type', 'amount_debited']);
        });

        Schema::table('cash_advances', function($table) {
            $table->dropColumn(['approved_on']);
        });
    }
}
