<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Sunnydevbox\CebuUnitedRebuilders\Models\Employee;

class AlterPayrollItemsAddCols extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('payroll_items', function($table) {
            
            $table->boolean('is_custom')->default(false);
            $table->decimal('override_total_net', 19, 2)->nullable()->default(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('payroll_items', function($table) {
            $table->dropColumn(['is_custom', 'override_total_net', ]);
        });
    }
}
