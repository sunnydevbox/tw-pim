<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateSuppdataAddEmploymentStatusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employment_status', function(Blueprint $table) {
            $table->increments('id')->unsigned();

            $table->string('name');
            $table->text('description');
            $table->boolean('system')->default(false)->nullable();
        });

        Schema::create('employment_types', function(Blueprint $table) {
            $table->increments('id')->unsigned();

            $table->string('name');
            $table->string('machine_name');
            $table->text('description');
            $table->boolean('system')->default(false)->nullable();
        });

        Schema::table('employees', function($table) {
            $table->integer('employment_status_id')->unsigned()->nullable();
            $table->integer('employment_type_id')->unsigned()->nullable();

            $table->index('employment_status_id');
            $table->index('employment_type_id');

            $table->foreign('employment_status_id')
                ->references('id')
                ->on('employment_status')
                ->onUpdate('cascade');

            $table->foreign('employment_type_id')
                ->references('id')
                ->on('employment_types')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('employees', function($table) {
            $table->dropForeign('employees_employment_status_id_foreign');
            $table->dropIndex('employees_employment_status_id_index');

            $table->dropForeign('employees_employment_type_id_foreign');
            $table->dropIndex('employees_employment_type_id_index');

            $table->dropColumn(['employment_status_id', 'employment_type_id']);
        });

        Schema::dropIfExists('employment_status');
        Schema::dropIfExists('employment_types');
    }
}
