<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Sunnydevbox\CebuUnitedRebuilders\Models\Employee;

class AlterPayrollAddWtaxCol extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('payrolls', function($table) {
            $table->decimal('wtax', 19, 4)->nullable()->default(0);
            $table->decimal('total_adj_earnings', 19, 4)->nullable()->default(0);
            $table->decimal('total_adj_deductions', 19, 4)->nullable()->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('payrolls', function ($table) {
            $table->dropColumn(['wtax', 'total_adj_earnings', 'total_adj_deductions']);
        });
    }
}
