<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePayrollTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payroll_templates', function($table) {
            $table->increments('id')->unsigned();
            $table->string('name');
            $table->string('period_type');
            $table->string('month_days')->nullable();
        });

        Schema::create('payroll_logs', function($table) {
            $table->increments('id')->unsigned();
            $table->integer('payroll_template_id')->unsigned();

            $table->string('status')->nullable()->default('PENDING');
            
            $table->timestamps();

            $table->index('payroll_template_id');
            $table->foreign('payroll_template_id')
                ->references('id')
                ->on('payroll_templates');
        });

        Schema::create('employees_payroll_templates', function($table) {
            $table->integer('employee_id')->unsigned();
            $table->integer('payroll_template_id')->unsigned();

            $table->index('employee_id');
            $table->index('payroll_template_id');

            $table->foreign('employee_id')
                ->references('id')
                ->on('employees')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('payroll_template_id')
                ->references('id')
                ->on('payroll_templates')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });


        Schema::create('payrolls', function($table) {
            $table->increments('id')->unsigned();
            
            $table->integer('employee_id')->unsigned();
            $table->string('company_id', 255)->nullable();
            $table->string('name', 255);
            $table->string('job_position', 255)->nullable();
            $table->string('department', 255)->nullable();

            $table->timestamp('start_date')->nullable();
            $table->timestamp('end_date')->nullable();

            $table->decimal('total_gross', 19, 4)->default(0);
            $table->decimal('total_deduction', 19, 4)->default(0);
            $table->decimal('total_net', 19, 4)->default(0);
            $table->decimal('hourly_rate', 19, 4)->default(0);

            $table->text('data')->nullable();

            $table->string('status')->nullable();

            $table->timestamps();
            $table->softDeletes();


            $table->index('employee_id');
            $table->foreign('employee_id')
                ->references('id')
                ->on('employees')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });


        Schema::create('payroll_items', function($table) {
            $table->increments('id')->unsigned();

            $table->integer('payroll_id')->unsigned();
            $table->string('type')->nullable(); // deduction; earnings
            $table->string('name')->nullable();

            $table->decimal('total_gross', 19, 4)->nullable()->default(0);
            $table->decimal('total_net', 19, 4)->nullable()->default(0);

            $table->timestamps();

            $table->index('payroll_id');
            $table->foreign('payroll_id')
                ->references('id')
                ->on('payrolls')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('payroll_items', function($table) {
            $table->dropForeign('payroll_items_payroll_id_foreign');
            $table->dropIndex('payroll_items_payroll_id_index');
        });


        Schema::table('payrolls', function($table) {
            $table->dropForeign('payrolls_employee_id_foreign');
            $table->dropIndex('payrolls_employee_id_index');
        });


        Schema::table('payroll_logs', function($table) {
            $table->dropForeign('payroll_logs_payroll_template_id_foreign');
            $table->dropIndex('payroll_logs_payroll_template_id_index');
        });

        Schema::table('employees_payroll_templates', function($table) {
            $table->dropForeign('employees_payroll_templates_employee_id_foreign');
            $table->dropIndex('employees_payroll_templates_employee_id_index');

            $table->dropForeign('employees_payroll_templates_payroll_template_id_foreign');
            $table->dropIndex('employees_payroll_templates_payroll_template_id_index');
        });
        
        Schema::dropIfexists('payroll_items');
        Schema::dropIfexists('payrolls');
        Schema::dropIfexists('payroll_logs');
        Schema::dropIfexists('payroll_templates');
        Schema::dropIfExists('employees_payroll_templates');

    }
}
