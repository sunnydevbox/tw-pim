<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTimelogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shift_templates', function($table) {
            $table->increments('id')->unsigned();
            $table->string('name');
            $table->text('description')->nullable();
            $table->string('timezone')->nullable();

            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('shift_template_periods', function($table) {
            $table->increments('id')->unsigned();

            $table->integer('shift_template_id')->unsigned();
            $table->string('name');
            $table->text('description')->nullable();
            $table->string('start');
            $table->string('end');
            $table->integer('grace_period')->nullable()->default(0); // In minutes

            $table->index('shift_template_id');
            $table->foreign('shift_template_id')
                ->references('id')
                ->on('shift_templates')
                ->onDelete('cascade');

            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('timelogs', function(Blueprint $table) {
            $table->increments('id')->unsigned();

            $table->integer('employee_id')->unsigned();
            $table->integer('shift_template_id')->unsigned()->nullable();
            $table->integer('shift_template_period_id')->unsigned()->nullable();

            $table->timestampTz('time_in')->nullable();
            $table->timestampTz('time_out')->nullable();

            // In MINUTES
            $table->integer('time_elapsed')->nullable()->default(0);
            $table->integer('tardy_minutes')->nullable()->default(0);
            $table->integer('billable_minutes')->nullable()->default(0);
            $table->integer('overtime_minutes')->nullable()->default(0);
            $table->boolean('is_overtime_billable')->nullable()->default(false);
            $table->text('notes')->nullable();
            

            $table->string('status', 255)->nullable()->default('PENDING'); // invalid / pending / approved / disapproved

            $table->timestamps();
            $table->softDeletes();

            $table->index('employee_id');
            $table->foreign('employee_id')
                ->references('id')
                ->on('employees')
                ->onDelete('cascade');


            $table->index('shift_template_id');
            $table->foreign('shift_template_id')
                ->references('id')
                ->on('shift_templates')
                ->onDelete('cascade');


            $table->index('shift_template_period_id');
            $table->foreign('shift_template_period_id')
                ->references('id')
                ->on('shift_template_periods');
        });

        Schema::table('employees', function($table)
        {
            $table->integer('shift_template_id')->unsigned()->nullable();

            $table->index('shift_template_id');
            $table->foreign('shift_template_id')
                ->references('id')
                ->on('shift_templates')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('shift_template_periods', function($table) {
            $table->dropForeign('shift_template_periods_shift_template_id_foreign');
            $table->dropIndex('shift_template_periods_shift_template_id_index');
        });

        Schema::table('employees', function($table) {
            $table->dropForeign('employees_shift_template_id_foreign');
            $table->dropIndex('employees_shift_template_id_index');

            $table->dropColumn(['shift_template_id']);
        });

        Schema::table('timelogs', function($table) {
            $table->dropForeign('timelogs_employee_id_foreign');
            $table->dropIndex('timelogs_employee_id_index');

            $table->dropForeign('timelogs_shift_template_id_foreign');
            $table->dropIndex('timelogs_shift_template_id_index');

            $table->dropForeign('timelogs_shift_template_id_foreign');
            $table->dropIndex('timelogs_shift_template_id_index');
        });

        Schema::drop('timelogs');
        Schema::drop('shift_template_periods');
        Schema::drop('shift_templates');
        
    }
}
