<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Sunnydevbox\CebuUnitedRebuilders\Models\Employee;

class CreateTaxTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('table_tax', function($table) {
            $table->increments('id')->unsigned();

            $table->string('compensation_level_period'); // DAILY / WEEKLY / SEMI-MONTHLY / MONTHLY
            $table->decimal('salary_range_from', 19, 2)->nullable();
            $table->decimal('salary_range_to', 19, 2)->nullable();
            $table->decimal('withholding_tax', 19, 2)->nullable();
            $table->decimal('withholding_percentage', 19, 2)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('table_tax');
    }
}
