<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Sunnydevbox\CebuUnitedRebuilders\Models\Employee;

class CreateEmployeeTemplatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employee_templates', function($table) {
            $table->increments('id');
            $table->string('name');
            $table->string('description')->nullable();
            $table->timestamps();
        });

        Schema::create('employee_template_employee', function($table) {
            $table->integer('employee_id')->unsigned();
            $table->integer('employee_template_id')->unsigned();

            $table->index('employee_id');
            $table->index('employee_template_id');
            
            $table->foreign('employee_id')
                ->references('id')
                ->on('employees')
                ->onDelete('cascade');
            $table->foreign('employee_template_id')
                ->references('id')
                ->on('employee_templates')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('employee_template_employee', function($table) {
            $table->dropForeign('employee_template_employee_employee_id_foreign');
            $table->dropIndex('employee_template_employee_employee_id_index');

            $table->dropForeign('employee_template_employee_employee_template_id_foreign');
            $table->dropIndex('employee_template_employee_employee_template_id_index');
        });


        Schema::dropIfExists('employee_template_employee');
        Schema::dropIfExists('employee_templates');
    }
}
