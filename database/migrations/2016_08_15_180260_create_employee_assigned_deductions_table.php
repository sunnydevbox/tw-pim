<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeeAssignedDeductionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employee_assigned_deductions', function(Blueprint $table) {
            $table->increments('id')->unsigned();

            $table->integer('employee_id')->unsigned();
            $table->integer('deduction_id')->unsigned();

            $table->timestamps();


            $table->index('employee_id');
            $table->foreign('employee_id')
                ->references('id')
                ->on('employees')
                ->onUpdate('cascade')
                ->onDelete('cascade')
                ;

            $table->index('deduction_id');
            $table->foreign('deduction_id')
                ->references('id')
                ->on('deductions')
                ->onUpdate('cascade')
                ->onDelete('cascade')
                ;

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('employee_assigned_deductions');
    }
}
