<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHolidaysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('holiday_types', function(Blueprint $table) {
            $table->increments('id')->unsigned();

            $table->string('name');
            $table->text('description')->nullable();
            $table->string('country')->default('PH');
            $table->boolean('system')->default(false)->nullable();
        });

        Schema::create('holidays', function(Blueprint $table) {
            $table->increments('id')->unsigned();

            $table->string('name');
            $table->text('description')->nullable();
            $table->timestampTz('start_date')->nullable();
            $table->timestampTz('end_date')->nullable();
            $table->integer('holiday_type_id')->unsigned();
            $table->string('country')->default('PH');

            $table->index('holiday_type_id');
            $table->foreign('holiday_type_id')
                ->references('id')
                ->on('holiday_types');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('holidays', function($table) {
            $table->dropForeign('holidays_holiday_type_id_foreign');
            $table->dropIndex('holidays_holiday_type_id_index');
        });

        Schema::drop('holiday_types');
        Schema::drop('holidays');
    }
}
