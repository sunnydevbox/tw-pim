<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Sunnydevbox\CebuUnitedRebuilders\Models\Employee;

class AlterTimelogsAddRestdayHolidayCols extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('timelogs', function($table) {
            $table->integer('restday_minutes')->default(0);
            $table->integer('holiday_minutes')->default(0);
            $table->integer('night_differential_minutes')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('timelogs', function ($table) {
            $table->dropColumn(['restday_minutes', 'holiday_minutes', 'night_differential_minutess']);
        });
    }
}
