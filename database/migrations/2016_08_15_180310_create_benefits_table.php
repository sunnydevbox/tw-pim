<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBenefitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('benefits', function($table) {
            $table->increments('id')->unsigned();
            $table->string('name');
            $table->string('machine_name')->nullable();
            $table->text('description')->nullable();

            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('employee_benefits', function($table) {
            $table->integer('employee_id')->unsigned();
            $table->integer('benefit_id')->unsigned();

            $table->index('employee_id');
            $table->foreign('employee_id')
                ->references('id')
                ->on('employees')
                ->onDelete('cascade');

            $table->index('benefit_id');
            $table->foreign('benefit_id')
                ->references('id')
                ->on('benefits')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('employee_benefits', function($table) {
            $table->dropForeign('employee_benefits_employee_id_foreign');
            $table->dropIndex('employee_benefits_employee_id_index');
            $table->dropForeign('employee_benefits_benefit_id_foreign');
            $table->dropIndex('employee_benefits_benefit_id_index');
        });

        Schema::drop('employee_benefits');
        Schema::drop('benefits');
    }
}
