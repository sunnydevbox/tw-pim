<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Sunnydevbox\CebuUnitedRebuilders\Models\Employee;


class AlterPayrollAddBasicPayCols extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('payrolls', function($table) {
            $table->decimal('basic_pay', 19, 4)->nullable()->default(0);
            $table->decimal('daily_rate', 19, 4)->nullable()->default(0);
            $table->decimal('total_hours', 19, 4)->nullable()->default(0);
            $table->decimal('total_days', 19, 4)->nullable()->default(0);
            $table->decimal('total_undertime', 19, 4)->nullable()->default(0);
            
            $table->decimal('total_holiday_pay', 19, 4)->nullable()->default(0);
            $table->decimal('total_paid_leaves_pay', 19, 4)->nullable()->default(0);
            $table->decimal('total_overtime_pay', 19, 4)->nullable()->default(0);
            $table->decimal('total_overtime', 19, 4)->nullable()->default(0);
            $table->decimal('total_restday_pay', 19, 4)->nullable()->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('payrolls', function ($table) {
            $table->dropColumn([
                'basic_pay',
                'daily_rate',
                'total_hours',
                'total_days',
                'total_undertime',
                'total_holiday_pay',
                'total_paid_leaves_pay',
                'total_overtime_pay',
                'total_overtime',
                'total_restday_pay',
            ]);
        });
    }
}
