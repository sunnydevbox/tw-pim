<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Sunnydevbox\CebuUnitedRebuilders\Models\Employee;

class CreateLeaveCreditsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employee_leave_credits', function($table) {
            $table->increments('id')->unsigned();

            $table->integer('employee_id')->unsigned();
            $table->integer('leave_type_id')->unsigned();
            $table->float('amount')->unsigned();
            $table->text('notes');

            $table->timestamps();
            $table->softDeletes();
            
            $table->index('employee_id');
            $table->foreign('employee_id')
                ->references('id')
                ->on('employees')
                ->onUpdate('cascade');

            $table->index('leave_type_id');
            $table->foreign('leave_type_id')
                ->references('id')
                ->on('leave_types')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('employee_leave_credits', function($table) {
            $table->dropForeign('employee_leave_credits_employee_id_foreign');
            $table->dropIndex('employee_leave_credits_employee_id_index');

            $table->dropForeign('employee_leave_credits_leave_type_id_foreign');
            $table->dropIndex('employee_leave_credits_leave_type_id_index');
        });

        Schema::dropIfExists('employee_leave_credits');
    }
}
