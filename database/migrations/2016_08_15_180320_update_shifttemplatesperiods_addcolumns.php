<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateShifttemplatesperiodsAddcolumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('shift_template_periods', function($table) {
            $table->boolean('day_m')->nullable()->default(1);
            $table->boolean('day_t')->nullable()->default(1);
            $table->boolean('day_w')->nullable()->default(1);
            $table->boolean('day_th')->nullable()->default(1);
            $table->boolean('day_f')->nullable()->default(1);
            $table->boolean('day_s')->nullable()->default(0);
            $table->boolean('day_sun')->nullable()->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('shift_template_periods', function($table) {
            $table->dropColumn(['day_m', 'day_t', 'day_w', 'day_th',  'day_f', 'day_s', 'day_sun']);
        });
    }
}
