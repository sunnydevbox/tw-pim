<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Sunnydevbox\CebuUnitedRebuilders\Models\Employee;

class AlterPayrollLogAddPayCoverageCol extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('payroll_logs', function($table) {
            $table->timestamp('pay_coverage_start')->nullable();
            $table->timestamp('pay_coverage_end')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('payroll_logs', function ($table) {
            $table->dropColumn(['pay_coverage_start', 'pay_coverage_end']);
        });
    }
}
