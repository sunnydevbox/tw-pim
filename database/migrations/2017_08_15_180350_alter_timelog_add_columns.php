<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTimelogAddColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('timelogs', function($table) {
            $table->timestamp('is_approved')->nullable();
            $table->boolean('is_payroll_generated')->nullable()->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('timelogs', function($table) {
            $table->dropColumn(['is_approved', 'is_payroll_generated']);
        });
    }
}
