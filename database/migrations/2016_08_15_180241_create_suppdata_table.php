<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSuppdataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('company_departments', function(Blueprint $table) {
            $table->increments('id')->unsigned();

            $table->string('name');
            $table->text('description');
            $table->boolean('system')->default(false)->nullable();

        });

        Schema::create('civil_status', function(Blueprint $table) {
            $table->increments('id')->unsigned();

            $table->string('name');
            $table->text('description');
            $table->boolean('system')->default(false)->nullable();

        });

        Schema::create('job_positions', function(Blueprint $table) {
            $table->increments('id')->unsigned();

            $table->string('name');
            $table->text('description');
            $table->boolean('system')->default(false)->nullable();

        });

        Schema::create('titles', function(Blueprint $table) {
            $table->increments('id')->unsigned();

            $table->string('name');
            $table->boolean('system')->default(false)->nullable();

        });

        Schema::create('tax_status', function(Blueprint $table) {
            $table->increments('id')->unsigned();

            $table->string('name');
            $table->text('description');
            $table->boolean('system')->default(false)->nullable();

        });

        Schema::create('deductions', function(Blueprint $table) {
            $table->increments('id')->unsigned();

            $table->string('name');
            $table->string('machine_name')->nullable();
            $table->text('description');
            $table->string('country')->nullable();
            $table->string('region')->nullable();
            $table->boolean('system')->default(false)->nullable();

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('company_departments');
        Schema::drop('civil_status');
        Schema::drop('job_positions');
        Schema::drop('titles');
        Schema::drop('tax_status');
        Schema::drop('deductions');
    }
}
