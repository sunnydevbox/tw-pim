<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeeLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employee_logs', function(Blueprint $table) {
            $table->increments('id')->unsigned();

            $table->integer('actorable_id')->unsigned()->nullable();
            $table->string('actorable_type')->nullable();
            
            $table->integer('employee_id')->unsigned();
            
            $table->text('description')->nullable();
            $table->timestamps();

            $table->index('employee_id');
            $table->foreign('employee_id')
                ->references('id')
                ->on('employees')
                ->onDelete('cascade');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('employee_logs', function($table) 
        {
            $table->dropForeign('employee_logs_employee_id_foreign');
            $table->dropIndex('employee_logs_employee_id_index');

        });

        Schema::drop('employee_logs');
    }
}
