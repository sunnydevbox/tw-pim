<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Sunnydevbox\CebuUnitedRebuilders\Models\Employee;

class AlterLeaveApplicationsAddHolidayTypeCol extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('leave_applications', function($table) {
            $table->integer('holiday_id')->unsigned()->nullable();
            $table->index('holiday_id');
            $table->foreign('holiday_id')
                ->references('id')
                ->on('holidays');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('leave_applications', function ($table) {
            $table->dropForeign('leave_applications_holiday_id_foreign');
            $table->dropIndex('leave_applications_holiday_id_index');
            $table->dropColumn(['holiday_id',]);
        });
    }
}
