<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Sunnydevbox\CebuUnitedRebuilders\Models\Employee;

class AlterPayrolllogAddTriggeredColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('payroll_logs', function($table) {
           $table->timestamp('triggered')->nullable();
           $table->integer('total_payroll_count')->default(0);
           $table->integer('total_payroll_completed_count')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('payroll_logs', function($table) {
            $table->dropColumn(['triggered', 'total_payroll_count', 'total_payroll_completed_count']);
        });
    }
}
