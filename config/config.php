<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Table name
    |--------------------------------------------------------------------------
    |
    | table name
    |
    */
    'table'=>'employees',

    /*
    |--------------------------------------------------------------------------
    | Employee Model
    |--------------------------------------------------------------------------
    |
    | Developer can override this with his class that extends 
    | the main Employee Model
    |
    */
    'models' => [
        'employee'              => \Sunnydevbox\TWPim\Models\Employee::class,
        'employee-leave-credit' => \Sunnydevbox\TWPim\Models\EmployeeLeaveCredit::class,
        'employee-log'          => \Sunnydevbox\TWPim\Models\EmployeeLog::class,
        // 'employee-template'     => \Sunnydevbox\TWPim\Models\EmployeeTemplate::class, // DEPRECATED
        'holiday'               => \Sunnydevbox\TWPim\Models\Holiday::class,
        'holiday-type'          => \Sunnydevbox\TWPim\Models\HolidayType::class,
        'shift-template'        => \Sunnydevbox\TWPim\Models\ShiftTemplate::class,
        'shift-template-period' => \Sunnydevbox\TWPim\Models\ShiftTemplatePeriod::class,
        'deduction'             => \Sunnydevbox\TWPim\Models\Deduction::class,
        'employee-deduction'    => \Sunnydevbox\TWPim\Models\EmployeeDeduction::class,
        'timelog'               => \Sunnydevbox\TWPim\Models\Timelog::class,
        'timelog-import'        => \Sunnydevbox\TWPim\Models\TimelogImport::class,
        'leave-type'            => \Sunnydevbox\TWPim\Models\LeaveType::class,
        'leave-application'     => \Sunnydevbox\TWPim\Models\LeaveApplication::class,
        'payroll'               => \Sunnydevbox\TWPim\Models\Payroll::class,
        'payroll-item'          => \Sunnydevbox\TWPim\Models\PayrollItem::class,
        'payroll-template'      => \Sunnydevbox\TWPim\Models\PayrollTemplate::class,
        'payroll-log'           => \Sunnydevbox\TWPim\Models\PayrollLog::class,
        'benefit'               => \Sunnydevbox\TWPim\Models\Benefit::class,
        'system-variable'       => \Sunnydevbox\TWPim\Models\SystemVariable::class,

    ],

    'model_deduction'           => \Sunnydevbox\TWPim\Models\Deduction::class,
    'model_payroll'             => \Sunnydevbox\TWPim\Models\Payroll::class,
    'model_payroll_item'        => \Sunnydevbox\TWPim\Models\PayrollItem::class,
    'model_civil_status'        => \Sunnydevbox\TWPim\Models\CivilStatus::class,
    'model_employee'            => \Sunnydevbox\TWPim\Models\Employee::class, 
    'model_employee_template'   => \Sunnydevbox\TWPim\Models\EmployeeTemplate::class,

    'model_philhealth'          => \Sunnydevbox\TWPim\Models\Philhealth::class,
    'model_table_tax'           => \Sunnydevbox\TWPim\Models\TableTax::class,

    'model_cash_advance'        => \Sunnydevbox\TWPim\Models\CashAdvance::class,
    'model_cash_advance_log'     => \Sunnydevbox\TWPim\Models\CashAdvanceLog::class,

    'repository_cash_advance_log'     => \Sunnydevbox\TWPim\Repositories\Deduction\CashAdvanceLogRepository::class,

    /*
    |--------------------------------------------------------------------------
    | Employee Role
    |--------------------------------------------------------------------------
    |
    | Default role to assign to new employee records
    |
    */
    'role' => 'employee',

    /*
    |--------------------------------------------------------------------------
    | Employee ID Generator
    |--------------------------------------------------------------------------
    |
    | ID Generator class
    |
    | Developers can override his with their own but extending 
    | the Abstract class provided
    |
    */
    'id_generator' => \Sunnydevbox\TWPIM\Services\EmployeeIDService::class,

  
    /*
    |--------------------------------------------------------------------------
    | Employee ID Generator
    |--------------------------------------------------------------------------
    |
    | ID Generator class
    |
    | Developers can override his with their own but extending 
    | the Abstract class provided
    |
    */
    'allowed_advanced_hours' => 1,

];