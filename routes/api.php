<?php


if (app()->getProvider('\Dingo\Api\Provider\LaravelServiceProvider'))
{
	$api = app('Dingo\Api\Routing\Router');

	$api->version('v1', ['middleware' => []], function ($api) {
		// $api->post('payrolls/get-payslip', 					'\Sunnydevbox\TWPim\Http\Controllers\API\V1\PayrollController@generatePayslip')->name('payroll.generate-payslip');
		$api->get('payrolls/get-payslip', 					'\Sunnydevbox\TWPim\Http\Controllers\API\V1\PayrollController@generatePayslip')->name('payroll.generate-payslip');
	});

	$api->version('v1', ['middleware' => ['api.auth']], function ($api) {

		$api->resource('company-departments', 				'\Sunnydevbox\TWPim\Http\Controllers\API\V1\CompanyDepartmentController');
		$api->resource('civil-status', 						'\Sunnydevbox\TWPim\Http\Controllers\API\V1\CivilStatusController');
		$api->resource('job-positions', 					'\Sunnydevbox\TWPim\Http\Controllers\API\V1\JobPositionController');
		$api->resource('titles', 							'\Sunnydevbox\TWPim\Http\Controllers\API\V1\TitleController');
		$api->resource('tax-status', 						'\Sunnydevbox\TWPim\Http\Controllers\API\V1\TaxStatusController');
		$api->resource('deductions', 						'\Sunnydevbox\TWPim\Http\Controllers\API\V1\DeductionController');
		$api->resource('employment-status', 				'\Sunnydevbox\TWPim\Http\Controllers\API\V1\EmploymentStatusController');
		$api->resource('employment-types', 					'\Sunnydevbox\TWPim\Http\Controllers\API\V1\EmploymentTypeController');

		/** ASSIGN/REMOVE ROLES/PERMISSIONS */
		$api->post('employees/{id}/assign-roles', 			'\Sunnydevbox\TWPim\Http\Controllers\API\V1\EmployeeController@assignRoles')->name('employee.assignroles');
		$api->post('employees/{id}/remove-role', 			'\Sunnydevbox\TWPim\Http\Controllers\API\V1\EmployeeController@removeRole')->name('employee.removerole');
		$api->get('employees/{id}/roles', 					'\Sunnydevbox\TWPim\Http\Controllers\API\V1\EmployeeController@getRoles')->name('employee.getroles');
		$api->post('employees/attach-benefit', 				'\Sunnydevbox\TWPim\Http\Controllers\API\V1\EmployeeController@attachBenefit')->name('employee.attach-benefit');
		$api->post('employees/detach-benefit', 				'\Sunnydevbox\TWPim\Http\Controllers\API\V1\EmployeeController@detachBenefit')->name('employee.detach-benefit');
		$api->post('employees/attach-deduction', 			'\Sunnydevbox\TWPim\Http\Controllers\API\V1\EmployeeController@attachDeduction')->name('employee.attach-deduction');
		$api->post('employees/detach-deduction', 			'\Sunnydevbox\TWPim\Http\Controllers\API\V1\EmployeeController@detachDeduction')->name('employee.detach-deduction');
		$api->post('employees/{id}/attach-shift-template', 	'\Sunnydevbox\TWPim\Http\Controllers\API\V1\EmployeeController@attachShiftTemplate')->name('employee.attach-shift-template');
		$api->post('employees/{id}/detach-shift-template', 	'\Sunnydevbox\TWPim\Http\Controllers\API\V1\EmployeeController@detachShiftTemplate')->name('employee.detach-shift-template');
		$api->post('employees/{id}/attach-payroll-template','\Sunnydevbox\TWPim\Http\Controllers\API\V1\EmployeeController@attachPayrollTemplate')->name('employee.attach-payroll-template');
		$api->post('employees/{id}/detach-payroll-template','\Sunnydevbox\TWPim\Http\Controllers\API\V1\EmployeeController@detachPayrollTemplate')->name('employee.detach-payroll-template');

		$api->resource('benefits', 							'\Sunnydevbox\TWPim\Http\Controllers\API\V1\BenefitController');
		$api->resource('employee-benefits', 				'\Sunnydevbox\TWPim\Http\Controllers\API\V1\EmployeeBenefitController');

		$api->resource('employees', 						'\Sunnydevbox\TWPim\Http\Controllers\API\V1\EmployeeController');
		$api->resource('employee-logs', 					'\Sunnydevbox\TWPim\Http\Controllers\API\V1\EmployeeLogController');
		$api->post('employee-deductions/attach-deduction', 	'\Sunnydevbox\TWPim\Http\Controllers\API\V1\EmployeeDeductionController@attachDeduction')->name('employees.attach-deduction');
		$api->post('employee-deductions/detach-deduction', 	'\Sunnydevbox\TWPim\Http\Controllers\API\V1\EmployeeDeductionController@detachDeduction')->name('employees.detach-deduction');
		$api->resource('employee-deductions', 				'\Sunnydevbox\TWPim\Http\Controllers\API\V1\EmployeeDeductionController');

		$api->resource('employee-templates', 				'\Sunnydevbox\TWPim\Http\Controllers\API\V1\EmployeeTemplateController');

		$api->post('holidays/month-list', 					'\Sunnydevbox\TWPim\Http\Controllers\API\V1\HolidayController@monthList');
		$api->resource('holidays', 							'\Sunnydevbox\TWPim\Http\Controllers\API\V1\HolidayController');
		$api->resource('holiday-types', 					'\Sunnydevbox\TWPim\Http\Controllers\API\V1\HolidayTypeController');

		$api->resource('shift-templates',        			'\Sunnydevbox\TWPim\Http\Controllers\API\V1\ShiftTemplateController');
		$api->get('shift-templates/employee/{id}',        	'\Sunnydevbox\TWPim\Http\Controllers\API\V1\ShiftTemplateController@perEmployee');
		$api->resource('shift-template-periods', 			'\Sunnydevbox\TWPim\Http\Controllers\API\V1\ShiftTemplatePeriodController');
		$api->get('shift-template-periods/employee/{id}', 	'\Sunnydevbox\TWPim\Http\Controllers\API\V1\ShiftTemplatePeriodController@perEmployee');

		$api->post('timelogs/import', 						'\Sunnydevbox\TWPim\Http\Controllers\API\V1\TimelogController@import')->name('timelog.importer');
		$api->post('timelogs/recalculate/{id}', 			'\Sunnydevbox\TWPim\Http\Controllers\API\V1\TimelogController@recalculate')->name('timelog.recalculate');
		$api->post('timelogs/{id}/{action}', 				'\Sunnydevbox\TWPim\Http\Controllers\API\V1\TimelogController@setStatus')->name('timelog.set-status');
		$api->resource('timelogs', 							'\Sunnydevbox\TWPim\Http\Controllers\API\V1\TimelogController');

		$api->post('leave-applications/{id}/{action}', 		'\Sunnydevbox\TWPim\Http\Controllers\API\V1\LeaveApplicationController@setStatus')->name('leave-application.set-status');
		$api->resource('leave-applications', 				'\Sunnydevbox\TWPim\Http\Controllers\API\V1\LeaveApplicationController');
		$api->resource('leave-applications', 				'\Sunnydevbox\TWPim\Http\Controllers\API\V1\LeaveApplicationController');
		$api->resource('leave-types', 						'\Sunnydevbox\TWPim\Http\Controllers\API\V1\LeaveTypeController');

		$api->post('payrolls/get-payslip', 					'\Sunnydevbox\TWPim\Http\Controllers\API\V1\PayrollController@generatePayslip')->name('payroll.generate-payslip');
		$api->post('payrolls/recalculate', 					'\Sunnydevbox\TWPim\Http\Controllers\API\V1\PayrollController@recalculate')->name('payroll-recalculate');
		
		$api->resource('payrolls', 							'\Sunnydevbox\TWPim\Http\Controllers\API\V1\PayrollController');
		$api->post('payroll-items/custom', 					'\Sunnydevbox\TWPim\Http\Controllers\API\V1\PayrollItemController@customEntry')->name('payroll-item.custom');
		$api->resource('payroll-items', 					'\Sunnydevbox\TWPim\Http\Controllers\API\V1\PayrollItemController');
		
		$api->post('payroll-templates/period-selections', 	'\Sunnydevbox\TWPim\Http\Controllers\API\V1\PayrollTemplateController@periodSelections')->name('payroll-template.period-selections');
		$api->resource('payroll-templates', 				'\Sunnydevbox\TWPim\Http\Controllers\API\V1\PayrollTemplateController');
		
		$api->post('payroll-logs/generate', 				'\Sunnydevbox\TWPim\Http\Controllers\API\V1\PayrollLogController@generate')->name('payroll-log.generate');
		$api->resource('payroll-logs', 						'\Sunnydevbox\TWPim\Http\Controllers\API\V1\PayrollLogController');

		$api->resource('employee-leave-credits', 			'\Sunnydevbox\TWPim\Http\Controllers\API\V1\EmployeeLeaveCreditController');
		$api->resource('system-variables', 					'\Sunnydevbox\TWPim\Http\Controllers\API\V1\SystemVariableController');
		
		$api->resource('sss-deduction', 					'\Sunnydevbox\TWPim\Http\Controllers\API\V1\SssDeductionController');
		$api->resource('philhealth', 						'\Sunnydevbox\TWPim\Http\Controllers\API\V1\PhilhealthController');
		
		$api->post('cash-advances/set-status', 				'\Sunnydevbox\TWPim\Http\Controllers\API\V1\CashAdvanceController@setStatus')->name('cash-advances.set-status');
		$api->resource('cash-advances', 					'\Sunnydevbox\TWPim\Http\Controllers\API\V1\CashAdvanceController');
		$api->resource('cash-advance-logs', 				'\Sunnydevbox\TWPim\Http\Controllers\API\V1\CashAdvanceLogController');

		$api->post('table-tax/get-range', 					'\Sunnydevbox\TWPim\Http\Controllers\API\V1\TableTaxController@getRange')->name('table-tax.get-range');
		$api->resource('table-tax', 						'\Sunnydevbox\TWPim\Http\Controllers\API\V1\TableTaxController');

		$api->get('reports/payrolls', 						'\Sunnydevbox\TWPim\Http\Controllers\API\V1\ReportController@getPayrolls')->name('report.get-payrolls');
	});
}
