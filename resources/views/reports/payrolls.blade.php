<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <style>
        * {
            font-size:11px;
        }
        table {
            width: 100%;
        }
        table td {
            padding: 0 5px;
            vertical-align: top;
        }
        .borderless td, .borderless th {
            border: none !important;
        }
        /* table {
            border-collapse: collapse;
            width: 100%;
        }
        table, th, td {
            padding: .3rem;
        }
        .header-title {
            text-align: center;
        }
        .header-title h3 {
            margin-top: 2rem;
            margin-bottom: .5rem;
        }
        .header-title h4 {
            margin: .5rem;
        }
        .table-right {
            text-align: right;
        }
        .table-center {
            text-align: center;
        }
        .table-border-top {
            border-top: 2px solid black;
        }
        .table-border-left {
            border-left: 2px solid black;
        }
        ul li {
            list-style-type: none;
        }
        .main-container {
            margin: 4rem;
        } */
    </style>
    <link href="./vendor/tw-pim/assets/bootstrap.min.css" rel="stylesheet" />
    <!-- <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet" /> -->
    <!-- <link href="/vendor/tw-pim/assets/bootstrap.min.css" rel="stylesheet" /> -->
</head>
<body class="small">
    <div class="fluid-container main-containe">
        <div class="text-center">
            <h4>United Rebuilders, Inc.</h4>
            <h5>www.unitedrebuilders.com</h5>
        </div>
        <div class="row">
            <div class="col-sm-12" *ngIf="payrolls && payrolls.length">

                <table class="table table-bordered">
                <thead>
                    <tr>
                    <td colspan="4" class="text-center bg-1">Active</td>
                    <td colspan="3" class="text-center bg-2 border-right border-bottom">Basic &amp; SL/VL</td>
                    <td colspan="3" class="text-center bg-1 border-bottom">O.T. / R.D.</td>
                    <td colspan="2"></td>
                    <td colspan="3" class="text-center bg-1">Deduction</td>
                    <td colspan="4">Employer's Cont.</td>
                    <td></td>
                    <td></td>
                    </tr>
                    <tr>
                    <th>Employee</th>
                    <th>Rate (Php)</th>
                    <th>UT&LA</th>
                    <th>Days</th>
                    <th>Base (Php)</th>
                    <th>Hol. (Php)</th>
                    <th class="border-right">SL/VL (Php)</th>
                    <th>Hr.</th>
                    <th>OT (Php)</th>
                    <th>RD (Php)</th>
                    <th>Grosspay</th>
                    <th>Adjustments</th>
                    <th>URI</th>
                    <th>SSS</th>
                    <th>Pagibig</th>
                    <th>SSS</th>
                    <th>PHIC</th>
                    <th>Pagibig</th>
                    <th>Tax</th>
                    <th>Net Due</th>
                    <th>SSS</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($payrolls as $payroll)

                        <tr>
                            <td>{{ $payroll->name }}</td>
                            <td>{{ $payroll->daily_rate }} {{ $payroll->hourly_rate }}</td>
                            <td>{{ $payroll->total_undertime }}</td>
                            <td>{{ $payroll->total_days }}</td>
                            <td>
                                {{ $payroll->basic_pay }}
                            </td>
                            <td>{{ $payroll->total_holiday_pay }}</td>
                            <td class="border-right">SL/VL (Php)</td>
                            <td>
                                {{ $payroll->total_overtime }}  
                            </td>
                            <td>
                                {{ $payroll->total_overtime_pay }}
                            </td>
                            <td>{{ $payroll->total_restday_pay }}</td>
                            <td>{{ $payroll->total_gross }}</td>
                            <td>{{ $payroll->total_adj_earnings }}</td>
                            <td>{{ $payroll->total_adj_deductions }}</td>
                            <td>
                                {{ $payroll->sss }}
                            </td>
                            <td>
                                {{ $payroll->pag_ibig }}
                            </td>
                            <td>
                                {{ $payroll->sss_employer }}
                            </td>
                            <td>
                                {{ $payroll->philhealth }}
                            </td>
                            <td>
                                {{ $payroll->pag_ibig_employer }}
                            </td>
                            <td>
                                {{ $payroll->tax }}
                            </td>
                            <td>{{ $payroll->total_net }}</td>
                            <td>SSS</td>
                        </tr>
                    @endforeach
                </tbody>
                </table>
            </div>
        </div>
    </div>
</body>
</html>