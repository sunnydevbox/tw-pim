<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <style>
        * {
            font-size:11px;
        }
        table {
            width: 100%;
        }
        table td {
            padding: 0 5px;
            vertical-align: top;
        }
        .borderless td, .borderless th {
            border: none !important;
        }
        /* table {
            border-collapse: collapse;
            width: 100%;
        }
        table, th, td {
            padding: .3rem;
        }
        .header-title {
            text-align: center;
        }
        .header-title h3 {
            margin-top: 2rem;
            margin-bottom: .5rem;
        }
        .header-title h4 {
            margin: .5rem;
        }
        .table-right {
            text-align: right;
        }
        .table-center {
            text-align: center;
        }
        .table-border-top {
            border-top: 2px solid black;
        }
        .table-border-left {
            border-left: 2px solid black;
        }
        ul li {
            list-style-type: none;
        }
        .main-container {
            margin: 4rem;
        } */
    </style>
    <link href="./vendor/tw-pim/assets/bootstrap.min.css" rel="stylesheet" />
    <!-- <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet" /> -->
    <!-- <link href="/vendor/tw-pim/assets/bootstrap.min.css" rel="stylesheet" /> -->
</head>
<body class="small">
    <div class="fluid-container main-containe">
        <div class="text-center">
            <h4>United Rebuilders, Inc.</h4>
            <h5>www.unitedrebuilders.com</h5>
        </div>
        <table class="tabl">
            <tbody>
                <tr>
                    <td style="width:70%">
                        <!-- PAYROLL DETAILS -->
                        <table class="borderless">
                            <tbody>
                                <tr>
                                    <td>Payslip Period:</td>
                                    <td>
                                        {{ $payroll->payroll_log->start->format('M d') }} - {{ $payroll->payroll_log->end->format('M d') }}
                                    </td>
                                </tr>
                                <tr>
                                    <td>Pay Date: </td>
                                    <td>
                                        @if($payroll->payroll_log->pay_date)
                                            {{ $payroll->payroll_log->pay_date->format('Y-m-d') }}
                                        @else
                                            <i><strong class="text-danger">Not Specified</strong></i>
                                        @endif
                                    </td>
                                </tr>
                                <tr>
                                    <td>Pay Coverage: </td>
                                    <td>
                                        @if($payroll->payroll_log->pay_coverage_start && $payroll->payroll_log->pay_coverage_end)
                                            {{ $payroll->payroll_log->pay_coverage_start->format('M d') }} - {{ $payroll->payroll_log->pay_coverage_end->format('M d') }}
                                        @else
                                            <i><strong class="text-danger">Not Specified</strong></i>
                                        @endif
                                    </td>
                                </tr>
                                <tr>
                                    <td>Name: </td>
                                    <td>{{ $payroll->name }}</td>
                                </tr>
                            </tbody>
                        </table>
                        <hr/>
                        <!-- EMPLOYEE DETAILS -->
                        <table class="borderless">
                            <tbody>
                                <tr>
                                    <td style="width:50%">Emp# {{ $payroll->employee->employee_id_number }}  Bio# {{ $payroll->employee->bio_id }}</td>
                                    <td>Position: {{ $payroll->employee->job_position->name }}</td>
                                </tr>
                                <tr>
                                    <td>Civil Status: 
                                        @if ($payroll->employee->civil_status)
                                            {{ $payroll->employee->civil_status->name }}
                                        @else 
                                            <i><strong class="text-danger">Not Specified</strong></i>
                                        @endif
                                    </td>
                                    <td>Rate: $15000</td>
                                </tr>
                            </tbody>
                        </table>
                        <hr/>
                        <!-- BREAKDONW ITEMS -->
                        <table>
                            <tbody>
                                <tr>
                                    <td style="width:50%;">
                                        <!-- EARNINGS -->
                                        <table class="borderless">
                                            <tbody>
                                                <!-- @foreach($payroll->items->where('type', $payroll->items[0]::TYPE_EARNING) as $item)
                                                    <tr>
                                                        <td style="width:50%;">{{ $item->name }}</td>
                                                        <td class="text-right">{{ $item->total_net }}</td>
                                                    </tr>
                                                @endforeach -->
                                                <tr>
                                                    <td style="width:50%;"><strong>GROSS</strong></td>
                                                    <td class="text-right"><strong><u>{{ $payroll->total_gross }}</u></strong></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        <hr/>
                                        <table class="borderless">
                                            <tbody>
                                                <!-- @foreach($payroll->items->where('type', $payroll->items[0]::TYPE_ADJ_EARNING) as $item)
                                                    <tr>
                                                        <td style="width:50%;">{{ $item->name }}</td>
                                                        <td class="text-right">{{ $item->total_net }}</td>
                                                    </tr>
                                                @endforeach -->
                                                <tr>
                                                    <td style="width:50%;"><strong>ADJUSTED EARNINGS</strong></td>
                                                    <td class="text-right"><strong><u>{{ number_format($payroll->total_adj_earnings, 2, '.', ',') }}</u></strong></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    <td>
                                        <!-- DEDUCTIONS -->
                                        <table class="borderless">
                                            <tbody>
                                                @foreach($payroll->items->where('type', $payroll->items[0]::TYPE_DEDUCTION) as $item)
                                                    <tr>
                                                        <td style="width:50%;">{{ $item->name }}</td>
                                                        <td class="text-right">{{ $item->total_net }}</td>
                                                    </tr>
                                                @endforeach
                                                <tr>
                                                    <td style="width:50%;">Withholding Tax</td>
                                                    <td class="text-right">{{ $payroll->wtax }}</td>
                                                </tr>
                                                <tr>
                                                    <td style="width:50%;"><strong>DEDUCTIONS</strong></td>
                                                    <td class="text-right"><strong><u>{{ number_format($payroll->total_deduction, 2, '.', ',') }}</u></strong></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        <hr/>
                                        <table class="borderless">
                                            <tbody>
                                                @foreach($payroll->items->where('type', $payroll->items[0]::TYPE_ADJ_DEDUCTION) as $item)
                                                    <tr>
                                                        <td style="width:50%;">{{ $item->name }}</td>
                                                        <td class="text-right">{{ $item->total_net }}</td>
                                                    </tr>
                                                @endforeach
                                                <tr>
                                                    <td style="width:50%;"><strong>ADJUSTED DEDUCTIONS</strong></td>
                                                    <td class="text-right"><strong><u>{{ number_format($payroll->total_adj_deductions, 2, '.', ',') }}</u></strong></td>
                                                </tr>
                                            </tbody>
                                        </table>    

                                    
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <hr/>
                        <!-- NET -->
                        <table>
                            <tbody>
                                <tr>
                                    <td class="text-right"><strong>NET PAY:</strong></td>
                                    <td><strong><u>{{ number_format($payroll->total_net, 2, '.', ',') }}</u></strong></td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                    <td class="col-md-">
                        
                        <table class="table">
                            <tbody>
                                <tr>
                                    <td>
                                        Payslip# {{ $payroll->id }}
                                    </td>
                                </tr>
                                <tr>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td class="text-center">
                                        RECEIVED BY:

                                        <br/>
                                        <br/>
                                        <br/>
                                        {{ $payroll->name }}
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
            </tbody>
        </table>


    </div>
</body>
</html>