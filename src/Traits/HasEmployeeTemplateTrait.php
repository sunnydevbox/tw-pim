<?php
namespace Sunnydevbox\TWPim\Traits;

trait HasEmployeeTemplateTrait 
{
    /**
     * Deprecated
     * ALIAS to employeeTemplate.
     */
    public function template()
    {
        return $this->employee_template();
        // return $this->belongsToMany(config('tw-pim.models.employee-template'), 'employee_template_employee');
    }


    public function employee_template()
    {
        return $this->belongsTo(config('tw-pim.model_employee_template'), 'employee_template_id');
    }
}