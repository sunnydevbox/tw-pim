<?php
namespace Sunnydevbox\TWPim\Traits;


trait HasEmploymentStatusModelTrait
{
    /** 
     * SOON TO DEPRECATE
     */
    public function status()
    {
        return $this->belongsTo('\Sunnydevbox\TWPim\Models\EmploymentStatus',  'employment_status_id');
    }

    /**
     * Alias to status()
     */
    public function employment_status()
    {
        return $this->status();
    }

    public function scopeEmploymentStatus($query, $status_id = null)
    {
        if ($status_id) {
            $query->where('employment_status_id', $status_id);
        }
    }

    public function scopeActive($query)
    {
        // 2 - Active
        $this->scopeEmploymentStatus($query, 2);
    }
}