<?php
namespace Sunnydevbox\TWPim\Traits;

trait HasEmployeelTemplateTrait 
{
    public function template()
    {
        return $this->belongsToMany(config('tw-pim.models.employee-template'), 'employee_template_employee');
    }
}