<?php
namespace Sunnydevbox\TWPim\Traits;

use Carbon\Carbon;

trait HasTimelogPostProcessTrait
{
    /*** NIGHT DIFFERENTIALS */
    
    public function hasNightShiftDifferential()
    {
        $date = $this->time_in->copy();


        $nightDifferentialStart = $date->hour(22)->minute(00);
        $nightDifferentialEnd = $nightDifferentialStart->copy()->addHours(8);
        
        // FORMULA: (StartDate1 <= EndDate2) and (StartDate2 <= EndDate1)
        // dd($this->time_in, $nightDifferentialEnd, '&&', $this->time_out, $nightDifferentialStart);

        if (
            ($nightDifferentialStart->between($this->time_in, $this->time_out) 
            || 
            $nightDifferentialEnd->between($this->time_in, $this->time_out) 
            ||
            $this->time_in->between($nightDifferentialStart, $nightDifferentialEnd))
        ) {


        // if (($this->time_in->lte($nightDifferentialEnd)) && ($this->time_out->lte($nightDifferentialStart))) {
            return true;
        }

        return false;
    }


    // public function getEmployeeStatusAttribute()
    // {
    //     return $this->timelogs()->where(functoin($query) {
    //         $query->invalid())
    //     ->get();
    // }
}