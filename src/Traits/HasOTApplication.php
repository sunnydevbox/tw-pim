<?php
namespace Sunnydevbox\TWPim\Traits;

trait HasOTApplication 
{
    public function otApplication()
    {
        return $this->belongsTo(config('tw-pim.models.leave-application'), 'ot_application_id');
    }

    public function hasOvertimeApplication()
    {
        return ($this->ot_application_id) ? true : false;
    }
}