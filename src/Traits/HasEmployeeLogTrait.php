<?php
namespace Sunnydevbox\TWPim\Traits;

trait HasEmployeeLogTrait
{
    public function logs()
    {
        return $this->morphMany(\Sunnydevbox\TWPim\Models\EmployeeLog::class, 'actorable');
    }
}