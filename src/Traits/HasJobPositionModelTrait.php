<?php
namespace Sunnydevbox\TWPim\Traits;


trait HasJobPositionModelTrait
{
    public function job_position()
    {
        return $this->belongsTo('\Sunnydevbox\TWPim\Models\JobPosition');
    }

    public function scopeJobPosition($query, $position_id = null)
    {
        if ($position_id) {
            $query->where('job_position_id', $position_id);
        }
    }
}