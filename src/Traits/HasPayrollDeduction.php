<?php
namespace Sunnydevbox\TWPim\Traits;


trait HasPayrollDeduction
{
    public function getSssAttribute()
    {
        $value = $this->items->where('name', 'SSS')->first();

        if ($value) {
            return $value->total_net;
        }

        return null;
    }


    public function getPagIbigAttribute()
    {
        $value = $this->items->where('name', 'Pag-ibig')->first();

        if ($value) {
            return $value->total_net;
        }

        return null;
    }
    
    public function getPhilhealthAttribute()
    {
        $value = $this->items->where('name', 'Philhealth')->first();

        if ($value) {
            return $value->total_net;
        }

        return null;
    }

    public function getTaxAttribute()
    {
        $value = $this->items->where('name', 'Tax')->first();

        if ($value) {
            return $value->total_net;
        }

        return null;
    }

    public function getSssEmployerAttribute()
    {
        $value = $this->items->where('name', 'SSS')->first();

        if ($value) {
            return $value->data['employer_share'];
        }

        return null;
    }

    public function getPagIbigEmployerAttribute()
    {
        $value = $this->items->where('name', 'Pag-ibig')->first();

        if ($value) {
            return $value->data['employer_share'];
        }

        return null;
    }
}