<?php
namespace Sunnydevbox\TWPim\Traits;

trait HasEmployeeTrait 
{
    public function employee()
    {
        return $this->belongsTo(config('tw-pim.models.employee'));
    }
}