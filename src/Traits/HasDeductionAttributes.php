<?php
namespace Sunnydevbox\TWPim\Traits;

use Carbon\Carbon;

trait HasDeductionAttributes
{
    public function timelogs()
    {
        return $this->hasMany(config('tw-pim.models.timelog'));
    }


    public function getTotalPendingAttribute()
    {
        return $this->timelogs()->pending()->count();
    }

    public function getTotalInvalidAttribute()
    {
        return $this->timelogs()->invalid()->count();
    }

    public function getTotalApprovedAttribute()
    {
        return $this->timelogs()->approved()->count();
    }

    public function getTotalCloseAttribute()
    {
        return $this->timelogs()->close()->count();
    }

    public function getTotalClosedAttribute()
    {
        return $this->getTotalCloseAttribute();
    }

    // public function getEmployeeStatusAttribute()
    // {
    //     return $this->timelogs()->where(functoin($query) {
    //         $query->invalid())
    //     ->get();
    // }
}