<?php
namespace Sunnydevbox\TWPim\Traits;

trait HasTimelogImportTrait 
{
    public function timelogImport()
    {
        return $this->hasMany(\Sunnydevbox\TWPim\Models\TimelogImport::class);
    }

    // public function __construct(array $attributes = []) {
    //     parent::__construct($attributes);

    //     $this->fillable[] = 'timelog_import_id';
    // }
}