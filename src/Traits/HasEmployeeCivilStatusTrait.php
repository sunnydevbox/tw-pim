<?php
namespace Sunnydevbox\TWPim\Traits;

trait HasEmployeeCivilStatusTrait 
{
    public function civil_status()
    {
        return $this->belongsTo(config('tw-pim.model_civil_status'));
    }
}