<?php
namespace Sunnydevbox\TWPim\Traits;

trait HasShiftTemplate 
{
    // DEPRECATED
    public function shiftTemplate()
    {
        return $this->belongsTo(config('tw-pim.models.shift-template'), 'shift_template_id');
    }

    // DEPRECATED
    public function shiftTemplates()
    {
        return $this->shift_templates();
    }

    public function shift_templates()
    {
        return $this->belongsToMany(config('tw-pim.models.shift-template'), 'employees_shift_templates');
    }

    public function __construct(array $attributes = []) {
        parent::__construct($attributes);

        $this->fillable[] = 'shift_template_id';
    }
}