<?php
namespace Sunnydevbox\TWPim\Traits;

trait HasEmployeeDeductionTrait 
{
    public function deductions()
    {
        return $this->belongsToMany(config('tw-pim.models.deduction'), 'employee_assigned_deductions', 'employee_id', 'deduction_id');
    }
}