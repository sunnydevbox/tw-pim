<?php
namespace Sunnydevbox\TWPim\Traits;

trait HasPayrollTemplateTrait 
{
    public function payroll_templates()
    {
        return $this->belongsToMany(config('tw-pim.models.payroll-template'), 'employees_payroll_templates');
    }
}