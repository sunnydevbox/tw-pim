<?php
namespace Sunnydevbox\TWPim\Traits;

use Dingo\Api\Http\Request;

trait HasShiftTemplateControllerTrait 
{

    public function attachShiftTemplate($id, Request $request)
    {
        $result = $this->dataService->attachShiftTemplate($id, $request->get('shift_template_id'));
    }

    public function detachShiftTemplate($id, Request $request)
    {
        $result = $this->dataService->detachShiftTemplate($id, $request->get('shift_template_id'));
    }
}