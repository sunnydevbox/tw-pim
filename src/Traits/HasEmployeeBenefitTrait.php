<?php
namespace Sunnydevbox\TWPim\Traits;

trait HasEmployeeBenefitTrait 
{
    public function benefits()
    {
        return $this->belongsToMany(config('tw-pim.models.benefit'), 'employee_benefits', 'employee_id', 'benefit_id');
    }
}