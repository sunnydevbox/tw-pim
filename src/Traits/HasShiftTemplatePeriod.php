<?php
namespace Sunnydevbox\TWPim\Traits;

trait HasShiftTemplatePeriod
{
    /**
     * DEPRECATED
     */
    public function shiftTemplatePeriod()
    {
        return $this->shift_template_period();
    }

    public function shift_template_period()
    {
        return $this->belongsTo(config('tw-pim.models.shift-template-period'), 'shift_template_period_id');
    }
}