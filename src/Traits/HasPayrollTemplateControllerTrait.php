<?php
namespace Sunnydevbox\TWPim\Traits;

use Dingo\Api\Http\Request;

trait HasPayrollTemplateControllerTrait 
{

    public function attachPayrollTemplate($id, Request $request)
    {
        $result = $this->dataService->attachPayrollTemplate($id, $request->get('payroll_template_id'));
    }

    public function detachPayrollTemplate($id, Request $request)
    {
        $result = $this->dataService->detachPayrollTemplate($id, $request->get('payroll_template_id'));
    }
}