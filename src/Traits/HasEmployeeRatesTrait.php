<?php
namespace Sunnydevbox\TWPim\Traits;

trait HasEmployeeRatesTrait
{   
    /*
    A Regular Day (basic daily rate = monthly rate x number of months in a year (12) / total working days in a year)
    A Special Day (130% x basic daily rate)
    A Special Day, which is also a scheduled Rest Day (150% x basic daily rate)
    A Regular Holiday (200% x basic daily rate)
    A Regular Holiday, which is also a scheduled Rest Day (260% x basic daily rate)
*/
    /**
     * A Regular Day (basic daily rate = monthly rate x number of months in a year (12) / total working days in a year)
     */
    public function getBasicDailyRateAttribute()
    {
        // A Regular Day (
            /* basic daily rate = 
                        ($this->monthyl_rate * 12) / total working days in a year)
            */
    }

    /**
     * A Special Day (130% x basic daily rate)
     */
    public function getSpecialDayRateAttribute()
    {

        // A Special Day (130% x basic daily rate)
        // 130% must be in a global variable
        /*
            $this->getBasicDailyRateAttribute() * 1.3
        */
        return $this->getBasicDailyRateAttribute() * 1.3;
    }


}