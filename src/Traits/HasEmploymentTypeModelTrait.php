<?php
namespace Sunnydevbox\TWPim\Traits;


trait HasEmploymentTypeModelTrait
{
    /** 
     * SOON TO DEPRECATE
     */
    public function type()
    {
        return $this->belongsTo('\Sunnydevbox\TWPim\Models\EmploymentType',  'employment_type_id');
    }

    /**
     * Alias to type()
     */
    public function employment_type()
    {
        return $this->type();
    }

    public function scopeEmploymentType($query, $employment_type_id = null)
    {
        if ($employment_type_id) {
            $query->where('employment_type_id', $employment_type_id);
        }
    }
}