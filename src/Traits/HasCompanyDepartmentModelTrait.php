<?php
namespace Sunnydevbox\TWPim\Traits;


trait HasCompanyDepartmentModelTrait
{
    public function department()
    {
        return $this->belongsTo('\Sunnydevbox\TWPim\Models\CompanyDepartment');
    }

    public function scopeDepartment($query, $department_id = null)
    {
        if ($department_id) {
            $query->where('department_id', $department_id);
        }
    }
}