<?php
namespace Sunnydevbox\TWPim\Traits;

trait HasLeaveApplication 
{
    /** DEPRECATE */
    public function leaveApplications()
    {
        return $this->leave_applications();
    }

    public function leave_applications()
    {
        return $this->hasMany(config('tw-pim.models.leave-application'));
    }
}