<?php
namespace Sunnydevbox\TWPim\Observers;

use Sunnydevbox\TWPim\Models\PayrollItem;
use Sunnydevbox\TWPim\Events\PayrollItemCreatedEvent;
use Sunnydevbox\TWPim\Events\PayrollItemUpdatedEvent;

class PayrollItemObserver
{
    // creating, created, updating, updated, saving, saved, deleting, deleted,  restoring, restored

    /**
     * Listen to the User created event.
     *
     * @param  User  $user
     * @return void
     */
    public function created(PayrollItem $payrollItem)
    {
        $payrollItem = $payrollItem->refresh();
        $payrollItem->total_net = $this->adjustment($payrollItem);
        
        $payrollItem->save();

        event(new PayrollItemCreatedEvent($payrollItem->payroll_id));
    }

    public function creating(PayrollItem $payrollItem)
    {
    }

    /**
     * Listen to the User deleting event.
     *
     * @param  Employee  $employee
     * @return void
     */
    public function deleting(PayrollItem $payrollItem)
    {
    }

    /**
     * Listen to the User deleting event.
     *
     * @param  Employee  $employee
     * @return void
     */
    public function deleted(PayrollItem $payrollItem)
    {
        event(new PayrollItemUpdatedEvent($payrollItem->payroll_id));
    }

    public function updating(PayrollItem $payrollItem) 
    {
        $payrollItem->total_net = $this->adjustment($payrollItem);
    }

    public function updated(PayrollItem $payrollItem)
    {   
        event(new PayrollItemUpdatedEvent($payrollItem->payroll_id));
    }

    public function saving(PayrollItem $payrollItem) 
    {
        
    }

    public function adjustment($payrollItem)
    {
        $value = null;

        if ($payrollItem->is_custom) {
            
            $value = 0;
            $reference = $payrollItem->data['reference'];
        
            if ($reference) {

                if (is_object($reference)) {

                    $reference = $reference->toArray();
                    $reference = $reference['id'];

                }
            }

            if (is_numeric($reference)) {
                if ($payrollItem->data['multiplier'] == 'percentage') {

                    $value = $payrollItem->data['value'] * $payrollItem->find($reference)->total_net;

                } else if ($payrollItem->data['multiplier'] == 'flat') {


                    $value =  $payrollItem->data['value'];
                }
            } else {    
                $value = $payrollItem->data['value'];
            }
            
            // if ($payrollItem->type == 'ADJ_DEDUCTION') {
            //     $value = 0 - abs($value);
            // }
        } 
        else {
            $value = $payrollItem->total_net;
        }
        
        return $value;
    }
}