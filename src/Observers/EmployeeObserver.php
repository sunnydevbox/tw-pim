<?php
namespace Sunnydevbox\TWPim\Observers;

use Sunnydevbox\TWPim\Models\Employee;

class EmployeeObserver
{
    // creating, created, updating, updated, saving, saved, deleting, deleted,  restoring, restored

    /**
     * Listen to the User created event.
     *
     * @param  User  $user
     * @return void
     */
    public function created( $employee)
    {   
        // _dd($employee);
        $action = 'CREATED';
        if (count($employee->getDirty())) {
            $this->buildData('Employee', $action, $employee);
        }

        event(new \Sunnydevbox\TWPim\Events\EmployeeCreatedEvent($employee));
    }

    /**
     * Listen to the User deleting event.
     *
     * @param  Employee  $employee
     * @return void
     */
    public function deleting(Employee $employee)
    {
        event(new \Sunnydevbox\TWPim\Events\EmployeeDeletingEvent($employee));

    }

    /**
     * Listen to the User deleting event.
     *
     * @param  Employee  $employee
     * @return void
     */
    public function deleted(Employee $employee)
    {
        $this->buildData(request('type'), 'Employee DELETED ' . \Carbon\Carbon::createFromTimestamp(strtotime($employee->deletd_at))->format('Y-m-d H:i'), $employee);
        event(new \Sunnydevbox\TWPim\Events\EmployeeDeletedEvent($employee));
    }

    public function updated(Employee $employee) 
    {
        $action = 'UPDATED';
        if (count($employee->getDirty())) {
            $this->buildData(request('type'), $action, $employee);
        }

        event(new \Sunnydevbox\TWPim\Events\EmployeeUpdatedEvent($employee));        
    }

    public function saved(Employee $employee) 
    {
        //echo 2;
    }

    private function buildData($type, $action, $employee) 
    {
        $description = join(', ', collect($employee->getDirty())->keys()->all());
        event(new \Sunnydevbox\TWPim\Events\EmployeeLogEvent(ucfirst($type)  . ' ' . $action, $employee, $description));
    }
}