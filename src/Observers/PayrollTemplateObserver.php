<?php
namespace Sunnydevbox\TWPim\Observers;

use Sunnydevbox\TWPim\Models\PayrollTemplate;

class PayrollTemplateObserver
{
    // creating, created, updating, updated, saving, saved, deleting, deleted,  restoring, restored

    /**
     * Listen to the User created event.
     *
     * @param  User  $user
     * @return void
     */
    public function created(PayrollTemplate $payrollTemplate)
    { }

    /**
     * Listen to the User deleting event.
     *
     * @param  Employee  $payrollTemplate
     * @return void
     */
    public function deleting(PayrollTemplate $payrollTemplate)
    { }

    /**
     * Listen to the User deleting event.
     *
     * @param  Employee  $payrollTemplate
     * @return void
     */
    public function deleted(PayrollTemplate $payrollTemplate)
    { }

    public function updated(PayrollTemplate $payrollTemplate) 
    { }

    public function saved(PayrollTemplate $payrollTemplate) 
    { }

    public function saving(PayrollTemplate $payrollTemplate) 
    {
        // $payrollTemplate->month_days = collect($payrollTemplate->month_days)->reject(function($v, $k)
        // {
        //     if (!is_numeric($v) || (is_numeric($v) &&  $v > 32)) {
        //         throw new \Exception('invalid_payroll_template_day');
        //     }
        //     // Reject anything that is not numeric
        //     return !is_numeric($v);
        // })->all();
    }
}