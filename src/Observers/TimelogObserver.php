<?php
namespace Sunnydevbox\TWPim\Observers;

use Sunnydevbox\TWPim\Models\Timelog;

class TimelogObserver
{
    // creating, created, updating, updated, saving, saved, deleting, deleted,  restoring, restored

    public function created(Timelog $timelog)
    {   
        if ($timelog->isDirty('time_in') || $timelog->isDirty('time_out')) {
            event(new \Sunnydevbox\TWPim\Events\TimelogEntryEvent($timelog, 'saving'));
        }
    }

    public function deleting(Timelog $timelog)
    {
        
    }

    public function deleted(Timelog $timelog)
    {
    
    }

    public function updated(Timelog $timelog) 
    {
        if ($timelog->isDirty('time_in') || $timelog->isDirty('time_out')) {
            \Log::info('TL updating');
            event(new \Sunnydevbox\TWPim\Events\TimelogEntryEvent($timelog, 'updating'));
        }
    }

    public function saved(Timelog $timelog)
    {
        
    }

    public function saving(Timelog $timelog) 
    {

    }
}