<?php
namespace Sunnydevbox\TWPim\Observers;

use Sunnydevbox\TWPim\Models\Holiday;

class HolidayObserver
{
    // creating, created, updating, updated, saving, saved, deleting, deleted,  restoring, restored

    /**
     * Listen to the User created event.
     *
     * @param  User  $user
     * @return void
     */
    public function created(Holiday $holiday)
    {   
    }

    /**
     * Listen to the User deleting event.
     *
     * @param  Employee  $employee
     * @return void
     */
    public function deleting(Holiday $holiday)
    {
        // event(new \Sunnydevbox\TWPim\Events\EmployeeDeletingEvent($employee));
    }

    /**
     * Listen to the User deleting event.
     *
     * @param  Employee  $employee
     * @return void
     */
    public function deleted(Holiday $holiday)
    {
    
    }

    public function updated(Holiday $holiday) 
    {

    }

    public function saving(Holiday $holiday) 
    {
        if (!$holiday->end_date) {
            $holiday->end_date = $holiday->start_date;
        }
    }
}