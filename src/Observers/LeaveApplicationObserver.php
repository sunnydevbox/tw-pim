<?php
namespace Sunnydevbox\TWPim\Observers;

use Sunnydevbox\TWPim\Models\LeaveApplication;
use Carbon\Carbon;

class LeaveApplicationObserver
{
    // creating, created, updating, updated, saving, saved, deleting, deleted,  restoring, restored

    /**
     * Listen to the User created event.
     *
     * @param  User  $user
     * @return void
     */
    public function creating(LeaveApplication $leaveApplication)
    {
        $leaveApplication->duration_in_minutes = $leaveApplication->start->diffInMinutes($leaveApplication->end);
    }

    public function updating(LeaveApplication $leaveApplication)
    {
        $leaveApplication->duration_in_minutes = $leaveApplication->start->diffInMinutes($leaveApplication->end);
    }

    /**
     * Listen to the User deleting event.
     *
     * @param  Employee  $employee
     * @return void
     */
    public function deleting(LeaveApplication $leaveApplication)
    {}

    /**
     * Listen to the User deleting event.
     *
     * @param  Employee  $employee
     * @return void
     */
    public function deleted(LeaveApplication $leaveApplication)
    {}

    public function updated(LeaveApplication $leaveApplication) 
    {}

    public function saving(LeaveApplication $leaveApplication) 
    {}
}