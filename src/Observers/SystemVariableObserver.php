<?php
namespace Sunnydevbox\TWPim\Observers;

use Sunnydevbox\TWPim\Models\SystemVariable;

class SystemVariableObserver
{
    // creating, created, updating, updated, saving, saved, deleting, deleted,  restoring, restored

    public function created(SystemVariable $systemVariable)
    {   

    }
    

    public function updated(SystemVariable $systemVariable) 
    {

    }

    public function saved(SystemVariable $systemVariable)
    {
        
    }

    public function saving(SystemVariable $systemVariable) 
    {
        if (is_array($systemVariable->value)) {
            $systemVariable->value = serialize($systemVariable->value);
        }
    }
}