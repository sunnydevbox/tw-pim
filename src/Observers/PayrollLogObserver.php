<?php
namespace Sunnydevbox\TWPim\Observers;

use Sunnydevbox\TWPim\Models\PayrollLog;

class PayrollLogObserver
{
    // creating, created, updating, updated, saving, saved, deleting, deleted,  restoring, restored

    /**
     * Listen to the User created event.
     *
     * @param  User  $user
     * @return void
     */
    public function created(PayrollLog $payrollLog)
    {
    }

    public function creating(PayrollLog $payrollLog)
    {
        $payrollLog->status = PayrollLog::STATUS_PENDING;
    }

    /**
     * Listen to the User deleting event.
     *
     * @param  Employee  $employee
     * @return void
     */
    public function deleting(PayrollLog $payrollLog)
    {
        // event(new \Sunnydevbox\TWPim\Events\EmployeeDeletingEvent($employee));
    }

    /**
     * Listen to the User deleting event.
     *
     * @param  Employee  $employee
     * @return void
     */
    public function deleted(PayrollLog $payrollLog) { }

    public function updated(PayrollLog $payrollLog) { }

    public function saving(PayrollLog $payrollLog) {}
}