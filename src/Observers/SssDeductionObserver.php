<?php
namespace Sunnydevbox\TWPim\Observers;

use Sunnydevbox\TWPim\Models\SssDeduction;

class SssDeductionObserver
{
    // creating, created, updating, updated, saving, saved, deleting, deleted,  restoring, restored

    /**
     * Listen to the User created event.
     *
     * @param  User  $user
     * @return void
     */
    public function creating(SssDeduction $sssDeduction) { 

        $sssDeduction->social_security_total = $sssDeduction->social_security_er + $sssDeduction->social_security_ee;
        $sssDeduction->total_contribution_er_ee = $sssDeduction->total_contribution_er + $sssDeduction->total_contribution_ee;

        // $sssDeduction->save();

    }

    /**
     * Listen to the User deleting event.
     *
     * @param  SssDeduction sssDeduction
     * @return void
     */
    public function deleting(SssDeduction $sssDeduction) { }

    /**
     * Listen to the User deleting event.
     *
     * @param  SssDeduction sssDeduction
     * @return void
     */
    public function deleted(SssDeduction $sssDeduction) { }

    public function updated(SssDeduction $sssDeduction) { }


    public function updating(SssDeduction $sssDeduction) 
    {
        $sssDeduction->social_security_total = $sssDeduction->social_security_er + $sssDeduction->social_security_ee;
        $sssDeduction->total_contribution_er_ee = $sssDeduction->total_contribution_er + $sssDeduction->total_contribution_ee;
     }

    public function saved(SssDeduction $sssDeduction) 
    { 

    }

}