<?php
namespace Sunnydevbox\TWPim\Observers;

use Sunnydevbox\TWPim\Models\CashAdvance;

class CashAdvanceObserver
{
    // creating, created, updating, updated, saving, saved, deleting, deleted,  restoring, restored

    /**
     * Listen to the User created event.
     *
     * @param  User  $user
     * @return void
     */
    public function created(CashAdvance $cashAdvance)
    {   
        $cashAdvance = $cashAdvance->fresh();
        $cashAdvance->status = $cashAdvance::STATUS_PENDING;

        $cashAdvance->save();

        event(new \Sunnydevbox\TWPim\Events\CashAdvanceCreatedEvent($cashAdvance));
    }

    /**
     * Listen to the User deleting event.
     *
     * @param  CashAdvance  $cashAdvance
     * @return void
     */
    public function deleting(CashAdvance $cashAdvance) { }

    /**
     * Listen to the User deleting event.
     *
     * @param  CashAdvance  $cashAdvance
     * @return void
     */
    public function deleted(CashAdvance $cashAdvance) { }

    public function updated(CashAdvance $cashAdvance) { }


    public function updating(CashAdvance $cashAdvance) 
    {
        /**
         * IF status was changed to 'APPROVED', 
         * then populate approved_on
         */
    
        if ($cashAdvance->getOriginal()['status'] != $cashAdvance::STATUS_APPROVED && 
            $cashAdvance->getAttributes()['status'] == $cashAdvance::STATUS_APPROVED) {
                // dd($cashAdvance::STATUS_APPROVED, $cashAdvance->getAttributes(), $cashAdvance->getOriginal());
                $cashAdvance->approved_on = \Carbon\Carbon::now();
        }
    }

    public function saved(CashAdvance $cashAdvance) { }

}