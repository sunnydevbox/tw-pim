<?php

namespace Sunnydevbox\TWPim\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Illuminate\Support\Facades\Artisan;

class PublishConfigCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'twpim:publish-config';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Publish config files';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('Publishing config files');
        $exitCode = Artisan::call('vendor:publish', [
            '--provider'    => 'Sunnydevbox\TWPim\TWPimServiceProvider',
            '--tag'        => 'config',
        ]);
        $this->info('...DONE');
    }

    public function fire()
    {
        echo 'fire';
    }
}
