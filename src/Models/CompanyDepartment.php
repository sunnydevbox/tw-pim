<?php
namespace Sunnydevbox\TWPim\Models;

use \Sunnydevbox\TWCore\Models\BaseModel;
use \Sunnydevbox\TWPim\Traits\HasEmployeeTrait;

class CompanyDepartment extends BaseModel
{
    use HasEmployeeTrait;

    protected $table = 'company_departments';
    
    public $timestamps = false;

    protected $fillable = [
        'name',
        'description',
        'system',
    ];
}