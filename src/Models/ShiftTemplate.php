<?php
namespace Sunnydevbox\TWPim\Models;

use \Sunnydevbox\TWCore\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class ShiftTemplate extends BaseModel
{
    use SoftDeletes;

    protected $table = 'shift_templates';
    
    protected $fillable = [
        'name',
        'description',
        'timezone',
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public function periods()
    {
        return $this->hasMany(config('tw-pim.models.shift-template-period'));
    }

    // DEPRECATED
    public function employees_()
    {
        return $this->belongsTo(config('tw-pim.models.employee'), 'shift_template_id', 'id');
    }

    public function employees()
    {
        return $this->belongsToMany(config('tw-pim.models.employee'), 'employees_shift_templates');
    }
}
