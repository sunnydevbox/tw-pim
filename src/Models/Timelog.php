<?php
namespace Sunnydevbox\TWPim\Models;

use \Sunnydevbox\TWCore\Models\BaseModel;
use \Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;
use Sunnydevbox\TWPim\Traits\HasEmployeeTrait;
use Sunnydevbox\TWPim\Traits\HasShiftTemplate;
use Sunnydevbox\TWPim\Traits\HasShiftTemplatePeriod;
use Sunnydevbox\TWPim\Traits\HasTimelogImportTrait;
use Sunnydevbox\TWPim\Traits\HasOTApplication;
use Sunnydevbox\TWPim\Traits\HasTimelog;
use Sunnydevbox\TWPim\Traits\HasTimelogPostProcessTrait;

class Timelog extends BaseModel
{
    use HasEmployeeTrait;
    use HasShiftTemplate;
    use HasShiftTemplatePeriod;
    use HasTimelogImportTrait;
    use HasOTApplication;
    use HasTimelogPostProcessTrait;
    use SoftDeletes;

    protected $table = 'timelogs';

    // timelog has invalid data and cannot be processed completey
    const STATUS_INVALID = 'INVALID'; 

    // Timelog is completely processed and pending user's (HR) approval
    const STATUS_PENDING = 'PENDING';

    // Timelog is validated by user (HR)
    const STATUS_VALID = 'VALID';
     
    // Disregard from further processing
    const STATUS_CLOSE = 'CLOSE';

    // Disregard from further processing
    const STATUS_APPROVED = 'APPROVED';
    
    protected $fillable = [
        'employee_id',
        'shift_template_id',
        'shift_template_period_id',
        'time_in',
        'time_out',
        'time_elapsed',
        'tardy_minutes',
        'billable_minutes',
        'overtime_minutes',
        'notes',
        'status',
        'is_overtime_billable',
        'ot_application_id',
        'is_approved',
        'timelog_import_id',
        'is_payroll_generated',
        'is_auto_approve',
        'is_rest_day',
        'holiday_id',
        'has_night_differential',
        'restday_minutes',
        'holiday_minutes',
        'night_differential_minutes',
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at',
        'shift_template_id',
    ];

    /** RELATIONSHIPS ***/

    public function leave_application()
    {
        return $this->belongsTo(config('tw-pim.models.leave-application'), 'ot_application_id');
    }


    public function holiday()
    {
        return $this->belongsTo(config('tw-pim.models.holiday'));
    }

    /*** GETTERS/SETTERS ***/

    public function getTimeInAttribute()
    {
        if ($this->attributes['time_in']) {
            return Carbon::parse($this->attributes['time_in']);
        }

        return null;
    }

    public function getTimeOutAttribute()
    {
        if ($this->attributes['time_out']) {
            return Carbon::parse($this->attributes['time_out']);
        }
        
        return null;
    }

    public function getIsRestDayAttribute()
    {
        return $this->attributes['is_rest_day'] ? true : false;
    }

    public function setTimeInAttribute($value)
    {
        
        $value = $value ? Carbon::parse($value)->format('Y-m-d H:i:s') : null;
        $this->attributes['time_in'] = $value;
    }

    public function setTimeOutAttribute($value)
    {
        $value = $value ? Carbon::parse($value)->format('Y-m-d H:i:s') : null;
        $this->attributes['time_out'] = $value;
    }

    public function getIsApprovedAttribute()
    {
        if (!$this->attributes['is_approved']) {
            return false;
        }

        return Carbon::parse($this->attributes['is_approved']);
    }

    public function setIsApprovedAttribute($value)
    {
        $newValue = null;
        
        if ($value) {
            $newValue = Carbon::now();
        }

        if ($value === false) {
            $newValue = null;
        }

        $this->attributes['is_approved'] = $newValue;
    }

    public function setStatusAttribute($value)
    {
        $this->attributes['status'] = $value;

        $this->setIsApprovedAttribute(($value != self::STATUS_APPROVED) ? false : true);
    }

    public function setNotesAttribute($value)
    {
        if (strlen($value)) {
            $this->attributes['notes'] = $value; 
        }
    }

    /*** SCOPES ***/

    public function scopeApproved($query)
    {
        $query->where('status', self::STATUS_APPROVED);
    }

    public function scopeUnapproved($query)
    {
        $query->whereNull('is_approved');
    }


    public function scopeInvalid($query)
    {
        $query->where('status', self::STATUS_INVALID);
    }

    public function scopePending($query)
    {
        $query->where('status', self::STATUS_PENDING);
    }

    public function scopeValid($query)
    {
        $query->where('status', self::STATUS_VALID);
    }
    
    public function scopeClose($query)
    {
        $query->where('status', self::STATUS_CLOSE);
    }

    public function scopeDateRange($query, $startDate, $endDate)
    {
        $query
            ->whereDate('time_in', '>=', $startDate)
            ->whereDate('time_in', '<=', $endDate);
    }

    public function scopeIsRestDay($query, $isRestDay = false)
    {
        $query->where('is_rest_day', ($isRestDay) ? true : false);
    }

    public function scopeIsAutoApprove($query, $isAutoApproved = false)
    {
        $query->where('is_auto_approve', ($isAutoApproved) ? true : false);
    }


    /*** HELPERS ***/

    public function hasShiftTemplate()
    {
        return $this->attributes['shift_template_period_id'] ? true : false;
    }

    public function isRestDay()
    {
        return $this->is_rest_day ? true : false;
    }

    public function isHoliday()
    {
        return $this->holiday_id ? true : false;
    }

    public function hasNightDifferential()
    {
        return $this->has_night_differential ? true : false;
    }

    public function hasOvertime()
    {
        return $this->ot_application_id ? true : false;
    }

    public static function boot()
    {
        parent::boot();
        Timelog::observe(new \Sunnydevbox\TWPim\Observers\TimelogObserver);
    }
}