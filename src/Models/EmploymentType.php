<?php
namespace Sunnydevbox\TWPim\Models;

use \Sunnydevbox\TWCore\Models\BaseModel;
use \Sunnydevbox\TWPim\Traits\HasEmployeeTrait;

class EmploymentType extends BaseModel
{
    use HasEmployeeTrait;

    protected $table = 'employment_types';

    public $timestamps = false;
    
    protected $fillable = [
        'name',
        'description',
        'system',
    ];

    const TYPE_REGULAR = 'regular';
    const TYPE_SELF_EMPLOYED = 'self_employed';
    const TYPE_CONTRACTUAL = 'contractual';
    const TYPE_PROBATIONARY = 'probationary';
    const TYPE_OJT = 'ojt';
    const TYPE_OFW = 'ofw';


    /** SCOPES  **/
    public function scopeIsRegular($query)
    {
        $query->where('machine_name', $this::TYPE_REGULAR);
    }

    public function scopeIsSelfEmployed($query)
    {
        $query->where('machine_name', $this::TYPE_SELF_EMPLOYED);
    }

    public function scopeIsOJT($query)
    {
        $query->where('machine_name', $this::TYPE_OJT);
    }

    public function scopeIsOFW($query)
    {
        $query->where('machine_name', $this::TYPE_OFW);
    }

    public function scopeIsContractual($query)
    {
        $query->where('machine_name', $this::TYPE_CONTRACTUAL);
    }

    public function scopeIsProbtionary($query)
    {
        $query->where('machine_name', $this::TYPE_PROBATIONARY);
    }

    


    /**  HELPER **/
    public function isRegular()
    {
        return ($this->machine_name == $this::TYPE_REGULAR) ? true : false;
    }

    public function isContractual()
    {
        return ($this->machine_name == $this::TYPE_CONTRACTUAL) ? true : false;
    }

    public function isProbationary()
    {
        return ($this->machine_name == $this::TYPE_PROBATIONARY) ? true : false;
    }

    public function isOJT()
    {
        return ($this->machine_name == $this::TYPE_OJT) ? true : false;
    }

    public function isOFW()
    {
        return ($this->machine_name == $this::TYPE_OFW) ? true : false;
    }

    public function isSelfEmployed()
    {
        return ($this->machine_name == $this::TYPE_SELF_EMPLOYED) ? true : false;
    }
}