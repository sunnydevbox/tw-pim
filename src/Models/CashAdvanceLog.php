<?php
namespace Sunnydevbox\TWPim\Models;

use \Sunnydevbox\TWCore\Models\BaseModel;

class CashAdvanceLog extends BaseModel
{
    const TYPE_DEBIT = 'DEBIT';
    const TYPE_STATUS = 'STATUS';

    protected $table = 'cash_advances_logs';
    
    protected $fillable = [
        'cash_advance_id',
        'payment_date',
        'notes',
        'type',
        'amount_debited',
    ];

    public function cash_advance()
    {
        return $this->belongsTo(config('tw-pim.model_cash_advance'));
    }

    public function getAmountDebitedAttribute()
    {
        return number_format((float) $this->attributes['amount_debited'], 2, '.', '');
    }

    public function getNotesAttribute()
    {
        return nl2br($this->attributes['notes']);
    }
}