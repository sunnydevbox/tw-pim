<?php
namespace Sunnydevbox\TWPim\Models;

use \Sunnydevbox\TWCore\Models\BaseModel;

class EmployeeBenefit extends BaseModel
{
    protected $table = 'employee_benefits';
    
    protected $fillable = [
        'employee_id',
        'benefit_id',
    ];

    public function employee()
    {
        return $this->belongsTo(\Tweeklabs\Tweekapp\Models\Employee::class);
    }

    public function benefit()
    {
        return $this->belongsTo(\Tweeklabs\Tweekapp\Models\Benefit::class);
    }
}
