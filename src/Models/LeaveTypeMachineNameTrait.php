<?php
namespace Sunnydevbox\TWPim\Models;

use \Sunnydevbox\TWCore\Models\BaseModel;
use Sunnydevbox\TWPim\Models\LeaveType;
use Sunnydevbox\TWPim\Models\LeaveApplication;

trait LeaveTypeMachineNameTrait
{   
    /**
     * SCOPES
     */
    public function scopeTypeIsOvertime($query)
    {
        $this->scopeType($query, LeaveType::MACHINE_OVERTIME);
    }

    public function scopeUndertime($query)
    {
        $this->scopeType($query, LeaveType::MACHINE_UNDERTIME);
    }

    public function scopeSickLeave($query)
    {
        $this->scopeType($query, LeaveType::MACHINE_SICK_LEAVE);
    }

    public function scopeVacationLeave($query)
    {
        $this->scopeType($query, LeaveType::MACHINE_VACATION_LEAVE);
    }
    
    public function scopeSabatticalLeave($query)
    {
        $this->scopeType($query, LeaveType::MACHINE_SABATTICAL_LEAVE);
    }

    public function scopeEmergencyLeave($query)
    {
        $this->scopeType($query, LeaveType::MACHINE_EMERGENCY_LEAVE);
    }

    public function scopeBirthdayLeave($query)
    {
        $this->scopeType($query, LeaveType::MACHINE_BIRTHDAY_LEAVE);
    }

    public function scopeMaternityLeave($query)
    {
        $this->scopeType($query, LeaveType::MACHINE_MATERNITY_LEAVE);
    }

    public function scopePaternityLeave($query)
    {
        $this->scopeType($query, LeaveType::MACHINE_PATERNITY_LEAVE);
    }

    public function scopeSoloParentLeave($query)
    {
        $this->scopeType($query, LeaveType::MACHINE_SOLO_PARENT_LEAVE);
    }

    public function scopeRA9262Leave($query)
    {
        $this->scopeType($query, LeaveType::MACHINE_RA9262_LEAVE);
    }

    public function scopeRA9710Leave($query)
    {
        $this->scopeType($query, LeaveType::MACHINE_RA9710_LEAVE);
    }

    public function scopeRestDay($query)
    {
        $this->scopeType($query, LeaveType::MACHINE_REST_DAY);
    }

    public function scopeHoliday($query)
    {
        $this->scopeType($query, LeaveType::MACHINE_HOLIDAY);
    }

    public function scopeType($query, $machine_name)
    {
        if ($query->getModel() instanceof LeaveApplication) {
            $query->whereHas('leave_type', function($query) use ($machine_name) {
                        $query->{camel_case($machine_name)}();
                    });

        } else if ($query->getModel() instanceof LeaveType) {

            $query->where('machine_name', $machine_name);
        }
    }

    /***
     * 
     * HELPERS
     * 
     */
    public function isOvertime()
    {
        return $this->isMachineName(LeaveType::MACHINE_OVERTIME);
    }

    public function isUndertime()
    {
        return $this->isMachineName(LeaveType::MACHINE_UNDERTIME);
    }

    public function isSickLeave()
    {
        $this->isMachineName(LeaveType::MACHINE_SICK_LEAVE);
    }

    public function isVacationLeave()
    {
        $this->isMachineName(LeaveType::MACHINE_VACATION_LEAVE);
    }
    
    public function isSabatticalLeave()
    {
        return $this->isMachineName(LeaveType::MACHINE_SABATTICAL_LEAVE);
    }

    public function isEmergencyLeave()
    {
        return $this->isMachineName(LeaveType::MACHINE_EMERGENCY_LEAVE);
    }

    public function isBirthdayLeave()
    {
        return $this->isMachineName(LeaveType::MACHINE_BIRTHDAY_LEAVE);
    }

    public function isMaternityLeave()
    {
        return $this->isMachineName(LeaveType::MACHINE_MATERNITY_LEAVE);
    }

    public function isPaternityLeave()
    {
        return $this->isMachineName(LeaveType::MACHINE_PATERNITY_LEAVE);
    }

    public function isSoloParentLeave()
    {
        return $this->isMachineName(LeaveType::MACHINE_SOLO_PARENT_LEAVE);
    }

    public function isRA9262Leave()
    {
        return $this->isMachineName(LeaveType::MACHINE_RA9262_LEAVE);
    }

    public function isRA9710Leave()
    {
        return $this->isMachineName(LeaveType::MACHINE_RA9710_LEAVE);
    }

    public function isRestDay()
    {
        return $this->isMachineName(LeaveType::MACHINE_REST_DAY);
    }

    public function isHoliday()
    {
        $this->isMachineName(LeaveType::MACHINE_HOLIDAY);
    }

    public function isMachineName($machine_name)
    {
        if ($this instanceof LeaveApplication) {
            return $this->leave_type->machine_name == $machine_name ? true : false;
        } else if ($this instanceof LeaveType) {
            return $this->machine_name == $machine_name ? true : false;
        }
    }
}