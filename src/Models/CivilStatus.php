<?php
namespace Sunnydevbox\TWPim\Models;

use \Sunnydevbox\TWCore\Models\BaseModel;
use \Sunnydevbox\TWPim\Traits\HasEmployeeTrait;

class CivilStatus extends BaseModel
{
    use HasEmployeeTrait;

    protected $table = 'civil_status';
    
    public $timestamps = false;

    protected $fillable = [
        'name',
        'description',
        'system',
    ];
}