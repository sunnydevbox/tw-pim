<?php
namespace Sunnydevbox\TWPim\Models;

use \Sunnydevbox\TWCore\Models\BaseModel;
use \Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;
use Bnb\Laravel\Attachments\HasAttachment;

class TimelogImport extends BaseModel
{
    use SoftDeletes;
    use HasAttachment;
    
    protected $table = 'timelog_imports';
    
    protected $fillable = [
        'user_id',
        'status'
    ];

    protected $hidden = [
        
    ];


}