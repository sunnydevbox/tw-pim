<?php
namespace Sunnydevbox\TWPim\Models;

use \Sunnydevbox\TWCore\Models\BaseModel;
use \Sunnydevbox\TWPim\Traits\HasEmployeeTrait;

class EmploymentStatus extends BaseModel
{
    use HasEmployeeTrait;

    protected $table = 'employment_status';

    protected $fillable = [
        'name',
        'description',
        'system',
    ];
}