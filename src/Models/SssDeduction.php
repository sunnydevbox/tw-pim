<?php
namespace Sunnydevbox\TWPim\Models;

use Sunnydevbox\TWCore\Models\BaseModel;

class SssDeduction extends BaseModel
{

    protected $fillable = [
        'compensation_from',
        'compensation_to',
        'monthly_salary_credit',
        'social_security_er',
        'social_security_ee',
        'social_security_total',
        'ec_er',
        'total_contribution_er',
        'total_contribution_ee',
        'total_contribution_er_ee',
        'total_contribution_se_vm_ofw',
    ];




    /** GETTER / SETTERS */
    public function getEEAttribute()
    {
        return $this->attributes['social_security_ee'];
    }
    

    /** SCOPES **/
    public function scopeGetRange($query, $salary)
    {
        $query->where(function($query) use($salary) {
            $query
                ->where('compensation_from', '<=', $salary)
                ->where('compensation_to', '>=', $salary);
        })->orWhere(function($query) use($salary) {
            $query
                ->where('compensation_from', '<=', $salary)
                ->whereNull('compensation_to');
        });   
    }

    /** HELPERS **/


    public static function boot()
    {
        parent::boot();
        SssDeduction::observe(new \Sunnydevbox\TWPim\Observers\SssDeductionObserver);
    }

}
