<?php
namespace Sunnydevbox\TWPim\Models;

use \Sunnydevbox\TWCore\Models\BaseModel;
use \Sunnydevbox\TWPim\Traits\HasEmployeeTrait;

class Title extends BaseModel
{
    use HasEmployeeTrait;

    protected $table = 'titles';
    
    public $timestamps = false;

    protected $fillable = [
        'name',
        'system',
    ];
}