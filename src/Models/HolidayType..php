<?php
namespace Sunnydevbox\TWPim\Models;

use \Sunnydevbox\TWCore\Models\BaseModel;

class HolidayType extends BaseModel
{
    public $timestamps = false;
    
    protected $table = 'holiday_types';
    
    protected $fillable = [
        'name',
        'description',
        'country',
    ];

    public function holidays()
    {
        return $this->hasMany(config('tw-pim.models.holiday'), 'holiday_type_id');
    }
}