<?php
namespace Sunnydevbox\TWPim\Models;

use \Sunnydevbox\TWCore\Models\BaseModel;
use \Carbon\Carbon;
// use \Sunnydevbox\TWPim\Traits\HasEmployeeTrait;

class PayrollLog extends BaseModel
{
    // Disregard from further processing
    const STATUS_PENDING = 'PENDING';
    const STATUS_PROCESSING = 'PROCESSING';
    const STATUS_DONE = 'DONE';
    const STATUS_FAILED = 'FAILED';

    protected $table = 'payroll_logs';
    
    protected $fillable = [
        'payroll_template_id',
        'status',
        'start',
        'end',
        'triggered',
        'total_payroll_completed_count',
        'total_payroll_count',
        'is_custom',
        'pay_date',
        'period_count',
        'pay_coverage_start',
        'pay_coverage_end',
    ];

    protected $appends = [
        'progress',
    ];
    
    public function payroll_template()
    {
        return $this->belongsTo(config('tw-pim.models.payroll-template'));
    }

    public function payrolls()
    {
        return $this->hasMany(config('tw-pim.model_payroll'), 'payroll_log_id');
    }

    public function getStartAttribute()
    {
        return Carbon::parse($this->attributes['start']);
    }

    public function getEndAttribute()
    {
        return Carbon::parse($this->attributes['end']);
    }

    public function getPayCoverageStartAttribute()
    {
        if ($this->attributes['pay_coverage_start']) {
            return Carbon::parse($this->attributes['pay_coverage_start']);
        }

        return $this->attributes['pay_coverage_start'];
    }
    
    public function getPayCoverageEndAttribute()
    {
        if ($this->attributes['pay_coverage_end']) {
            return Carbon::parse($this->attributes['pay_coverage_end']);
        }

        return $this->attributes['pay_coverage_end'];
    }

    public function getPayDateAttribute()
    {
        return $this->attributes['pay_date'] ? Carbon::parse($this->attributes['pay_date']) : null;
    }

    public function getPeriodCountAttribute()
    {
        return $this->attributes['period_count'] ? str_ordinal($this->attributes['period_count']) : null;
    }

    public function getIsCustomAttribute()
    {
        if (!$this->attributes['is_custom']) {
            return 0;
        }

        return 1;
    }

    public function getProgressAttribute()
    {
        return (isset($this->attributes['total_payroll_count']) && $this->attributes['total_payroll_count'] > 0) 
                ? $this->attributes['total_payroll_completed_count'] / 
                    $this->attributes['total_payroll_count']
                : 0;
    }

    public function isDone()
    {
        return ($this->attributes['status'] == $this::STATUS_DONE) ? true : false;
    }

    public function isProcessing()
    {
        return ($this->attributes['status'] == $this::STATUS_PROCESSING) ? true : false;
    }

    public function isPending()
    {
        return ($this->attributes['status'] == $this::STATUS_PENDING) ? true : false;
    }

    public function getTriggeredAttribute()
    {
        if ($this->attribute['triggered']) {
            return Carbon::parse($this->attribute['triggered']);
        }

        return false;
    }

    public function setTriggeredAttribute($value)
    {
        if ($value) {
            $this->attributes['triggered'] = Carbon::now();
        } else {
            $this->attributes['triggered'] = null;
        }
    }

    public function setPayrollCount($value)
    {
        $this->total_payroll_count = $value;
        $this->save();

        return $this;
    }

    public function setCurretProgressCount($value = null)
    {
        if (is_numeric($value)) {
            $this->total_payroll_completed_count = $value;
        } else {
            $this->total_payroll_completed_count++;
            $this->save();
        }

        return $this;
    }

    public function setStatus($status)
    {
        $isGood = true;
        
        switch(strtolower($status)) {
            case 'processing':
                $this->status = $this::STATUS_PROCESSING;
                $this->setTriggeredAttribute(true);
            break;

            case 'done':
                $this->status = $this::STATUS_DONE;
            break;

            case 'pending':
                $this->status = $this::STATUS_PENDING;
            break;

            case 'failed':
                $this->status = $this::STATUS_FAILED;
            break;

            default:
                $isGood = false;
            break;
        }

        if ($isGood) {
            $this->save();
        }

        return $this;
    }

    public function scopeIsCustom($query)
    {
        $query->where('is_custom', true);
    }

    public function scopeIsNotCustom($query)
    {
        $query->where('is_custom', false);
    }

    public static function boot()
    {
        parent::boot();
        PayrollLog::observe(new \Sunnydevbox\TWPim\Observers\PayrollLogObserver);
    }
}