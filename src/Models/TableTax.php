<?php
namespace Sunnydevbox\TWPim\Models;

use \Sunnydevbox\TWCore\Models\BaseModel;

class TableTax extends BaseModel
{
    protected $table = 'table_tax';
    
    public $timestamps = false;

    const PERIOD_DAILY = 'DAILY';
    const PERIOD_WEEKLY = 'WEEKLY';
    const PERIOD_SEMI_MONTHLY = 'SEMI_MONTHLY';
    const PERIOD_MONTHLY = 'MONTHLY';

    protected $fillable = [
        'compensation_level_period',
        'salary_range_from',
        'salary_range_to',
        'withholding_tax',
        'withholding_percentage',
    ];

    /** GETTERS/SETTERS */
    public function getWithholdingPercentageAttribute()
    {
        return $this->attributes['withholding_percentage'] ? (float) $this->attributes['withholding_percentage'] : 0;
    }

    /** HELPERS */
    public function getPeriod($period)
    {
        $oClass = new \ReflectionClass(__CLASS__);
        $property = strtoupper('PERIOD_' . str_replace('-', '_', $period));

        if (isset($oClass->getConstants()[$property])) {
            return $oClass->getConstants()[$property];
        }

        return false;
    }


    public function getWithholdingPercentageDecimalAttribute()
    {
        return $this->attributes['withholding_percentage'] 
            ? $this->attributes['withholding_percentage'] / 100 
            : 0;
    }

    public function setSalaryRangeFromAttribute($value)
    {
        if ($value === 0) {
            $value = null;
        }

        $this->attributes['salary_range_from'] = $value;
    }

    public function setSalaryRangeToAttribute($value)
    {
        if ($value === 0) {
            $value = null;
        }

        $this->attributes['salary_range_to'] = $value;
    }

    public function getSalaryRangeFromAttribute()
    {
        return (is_null($this->attributes['salary_range_from'])) ? 0 : $this->attributes['salary_range_from'];
    }

    /***
     * @param $value: between 0 and 100 (whole numbers)
     */
    public function setWithholdingPercentageAttribute($value)
    {
        $this->attributes['withholding_percentage'] = $value / 100;
    }

    public function scopeGetRange($query, $period, $salary)
    {
        $period = $this->getPeriod($period);

        if (!$period) {
            $period = '-';
        }

        $query
            ->where('compensation_level_period', $period)
            ->where(function($query) use($salary) {
                $query->where(function($query) use($salary) {
                    $query
                        ->where('salary_range_from', '<=', $salary)
                        ->where('salary_range_to', '>=', $salary);
                })->orWhere(function($query) use($salary) {
                    $query
                        ->Where('salary_range_from', '<=', $salary)
                        ->whereNull('salary_range_to');
                })->orWhere(function($query) use($salary) {
                    $query
                        ->whereNull('salary_range_from')
                        ->where('salary_range_to', '>=', $salary);
                });
            })
            ;   
    }


}