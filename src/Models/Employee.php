<?php
namespace Sunnydevbox\TWPim\Models;

use \Sunnydevbox\TWUser\Models\User as TWUserModel;
use Bnb\Laravel\Attachments\HasAttachment;

use Sunnydevbox\TWPim\Traits\HasCompanyDepartmentModelTrait;
use Sunnydevbox\TWPim\Traits\HasJobPositionModelTrait;
use Sunnydevbox\TWPim\Traits\HasEmploymentStatusModelTrait;
use Sunnydevbox\TWPim\Traits\HasEmployeeDeductionTrait;
use Sunnydevbox\TWPim\Traits\HasEmploymentTypeModelTrait;
use Sunnydevbox\TWPim\Traits\HasEmployeeLogTrait;
use Sunnydevbox\TWPim\Traits\HasTimelogTrait;
use Sunnydevbox\TWPim\Traits\HasShiftTemplate;
use Sunnydevbox\TWPim\Traits\HasLeaveApplication;
use Sunnydevbox\TWPim\Traits\HasPayrollTemplateTrait;
use Sunnydevbox\TWPim\Traits\HasEmployeeBenefitTrait;
use Sunnydevbox\TWPim\Traits\HasEmployeeTemplateTrait;
use Sunnydevbox\TWPim\Traits\HasEmployeeCivilStatusTrait;
use Sunnydevbox\TWPim\Observers\EmployeeObserver;
use Spatie\Permission\Traits\HasRoles;

class Employee extends TWUserModel
{
    use HasRoles;
    use HasAttachment;
    use HasCompanyDepartmentModelTrait;
    use HasJobPositionModelTrait;
    use HasEmploymentStatusModelTrait;
    use HasEmployeeDeductionTrait;
    use HasEmploymentTypeModelTrait;
    use HasEmployeeLogTrait;
    use HasTimelogTrait;
    use HasShiftTemplate;
    use HasLeaveApplication;
    use HasPayrollTemplateTrait;
    use HasEmployeeBenefitTrait;
    use HasEmployeeTemplateTrait;
    use HasEmployeeCivilStatusTrait;

    protected $appends = [
        'profile_picture',
    ];

    protected $hidden = [
        'password',
        'remember_token',
        'created_at',
        'deleted_at',
        'updated_at',
    ];
    
    protected $fillable = [
        'password',
        'employee_id_number',

        'first_name',
        'last_name',
        'gender',
        'birthdate',
        
        'nationality',
        'religion',

        'email',
        'landline',
        'mobile',
        'address_1',
        'address_2',
        'city',
        'province',
        'zipcode',
        'country',
        'date_hired',
        'date_dismissed',
        'notes',

        'status',
        'hourly_rate',
        'monthly_rate',

        // Foreign keys
        'department_id',
        'job_position_id',
        'civil_status_id',
        'title_id',
        'employment_status_id',
        'employment_type_id',
        'employee_template_id',
    ];

    public function getNameAttribute()
    {
        return $this->attributes['first_name'] . ' ' . $this->attributes['last_name'];
    }

    public function setBirthdateAttribute($value)
    {
        if (is_string($value)) {
            $this->attributes['birthdate'] = \Carbon\Carbon::parse($value)->format('Y-m-d');
        }
    }

    public function setDateHiredAttribute($value)
    {
        if (is_string($value)) {
            $this->attributes['date_hired'] = \Carbon\Carbon::parse($value)->format('Y-m-d');
        }
    }

    public function setDateDismissedAttribute($value)
    {
        if (is_string($value)) {
            $this->attributes['date_dismissed'] = \Carbon\Carbon::parse($value)->format('Y-m-d');
        }
    }

    public function setProfilePictureAttribute($value)
    {   
        return $this->attach($value);
    }

    public function getProfilePictureAttribute()
    {
        return $this->attachment(config('tw-pim.attachmentKeys.profile_picture'));

    }
    

    public function getAttachmentsAttribute()
    {
        return $this->attachment(config('tw-pim.attachmentKeys.employee_files'));
    }


    public function getTable()
    {
        return config('tw-pim.table');
    }

    public function employeeBenefits()
    {
        return $this->hasMany(\Sunnydevbox\TWPim\Models\EmployeeBenefit::class);
    }


    public function scopeStatus($query, $status = null)
    {
        if ($status) {
            $query->where('employement_status_id', $status);
        }
    }

    public static function boot()
    {
        parent::boot();
        
        Employee::observe(new EmployeeObserver);
    }
}