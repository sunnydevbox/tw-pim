<?php
namespace Sunnydevbox\TWPim\Models;

use Sunnydevbox\TWCore\Models\BaseModel;

use Sunnydevbox\TWPim\Traits\HasEmployeeTrait;
use Sunnydevbox\TWPim\Observers\EmployeeObserver;

class EmployeeDeduction extends BaseModel
{
    use HasEmployeeTrait;

    protected $table = 'employee_assigned_deductions';

    protected $fillable = [
        'employee_id',
        'deduction_id',
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at',
        'deduction_id',
        'employee_id',
    ];

    public function deduction()
    {
        return $this->belongsTo(config('tw-pim.models.deduction'));
    }

    public static function boot()
    {
        parent::boot();
    }
}