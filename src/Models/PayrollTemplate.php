<?php
namespace Sunnydevbox\TWPim\Models;

use \Sunnydevbox\TWCore\Models\BaseModel;
use \Carbon\Carbon;

class PayrollTemplate extends BaseModel
{
    public $timestamps = false;
    protected $table = 'payroll_templates';

    const PAYPERIOD_WEEKLY = 'WEEKLY';
    const PAYPERIOD_BI_WIEEKLY = 'BI-WEEKLY';
    const PAYPERIOD_SEMI_MONTHLY = 'SEMI-MONTHLY';
    const PAYPERIOD_MONTHLY = 'MONTHLY';

    protected $fillable = [
        'name',
        'month_days',
        'period_type',
    ];

    protected $appends = [
        'month_days_pretty', 
    ];

    public function employees()
    {
        return $this->belongsToMany(config('tw-pim.models.employee'), 'employees_payroll_templates');
    }

    public function setMonthDaysAttribute($value)
    {
        if (is_string($value)) {
            $days = $this->cleanseMonthDays($value);
        } else {
            $days = $value;
        }

        $this->attributes['month_days'] = serialize($days);
    }


    public function cleanseMonthDays($value)
    {
        if (is_string($value)) {
            $days = explode(',', $value);
            
            $days = array_map(function($item) {
                return trim($item);
            }, $days);

            return $days;
        }
    }

    public function getMonthDaysPrettyAttribute()
    {
        return collect($this->getMonthDaysAttribute())->map(function($v, $k) {
            if (is_numeric($v)) {
                return str_ordinal($v);
            } else {
                return $v;
            }
        })->all();
    }

    public function getMonthDaysAttribute()
    {
        if (isset($this->attributes['month_days'])) { 
            return unserialize($this->attributes['month_days']);
        }
    }

    public function getPayPeriods()
    {
        $reflectionClass = new \ReflectionClass($this);
        
        return collect($reflectionClass->getConstants())->reject(function($value, $key){
            return strpos($key, 'PAYPERIOD_', 0) === false;
        })
        ->mapWithKeys(function($item) {
            return [$item => ucwords(strtolower($item))];
        })
        ->all();
    }
    
    public static function boot()
    {
        parent::boot();
        PayrollTemplate::observe(new \Sunnydevbox\TWPim\Observers\PayrollTemplateObserver);
    }
}