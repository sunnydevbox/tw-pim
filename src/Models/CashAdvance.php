<?php
namespace Sunnydevbox\TWPim\Models;

use \Sunnydevbox\TWCore\Models\BaseModel;
use Carbon\Carbon;

class CashAdvance extends BaseModel
{
    const STATUS_PENDING = 'PENDING';
    const STATUS_DISAPPROVED = 'DISAPPROVED';
    const STATUS_APPROVED = 'APPROVED';
    const STATUS_ACTIVE = 'ACTIVE';
    const STATUS_CLOSED = 'CLOSED';

    protected $table = 'cash_advances';
    
    protected $fillable = [
        'employee_id',
        'amount',
        'freeze_until',
        'duration_start',
        'duration_end',
        'total_amount',
        'paid_amount',
        'payable_per_period',
        'freeze_until',
        'notes',
        'status',
        'approved_on',
    ];

    protected $appends = [
        'balance',
    ];

    public function logs()
    {
        return $this->hasMany(config('tw-pim.model_cash_advance_log'));
    }

    public function employee()
    {
        return $this->belongsTo(config('tw-pim.model_employee'));
    }

    public function getBalanceAttribute()
    {
        return number_format((float) $this->total_amount - $this->paid_amount, 2, '.', '');
    }

    public function setDurationStartAttribute($value)
    {
        if ($value) {
            $value = Carbon::parse($value)->startOfDay();
        }

        $this->attributes['duration_start'] = $value;
    }

    public function setDurationEndAttribute($value)
    {
        if ($value) {
            $value = Carbon::parse($value)->endOfDay();
        }

        $this->attributes['duration_end'] = $value;
    }

    public function getPayablePerPeriodAttribute()
    {
        return number_format((float) $this->attributes['payable_per_period'], 2, '.', '');
    }

    public function getTotalAmountAttribute()
    {
        return number_format((float) $this->attributes['total_amount'], 2, '.', '');
    }

    public function getPaidAmountAttribute()
    {
        if (isset($this->paid_amoount)) {
            return number_format((float) $this->paid_amount, 2, '.', '');
        }
    }

    public function getDurationStartAttribute()
    {
        return Carbon::parse($this->attributes['duration_start']);
    }

    public function getDurationEndAttribute()
    {
        return Carbon::parse($this->attributes['duration_end']);
    }

    public function getFreezeUntilAttribute()
    {
        return ($this->attributes['freeze_until']) ? Carbon::parse($this->attributes['freeze_until']) : null;
    }

    public function isFrozen()
    {
        if (is_null($this->frozen_until)) {
            return false;
        }

        return true;
    }

    public function hasStartDatePassed(?Carbon $referenceDate = null)
    {
        if (!$referenceDate) {
            $referenceDate = Carbon::now();
        }
        
        return ($this->duration_start->lte($referenceDate)) ? true : false;
    }
    public function hasBalance()
    {
        return ((float) $this->balance <= 0) ? false : true;
    }

    public function scopeStatus($query, $status)
    {
        $status = strtoupper(trim($status));
        $query->where('status', $status);
    }

    public function scopeIsActive($query) 
    {
        $this->scopeStatus($query, self::STATUS_ACTIVE);
    }

    public function scopeIsClosed($query) 
    {
        $this->scopeStatus($query, self::STATUS_CLOSED);
    }

    public function scopeIsApproved($query) 
    {
        $this->scopeStatus($query, self::STATUS_APPROVED);
    }

    public function scopeIsDisapproved($query) 
    {
        $this->scopeStatus($query, self::STATUS_DISAPPROVED);
    }

    public function scopeIsPending($query) 
    {
        $this->scopeStatus($query, self::STATUS_PENDING);
    }

    public function scopeIsActiveOrPending($query)
    {
        $query->where('status', self::STATUS_ACTIVE)
            ->orWhere('status', self::STATUS_PENDING);
    }

    public function scopeSetEmployee($query, $employeeID)
    {
        $query->where('employee_id', $employeeID);
    }


    public static function boot()
    {
        parent::boot();
        CashAdvance::observe(new \Sunnydevbox\TWPim\Observers\CashAdvanceObserver);
    }
}