<?php
namespace Sunnydevbox\TWPim\Models;

use \Sunnydevbox\TWCore\Models\BaseModel;
use \Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;
use Sunnydevbox\TWPim\Traits\HasPayrollDeduction;

class Payroll extends BaseModel
{
    use SoftDeletes;
    use HasPayrollDeduction;

    // Disregard from further processing
    const STATUS_INIT = 'INIT';
    const STATUS_APPROVED = 'APPROVED';
    const STATUS_FAILED = 'FAILED';
    const STATUS_BELOW40 = 'BELOW40';
    const STATUS_COMPUTED = 'COMPUTED';

    protected $table = 'payrolls';
    
    protected $fillable = [
        'employee_id',
        'name',
        'title',
        'department',
        'total_gross',
        'total_deduction',
        'total_adjustments',
        'wtax',
        'total_adj_earnings',
        'total_adj_deduction',
        
        /**
         * THE SAME 'total' but this is precalculated. 
         * 
         */
        'total_net',  

        /***
         * NET INCOME
         * this is calculated on the fly
         * 
         * DEPRECATE SOON
         */
        'total', 


        'status',
        'start_date',
        'end_date',
        'payroll_log_id',
        

        // BASIC PAY INFO
        'basic_pay', // DAILY RATE * NUMBER OF DAYS RENDERED
        'hourly_rate',
        'daily_rate',
        'total_hours',
        'total_days',


        'total_undertime',
        'total_holiday',
        'total_holiday_pay',
        'total_paid_leaves_pay',

        'total_overtime_pay',
        'total_overtime',
        'total_restday',
        'total_restday_pay',
    ];

    protected $appends = [
        'adjustment_earning',
        'adjustment_deduction',
        'total_net_with_adjustments',
        'sss',
        'sss_employer',
        'pag_ibig',
        'pag_ibig_employer',
        'philhealth',
        'tax',
    ];

    public function employee()
    {
        return $this->belongsTo(config('tw-pim.models.employee'));
    }

    public function items()
    {
        return $this->hasMany(config('tw-pim.models.payroll-item'));
    }
    
    public function payroll_log()
    {
        return $this->belongsTo(config('tw-pim.models.payroll-log'), 'payroll_log_id');
    }

    public function getTotalNetAttribute()
    {
        if ($this->attributes['total_net']) {

            return number_format((float)$this->attributes['total_net'], 2, '.', '');
        }

        return $this->attributes['total_net'];
    }

    public function getTotalDeductionAttribute()
    {
        if ($this->attributes['total_deduction']) {

            return number_format((float)$this->attributes['total_deduction'], 2, '.', '');
        }

        return $this->attributes['total_net'];
    }

    public function getTotalGrossAttribute()
    {
        if ($this->attributes['total_gross']) {

            return number_format((float)$this->attributes['total_gross'], 2, '.', '');
        }

        return $this->attributes['total_gross'];
    }

    public function getTotalAdjustmentsAttribute()
    {
        if ($this->attributes['total_adjustments']) {

            return number_format((float)$this->attributes['total_adjustments'], 2, '.', '');
        }

        return $this->attributes['total_adjustments'];
    }

    public function getTotalAdjEarningsAttribute()
    {
        if ($this->attributes['total_adj_earnings']) {

            return number_format((float)$this->attributes['total_adj_earnings'], 2, '.', '');
        }

        return $this->attributes['total_adj_earnings'];
    }

    public function getTotalAdjDeductionsAttribute()
    {
        if ($this->attributes['total_adj_deductions']) {

            return number_format((float)$this->attributes['total_adj_deductions'], 2, '.', '');
        }

        return $this->attributes['total_adj_deductions'];
    }

    public function getWtaxAttribute()
    {
        if ($this->attributes['total_gross']) {

            return number_format((float)$this->attributes['wtax'], 2, '.', '');
        }

        return $this->attributes['total_gross'];
    }

    public function getAdjustmentEarningAttribute()
    {
        return number_format((float)$this->items()->get()->where('type', app(config('tw-pim.model_payroll_item'))::TYPE_ADJ_EARNING)->sum('total_net'), 2, '.', '');
    }

    public function getAdjustmentDeductionAttribute()
    {
        return number_format((float)$this->items()->get()->where('type', app(config('tw-pim.model_payroll_item'))::TYPE_ADJ_DEDUCTION)->sum('total_net'), 2, '.', '');
    }

    public function getTotalNetWithAdjustmentsAttribute()
    {
        return number_format(
            $this->total_net + $this->total_adjustments
            // ($this->total_gross - $this->total_deductions - $this->wtax) + $this->total_adjustments
            , 2, '.', '');
    }

    public function getBasicPayAttribute()
    {
        if ($this->attributes['basic_pay']) {

            return number_format((float)$this->attributes['basic_pay'], 2, '.', '');
        }

        return $this->attributes['basic_pay'];
    }

    public function getHourlyRateAttribute()
    {
        if ($this->attributes['hourly_rate']) {

            return number_format((float)$this->attributes['hourly_rate'], 2, '.', '');
        }

        return $this->attributes['hourly_rate'];
    }

    public function getDailyRateAttribute()
    {
        if ($this->attributes['daily_rate']) {

            return number_format((float)$this->attributes['daily_rate'], 2, '.', '');
        }

        return $this->attributes['daily_rate'];
    }

    public function getTotalOvertimePayAttribute()
    {
        if ($this->attributes['total_overtime_pay']) {

            return number_format((float)$this->attributes['total_overtime_pay'], 2, '.', '');
        }

        return $this->attributes['total_overtime_pay'];
    }

    public function getTotalRestdayPayAttribute()
    {
        if ($this->attributes['total_restday_pay']) {

            return number_format((float)$this->attributes['total_restday_pay'], 2, '.', '');
        }

        return $this->attributes['total_restday_pay'];
    }

    public function getTotalRestdayAttribute()
    {
        if ($this->attributes['total_restday']) {

            return number_format((float)$this->attributes['total_restday'], 2, '.', '');
        }

        return $this->attributes['total_restday'];
    }

    public function getTotalHolidayPayAttribute()
    {
        if ($this->attributes['total_holiday_pay']) {

            return number_format((float)$this->attributes['total_holiday_pay'], 2, '.', '');
        }

        return $this->attributes['total_holiday_pay'];
    }

    public function getTotalHolidayAttribute()
    {
        if ($this->attributes['total_holiday']) {

            return number_format((float)$this->attributes['total_holiday'], 2, '.', '');
        }

        return $this->attributes['total_holiday'];
    }

    public function initialize()
    {
        $this->status = $this::STATUS_INIT;
        $this->total_gross = 0;
        $this->total_net = 0;
        $this->total_deduction = 0;
        $this->total_adjustments = 0;
        $this->save();
    }

    public static function boot()
    {
        parent::boot();
        // Holiday::observe(new \Sunnydevbox\TWPim\Observers\HolidayObserver);
    }
}