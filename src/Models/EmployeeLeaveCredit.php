<?php
namespace Sunnydevbox\TWPim\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use \Sunnydevbox\TWCore\Models\BaseModel;
use Carbon\Carbon;

class EmployeeLeaveCredit extends BaseModel
{
    use SoftDeletes;

    protected $appends = [];

    protected $fillable = [
        'employee_id',
        'leave_type_id',
        'amount',
        'notes',
    ];
}