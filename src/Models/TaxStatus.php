<?php
namespace Sunnydevbox\TWPim\Models;

use \Sunnydevbox\TWCore\Models\BaseModel;
use \Sunnydevbox\TWPim\Traits\HasEmployeeTrait;

class TaxStatus extends BaseModel
{
    use HasEmployeeTrait;

    protected $table = 'tax_status';
    
    public $timestamps = false;

    protected $fillable = [
        'name',
        'description',
        'system',
    ];
}