<?php
namespace Sunnydevbox\TWPim\Models;

use Sunnydevbox\TWCore\Models\BaseModel;

class Philhealth extends BaseModel
{
    protected $table = 'philhealth';
    public $timestamps = false;
    protected $fillable = [
        'salary_bracket',
        'salary_range_from',
        'salary_range_to',
        'salary_base',
        'monthly_premium',
        'employee_share',
        'employer_share',
    ];

    /** GETTER / SETTERS */
    public function getEmployeeShareAttribute()
    {
        return number_format((float)$this->attributes['employee_share'], 2, '.', '');
    }

    public function getEmployerShareAttribute()
    {
        return number_format((float)$this->attributes['employer_share'], 2, '.', '');
    }

    /** SCOPES **/
    public function scopeGetRange($query, $salary)
    {
        $query->where(function($query) use($salary) {
            $query
                ->where('salary_range_from', '<=', $salary)
                ->where('salary_range_to', '>=', $salary);
        })->orWhere(function($query) use($salary) {
            $query
                ->Where('salary_range_from', '<=', $salary)
                ->whereNull('salary_range_to');
        })->orWhere(function($query) use($salary) {
            $query
                ->whereNull('salary_range_from')
                ->where('salary_range_to', '>=', $salary);
        });   
    }

    /** HELPERS **/


}
