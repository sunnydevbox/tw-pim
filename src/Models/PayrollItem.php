<?php
namespace Sunnydevbox\TWPim\Models;

use \Sunnydevbox\TWCore\Models\BaseModel;
use \Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;

class PayrollItem extends BaseModel
{
    use SoftDeletes;
    
    // Disregard from further processing
    const STATUS_APPROVED = 'APPROVED';

    const TYPE_EARNING = 'EARNING';
    const TYPE_DEDUCTION = 'DEDUCTION';
    const TYPE_ADJ_EARNING = 'ADJ_EARNING'; // adjustment earning
    const TYPE_ADJ_DEDUCTION = 'ADJ_DEDUCTION'; // adjustment deduction
    const TYPE_DEDUCTION_WTAX = 'DEDUCTION_WTAX'; // Withholding tax
    const TYPE_DEDUCTION_CA = 'DEDUCTION_CA'; // Cash advance

    protected $table = 'payroll_items';
    
    protected $fillable = [
        'name',
        'payroll_id',
        'type',
        'total_gross',
        'total_net',
        'data',
        'is_custom',
        'override_total_net',
        'tag',
    ];

    public function payroll()
    {
        return $this->belongsTo(config('tw-pim.models.payroll'));
    }

    public function getIsCustomAttribute()
    {
        return $this->attributes['is_custom'] ? true : false;
    }

    public function setDataAttribute ($value)
    {
        if (is_array($value)) {
            
            // GET reference ID if it is an array or object
            if (isset($value['reference'])) {
                if (is_array($value['reference'])) {
                    $value['reference'] = $value['reference']['id'];
                } else if (is_object($value['reference'])) {
                    $value['reference'] = $value['reference']->id;
                }
            }

            $this->attributes['data'] = serialize($value);
        }
    }

    public function getDataAttribute()
    {
        $value = null;

        if (is_array($this->attributes['data'])) {
            $value = $this->attributes['data'];
        }
        
        if (is_string($this->attributes['data'])) {
            $value = unserialize($this->attributes['data']);
        }

        if (isset($value['reference']) && is_numeric($value['reference'])) {
            $reference = app(config('tw-pim.model_payroll_item'))->find($value['reference']);

            if ($reference) {
                $value['reference'] = $reference;
            }
        }

        return $value;
    }

    public function getTotalNetAttribute()
    {
        // if ($this->getOverrideTotalNetAttribute()) {
        //     return $this->getOverrideTotalNetAttribute();
        // } else 

        
        if (isset($this->attributes['total_net'])) {
            return number_format((float)$this->attributes['total_net'], 2, '.', '');
        }

        return null;    
        return $this->attributes['total_net'];
    }

    public function getOverrideTotalNetAttribute()
    {
        if (!$this->attributes['override_total_net']) {
            return null;
        } else if (is_numeric($this->attributes['override_total_net'])) {
            return number_format((float)$this->attributes['override_total_net'], 2, '.', '');
        }

        return null;
    }

    /**
     * @$type must be one of the constants in PayrollItem model
     */
    public function scopeTypeIs($query, $type)
    {
        $query->where('type', $type);
    }
    
    public static function boot()
    {
        parent::boot();
        PayrollItem::observe(new \Sunnydevbox\TWPim\Observers\PayrollItemObserver);
    }
}