<?php
namespace Sunnydevbox\TWPim\Models;

use \Sunnydevbox\TWCore\Models\BaseModel;
use Carbon\Carbon;
use Sunnydevbox\TWPim\Models\LeaveTypeMachineNameTrait;

class LeaveApplication extends BaseModel
{   
    use LeaveTypeMachineNameTrait;

    const STATUS_PENDING = 'PENDING';
    const STATUS_APPROVED = 'APPROVED';
    const STATUS_DISAPPROVED = 'DISAPPROVED';

    protected $table = 'leave_applications';
    
    protected $fillable = [
        'leave_type_id',
        'employee_id',
        'purpose',
        'start',
        'end',
        'status',
        'duration_in_minutes',
        'used_in_minutes',
    ];

    public function employee()
    {
        return $this->belongsTo(config('tw-pim.models.employee'));
    }

    public function leave_type()
    {
        return $this->belongsTo(config('tw-pim.models.leave-type'), 'leave_type_id');
    }

    public function holiday()
    {
        return $this->belongsTo(config('tw-pim.models.holiday'), 'holiday_id');
    }

    /**
     * Alias to leave_type()
     */
    public function type()
    {
        return $this->leave_type();
    }

    public function getStartAttribute()
    {
        return $this->attributes['start'] ? Carbon::parse($this->attributes['start']) : null;
    }

    public function getEndAttribute()
    {
        return $this->attributes['end'] ? Carbon::parse($this->attributes['end']) : null;
    }

    public function getCreatedAtAttribute()
    {
        return $this->attributes['created_at'] ? Carbon::parse($this->attributes['created_at']) : null;
    }

    public function getUpdatedAtAttribute()
    {
        return $this->attributes['updated_at'] ? Carbon::parse($this->attributes['updated_at']) : null;
    }

    public function scopeRange($query, Carbon $startDate,  $endDate = null)
    {
        $query->where('start', '>=', $startDate);

        if ($endDate) {
            $query->where('start', '<=', $endDate);
        }
    }

    public function scopeDate($query, Carbon $date)
    {
        $query->whereDay('start', '=', $date->day)
                ->whereMonth('start', '=', $date->month)
                ->whereYear('start', '=', $date->year);
    }    

    public function scopeApproved($query)
    {
        $query->where('status', self::STATUS_APPROVED);
    }

    public function scopePending($query)
    {
        $query->where('status', self::STATUS_PENDING);
    }

    public function scopeDisapproved($query)
    {
        $query->where('status', self::STATUS_DISAPPROVED);
    }

    public function getStatuses()
    {
        $reflectionClass = new \ReflectionClass($this);

        return collect($reflectionClass->getConstants())->reject(function($value, $key){
            return strpos($key, 'STATUS_', 0) === false;
        })
        ->mapWithKeys(function($item) {
            // var_dump($item);
            return [$item => ucwords(strtolower($item))];
        })
        ->all();
    }

    public function scopeFindMatch($query, $start, $end = null)
    {
        $query->whereDate('start', '>=', $start)
            ->whereDate('end', '<=', $start)
            ;
    }

    public function scopeSetEmployee($query, $employeeId)
    {
        $query->where('employee_id', $employeeId);
    }

    // public function scopeRestDay($query)
    // {
    //     $query->whereHas('leave_type', function($query) {
    //         $query->isRestDay();
    //     });
    // }

    // public function scopeHoliday($query)
    // {
    //     $query->whereHas('leave_type', function($query) {
    //         $query->isHoliday();
    //     });
    // }
    

    public function checkRemaining()
    {
        return $this->duration_in_minutes - $this->used_in_minutes;
    }

    public function use($minutes)
    {
        // dd($minutes, $this->duration_in_minutes); 
        if ($minutes <= $this->duration_in_minutes) {
            $this->used_in_minutes += $minutes;
            $this->save();
        }
    }

    public static function boot()
    {
        parent::boot();
        LeaveApplication::observe(new \Sunnydevbox\TWPim\Observers\LeaveApplicationObserver);
    }
}
