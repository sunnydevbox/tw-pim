<?php
namespace Sunnydevbox\TWPim\Models;

use \Sunnydevbox\TWCore\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class ShiftTemplatePeriod extends BaseModel
{
    use SoftDeletes;

    protected $table = 'shift_template_periods';    
    
    protected $fillable = [
        'shift_template_id',
        'name',
        'description',
        'start',
        'end',
        'grace_period',
        'day_m',
        'day_t',
        'day_w',
        'day_th',
        'day_f',
        'day_s',
        'day_sun',
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    /**
     * DEPRECATE SOON
     */
    public function shiftTemplate()
    {
        return $this->shift_template();
    }

    /**
     * ALIAS to shiftTemplate()
     */
    public function shift_template()
    {
        return $this->belongsTo(config('tw-pim.models.shift-template'));
    }

    public function setStartAttribute($value)
    {
        $this->attributes['start'] = trim($value);
    }

    public function getWorkDaysAttribute()
    {
        $days = [];

        if ($this->attributes['day_m']) {
            $days[] = 1;
        }

        if ($this->attributes['day_t']) {
            $days[] = 2;
        }

        if ($this->attributes['day_w']) {
            $days[] = 3;
        }

        if ($this->attributes['day_th']) {
            $days[] = 4;
        }

        if ($this->attributes['day_f']) {
            $days[] = 5;
        }
        
        if ($this->attributes['day_s']) {
            $days[] = 6;
        }

        if ($this->attributes['day_sun']) {
            $days[] = 7;
        }

        return collect($days);
    }

    public function getRestDaysAttribute()
    {
        $days = [];

        if (!$this->attributes['day_m']) {
            $days[] = 1;
        }

        if (!$this->attributes['day_t']) {
            $days[] = 2;
        }

        if (!$this->attributes['day_w']) {
            $days[] = 3;
        }

        if (!$this->attributes['day_th']) {
            $days[] = 4;
        }

        if (!$this->attributes['day_f']) {
            $days[] = 5;
        }
        
        if (!$this->attributes['day_s']) {
            $days[] = 6;
        }

        if (!$this->attributes['day_sun']) {
            $days[] = 7;
        }

        return collect($days);
    }


    public function getStartObjectAttribute()
    {
        $startTime = explode(':', $this->start);

        return Carbon::now()->hour($startTime[0])->minute($startTime[1]);
    }

    public function getEndObjectAttribute()
    {
        $endTime = explode(':', $this->end);

        return Carbon::now()->hour($endTime[0])->minute($endTime[1]);
    }

    public function scopeEmployee($query, $employeeId)
    {
        $query
            ->join('shift_templates', 'shift_templates.id', $this->table . '.shift_template_id')
            ->join('employees_shift_templates', 'employees_shift_templates.shift_template_id', $this->table . '.shift_template_id')
            ->where('employees_shift_templates.employee_id', $employeeId)
            ->orderBy('shift_templates.id', 'asc')
            ->orderBy($this->table.'.id', 'asc')
        ;

        // $query
        //     ->join('shift_templates', 'shift_templates.id', $this->table . '.shift_template_id')
        //     ->join('employees', 'employees.shift_template_id', $this->table . '.shift_template_id')
        //     ->where('employees.id', $employeeId)
        //     ->orderBy('shift_templates.id', 'asc')
        //     ->orderBy($this->table.'.id', 'asc')
        //     ;
    }

    public function checkRestDay()
    {

    }
}
