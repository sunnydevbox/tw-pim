<?php
namespace Sunnydevbox\TWPim\Models;

use \Sunnydevbox\TWCore\Models\BaseModel;
use Sunnydevbox\TWPim\Models\LeaveTypeMachineNameTrait;

class LeaveType extends BaseModel
{   
    use LeaveTypeMachineNameTrait;

    const MACHINE_OVERTIME = 'OVERTIME';
    const MACHINE_UNDERTIME = 'UNDERTIME';
    const MACHINE_SICK_LEAVE = 'SICK_LEAVE';
    const MACHINE_VACATION_LEAVE = 'VACATION_LEAVE';
    const MACHINE_SABATICAL_LEAVE = 'SABATTICAL_LEAVE';
    const MACHINE_EMERGENCY_LEAVE = 'EMERGENCY_LEAVE';
    const MACHINE_BIRTHDAY_LEAVE = 'BIRTHDAY_LEAVE';
    const MACHINE_MATERNITY_LEAVE = 'MATERNITY_LEAVE';
    const MACHINE_PATERNITY_LEAVE = 'PATERNITY_LEAVE';
    const MACHINE_SOLO_PARENT_LEAVE = 'SOLO_PARENT_LEAVE';
    const MACHINE_RA9262_LEAVE = 'RA9262_LEAVE';
    const MACHINE_RA9710_LEAVE = 'RA9710_LEAVE';
    const MACHINE_REST_DAY = 'REST_DAY';
    const MACHINE_HOLIDAY = 'HOLIDAY';


    protected $table = 'leave_types';
    
    protected $fillable = [
        'name',
        'description',
        'system',
    ];   
}