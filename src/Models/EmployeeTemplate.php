<?php
namespace Sunnydevbox\TWPim\Models;

use \Sunnydevbox\TWCore\Repositories\TWMetaTrait;
use \Sunnydevbox\TWCore\Models\BaseModel;
use Carbon\Carbon;

class EmployeeTemplate extends BaseModel
{
    use TWMetaTrait;

    const TAX_PERIOD_DAILY = 'DAILY';
    const TAX_PERIOD_WEEKLY = 'WEEKLY';
    const TAX_PERIOD_SEMI_MONTHLY = 'DSEMI_MONTHLY';
    const TAX_PERIOD_MONTHLY = 'MONTHLY';
    

    protected $appends = [
        'employee_count',
        //Monthly Basic Pay + Overtime Pay + Holiday Pay + Night 
        //    Differential - Tardiness - Absences - SSS/Philhealth/PagIbig deductions 
        'deduction_philhealth',
        'deduction_sss',
        'deduction_tardiness',
        'deduction_bir',
        'deduction_absences',
        'deduction_pag_ibig',
        'deduction_cash_advance',

        'benefit_basic_pay',
        'benefit_overtime_pay',
        'benefit_holiday_pay',
        'benefit_restday_pay',
        'benefit_night_differential_pay',
        'benefit_loyalty_incentive',
        'benefit_daily_extra',

        'total_days_in_year',
        'total_hours_in_day',
        'night_differential_start',
        'night_differential_end',

        // LEAVES
        'total_sick_leave',
        'total_emergency_leave',
        'total_maternity_leave',
        'total_paternity_leave',
        'total_vacation_leave',
        'total_birthday_leave',
        'total_solo_parent_leave',

        // TAX OPTIONS
        'tax_period', // DAILY / WEEKLY / SEMI_MONTHLY / MONTHLY
    ];

    protected $fillable = [
        'name',
        'description',
    ];

    protected $meta = [

        /** DEDUCTIONS  (true/false) */
        'deduction_philhealth',
        'deduction_sss',
        'deduction_tardiness',
        'deduction_bir',
        'deduction_absences',
        'deduction_pag_ibig',
        'deduction_cash_advance',

        /** BENEFITS (true/false) */
        'benefit_basic_pay',
        'benefit_overtime_pay',
        'benefit_holiday_pay',
        'benefit_restday_pay',
        'benefit_night_differential_pay',
        'benefit_loyalty_incentive',
        'benefit_daily_extra',


        'total_days_in_year',
        'total_hours_in_day',
        'night_differential_total',
        
        'total_sick_leave',
        'total_emergency_leave',
        'total_maternity_leave',
        'total_paternity_leave',
        'total_vacation_leave',
        'total_birthday_leave',
        'total_solo_parent_leave',

        'night_differential_end',
        'night_differential_start', // FORMAT: hh:mm

        'tax_period',
    ];


    public function employees()
    {
        return $this->hasMany(config('tw-pim.model_employee'), 'employee_template_id');
    }


    public function getItems()
    {
        $d = [];
        foreach($this->appends as $key) {
            $d[$key] = $this->getMeta($key);
        }

        return collect($d);
    }

    public function getEmployeeCountAttribute()
    {   
        return $this->employees()->count();
    }

    /** GETTERS/SETTERS */
    public function getTotalHoursInDayAttribute()
    {
        return $this->getMeta('total_hours_in_day', null);
    }

    public function getTotalDaysInYearAttribute()
    {
        return $this->getMeta('total_days_in_year', null);
    }


    public function getNightDifferentialStartAttribute()
    {
        return $this->getMeta('night_differential_start', null);
    }

    public function getNightDifferentialEndAttribute()
    {
        return $this->getMeta('night_differential_end', null);
        $end = $this->getMeta('n ight_differential_end', null);

        if ($end) {
            $time = explode(':', trim($end));
            $t = Carbon::now()->hour($time[0])->minute($time[1])->addHours($this->getMeta('n ight_differential_total', 8));
            return $t->format('h:i');
        }

        return $end;
    }

    public function getTotalSickLeaveAttribute()
    {
        return $this->getMeta('total_sick_leave', null);
    }

    public function getTotalEmergencyLeaveAttribute()
    {
        return $this->getMeta('total_emergency_leave', null);
    }

    public function getTotalMaternityLeaveAttribute()
    {
        return $this->getMeta('total_maternity_leave', null);
    }

    public function getTotalPaternityLeaveAttribute()
    {
        return $this->getMeta('total_paternity_leave', null);
    }

    public function getTotalVacationLeaveAttribute()
    {
        return $this->getMeta('total_vacation_leave', null);
    }

    public function getTotalBirthdayLeaveAttribute()
    {
        return $this->getMeta('total_birthday_leave', null);
    }

    public function getTotalSoloParentLeaveAttribute()
    {
        return $this->getMeta('total_solo_parent_leave', null);
    }

    public function getDeductionPhilhealthAttribute()
    {
        return $this->getMeta('deduction_philhealth', null) ? true : false;
    }

    public function getDeductionSssAttribute()
    {
            return $this->getMeta('deduction_sss', null) ? true : false;
    }

    public function getDeductionTardinessAttribute()
    {
        return $this->getMeta('deduction_tardiness', null) ? true : false;
    }
    
    public function getDeductionBirAttribute()
    {
        return $this->getMeta('deduction_bir', null) ? true : false;
    }

    public function getDeductionAbsencesAttribute()
    {
        return $this->getMeta('deduction_absences', null) ? true : false;
    }

    public function getDeductionPagIbigAttribute()
    {
        return $this->getMeta('deduction_pag_ibig', null) ? true : false;
    }

    public function getDeductionCashAdvanceAttribute()
    {
        return $this->getMeta('deduction_cash_advance', null) ? true : false;
    }

    public function getBenefitBasicPayAttribute()
    {
        return $this->getMeta('benefit_basic_pay', null) ? true : false;
    }

    public function getBenefitOvertimePayAttribute()
    {
        return $this->getMeta('benefit_overtime_pay', null) ? true : false;
    }

    public function getBenefitHolidayPayAttribute()
    {
        return $this->getMeta('benefit_holiday_pay', null) ? true : false;
    }

    public function getBenefitRestdayPayAttribute()
    {
        return $this->getMeta('benefit_restday_pay', null) ? true : false;
    }

    public function getBenefitNightDifferentialPayAttribute()
    {
        return $this->getMeta('benefit_night_differential_pay', null) ? true : false;
    }

    public function getBenefitLoyaltyIncentiveAttribute()
    {
        return $this->getMeta('benefit_loyalty_incentive', null) ? true : false;
    }
    

    public function getBenefitDailyExtraAttribute()
    {
        return $this->getMeta('benefit_daily_extra', null) ? true : false;
    }
    
    public function getTaxPeriodAttribute()
    {
        return $this->getMeta('tax_period', null);
    }
}