<?php
namespace Sunnydevbox\TWPim\Models;

use \Sunnydevbox\TWCore\Models\BaseModel;
use \Sunnydevbox\TWPim\Traits\HasEmployeeTrait;

class JobPosition extends BaseModel
{
    use HasEmployeeTrait;

    protected $table = 'job_positions';

    public $timestamps = false;
    
    protected $fillable = [
        'name',
        'description',
        'system',
    ];
}