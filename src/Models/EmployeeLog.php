<?php
namespace Sunnydevbox\TWPim\Models;

use \Sunnydevbox\TWCore\Models\BaseModel;
use \Sunnydevbox\TWPim\Traits\HasEmployeeTrait;

class EmployeeLog extends BaseModel
{
    use HasEmployeeTrait;

    protected $table = 'employee_logs';

    protected $fillable = [
        'actorable_id',
        'actorable_type',
        'employee_id',
        'description',
        'action',
    ];

    /**
     * Actor
     */
    public function actorable()
    {
        return $this->morphTo();
    }
}