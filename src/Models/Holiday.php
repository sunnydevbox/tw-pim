<?php
namespace Sunnydevbox\TWPim\Models;

use \Sunnydevbox\TWCore\Models\BaseModel;
use \Carbon\Carbon;

class Holiday extends BaseModel
{
    public $timestamps = false;

    protected $table = 'holidays';
    
    protected $fillable = [
        'name',
        'description',
        'date',
        'start_date',
        'end_date',
        'country',
        'holiday_type_id',
    ];

    public function holiday_type()
    {
        return $this->belongsTo(config('tw-pim.models.holiday-type'), 'holiday_type_id');
    }


    public function setStartDateAttribute($value)
    {
        if (is_string($value)) {
            $value = Carbon::parse($value)->startOfDay()->format('Y-m-d');
        }

        $this->attributes['start_date'] = $value;
    }

    public function setEndDateAttribute($value)
    {
        if (is_string($value)) {
            $value = \Carbon\Carbon::parse($value)->endOfDay()->format('Y-m-d');
        }

        $this->attributes['end_date'] = $value;
    }

    public function getStartDateAttribute()
    {
        $value = Carbon::parse($this->attributes['start_date'])->format('Y-m-d');
        $value = Carbon::parse($this->attributes['start_date']);

        return $value;
    }

    public function getEndDateAttribute()
    {
        $value = \Carbon\Carbon::parse($this->attributes['end_date'])->format('Y-m-d');
        
        return $value;
    }

    public function isRegularHoliday()
    {
        return ($this->holiday_type->id == 1)? true : false;
    }

    /**
     * Alias to isRegularHoliday()
     */
    public function isSpecialHoliday()
    {
        return $this->isNonWorkingHoliday();
    }

    public function isNonWorkingHoliday()
    {
        return ($this->holiday_type->id == 2)? true : false;
    }

    public function scopeRange($query, $start, $end)
    {
        $query
            ->where('start_date', '>=', $start)
            ->where('end_date', '<=', $end);
    }

    public static function boot()
    {
        parent::boot();
        Holiday::observe(new \Sunnydevbox\TWPim\Observers\HolidayObserver);
    }
}