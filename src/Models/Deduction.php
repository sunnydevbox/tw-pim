<?php
namespace Sunnydevbox\TWPim\Models;

use \Sunnydevbox\TWCore\Models\BaseModel;
use \Sunnydevbox\TWPim\Traits\HasEmployeeTrait;
use Illuminate\Database\Eloquent\SoftDeletes;

class Deduction extends BaseModel
{
    use SoftDeletes;
    use HasEmployeeTrait;
    
    protected $table = 'deductions';
    
    protected $fillable = [
        'name',
        'description',
        'country',
        'region',
        'system',
    ];

    protected $hidden = [
        'created_at',
        'deleted_at',
        'updated_at',
    ];

    public function employees()
    {
        return $this->belongsTo(config('tw-pim.models.employee'), 'employee_assigned_deductions', 'deduction_id', 'employee_id');
    }

    public function scopeEmployee( $employeeId)
    {
        echo 'A';
        dd($employeeId);
        exit;
        return $query->wherePivot('employee_id', $employeeId);
    }
}