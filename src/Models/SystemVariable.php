<?php
namespace Sunnydevbox\TWPim\Models;

use \Sunnydevbox\TWCore\Models\BaseModel;

class SystemVariable extends BaseModel
{
    protected $table = 'system_variables';
    public $timestamps = false;
    public $incrementing = false;
    protected $fillable = [
        'id',
        'value',
    ];

    public static function boot()
    {
        parent::boot();
        SystemVariable::observe(new \Sunnydevbox\TWPim\Observers\SystemVariableObserver);
    }
}