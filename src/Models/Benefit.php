<?php
namespace Sunnydevbox\TWPim\Models;

use \Sunnydevbox\TWCore\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class Benefit extends BaseModel
{
    use SoftDeletes;

    protected $table = 'benefits';
    
    protected $fillable = [
        'name',
        'description',
        'machine_name',
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public function employeeBenefits()
    {
        return $this->hasMany(\Sunnydevbox\TWPim\Models\EmployeeBenefit::class);
    }
}
