<?php
namespace Sunnydevbox\TWPim\Factories;

use Illuminate\Support\Facades\Input;

class EmployeeDataFactory
{
    private $repository;


    public static function store($type, $attributes = [])
    {
        $rpo = null;
        switch($type) {
            case 'personal':
                $rpo = new \Sunnydevbox\TWPim\Repositories\Employee\EmployeeDataDetailsRepository(app());
            break;

            case 'employment':  
                $rpo = new \Sunnydevbox\TWPim\Repositories\Employee\EmployeeDataEmploymentRepository(app());
            break;

            case 'employee-id':  
                $rpo = new \Sunnydevbox\TWPim\Repositories\Employee\EmployeeDataEmployeeIdRepository(app());
            break;

            case 'profile-picture':
                $rpo = new \Sunnydevbox\TWPim\Repositories\Employee\EmployeeDataPictureRepository(app());
                return $rpo->attachProfilePicture($attributes['id'], $attributes['profile_picture']);
            break;

            case 'password':
                $rpo = new \Sunnydevbox\TWPim\Repositories\Employee\EmployeeDataPasswordRepository(app());
            break;

            case 'remittance':
                $rpo = new \Sunnydevbox\TWPim\Repositories\Employee\EmployeeDataDeductionRepository(app());
            break;

            case 'biofield-id':
                $rpo = new \Sunnydevbox\TWPim\Repositories\Employee\EmployeeDataBiofieldRepository(app());
            break;

            default:
                throw new \Exception("Invalid data type given.");
            break;
        }

        return $rpo;
    }

    public function __construct(
        // \Sunnydevbox\TWPim\Repositories\Employee\EmployeeDataDetailsRepository $rpoEmployeeDataDetails,
        // \Sunnydevbox\TWPim\Repositories\Employee\EmployeeDataEmploymentRepository $rpoEmployeeDataEmployment
    ) {
        // $this->rpoEmployeeDataDetails = $rpoEmployeeDataDetails;
        // $this->rpoEmployeeDataEmployment = $rpoEmployeeDataEmployment;
    }
}