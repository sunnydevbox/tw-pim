<?php

namespace Sunnydevbox\TWPim;

use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [

        // User signed up for an account
        'Sunnydevbox\TWPim\Events\EmployeeLogEvent' => [
            'Sunnydevbox\TWPim\Listeners\LogEmployeeActionListener',
        ],

        'Sunnydevbox\TWPim\Events\EmployeeCreatedEvent' => [
            'Sunnydevbox\TWPim\Listeners\EmployeeGenerateIDListener',
            'Sunnydevbox\TWPim\Listeners\EmployeeAttachRoleListener',
        ],

        'Sunnydevbox\TWPim\Events\EmployeeDeletedEvent' => [
            'Sunnydevbox\TWPim\Listeners\EmployeeDeletedListener',
        ],

        'Sunnydevbox\TWPim\Events\EmployeeDeletingEvent' => [
            'Sunnydevbox\TWPim\Listeners\EmployeeDeletingListener',
        ],

        'Sunnydevbox\TWPim\Events\EmployeeUpdatedEvent' => [
            
        ],

        'Sunnydevbox\TWPim\Events\EmployeeUpdatedIdEvent' => [
            'Sunnydevbox\TWPim\Listeners\EmployeeGenerateIDListener',
        ],

        'Sunnydevbox\TWPim\Events\TimelogEntryEvent' => [
            'Sunnydevbox\TWPim\Listeners\TimelogEntryCalculateListerner',
        ],

        'Sunnydevbox\TWPim\Events\TimelogImportEvent' => [
            'Sunnydevbox\TWPim\Listeners\TimelogImportEventListener',
        ],

        'Sunnydevbox\TWPim\Events\PayrollGenerateEvent' => [
            'Sunnydevbox\TWPim\Listeners\PayrollLogGeneratingListener',
        ],

        'Sunnydevbox\TWPim\Events\PayrollItemCreatedEvent' => [
            'Sunnydevbox\TWPim\Listeners\PayrollRecalculateListener',
        ],
        
        'Sunnydevbox\TWPim\Events\PayrollItemUpdatedEvent' => [
            'Sunnydevbox\TWPim\Listeners\PayrollRecalculateListener',
        ],

        'Sunnydevbox\TWPim\Events\CashAdvanceCreatedEvent' => [
            'Sunnydevbox\TWPim\Listeners\CashAdvanceCreateLogListener',
        ],

        'Sunnydevbox\TWPim\Events\CashAdvanceUpdatedEvent' => [
            'Sunnydevbox\TWPim\Listeners\CashAdvanceBalanceSettledListener',
        ],

    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
