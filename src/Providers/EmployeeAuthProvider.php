<?php
namespace Sunnydevbox\TWPim\Providers;

use Illuminate\Http\Request;
use Dingo\Api\Routing\Route;
use Dingo\Api\Contract\Auth\Provider;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Dingo\Api\Auth\Provider\JWT;

// LET's just extend the existing JWT provider from Dingo
// use Dingo\Api\Auth\Provider\JWT;
class EmployeeAuthProvider extends JWT
{
    public function authenticate(Request $request, Route $route)
    {
        // Logic to authenticate the request.
        // \Config::set('auth.providers.users.model', \Sunnydevbox\TWPim\Models\Employee::class);
        // \Config::set('jwt.user', \Sunnydevbox\TWPim\Models\Employee::class);
        if ($token = \JWTAuth::getToken()) {
            $token = $token->get();
            try {
                $payload = $this->auth->getPayload($token);

                if (! $employee = \Sunnydevbox\TWPim\Models\Employee::find($payload['sub'])) {
                    throw new UnauthorizedHttpException('JWTAuth', 'Unable to authenticate with invalid token.');
                }
            } catch (JWTException $exception) {
                throw new UnauthorizedHttpException('JWTAuth', $exception->getMessage(), $exception);
            }

            return $employee;
        } else {
            abort(401);
        }


        
    }

    public function getAuthorizationMethod()
    {
        return 'bearer';
    }
}