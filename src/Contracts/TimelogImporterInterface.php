<?php
namespace Sunnydevbox\TWPim\Contracts;
 
interface TimelogImporterInterface
{
    /**
     * Verify that the file is in valid csv format
     *
     * @param File $file
     * @return void
     */
    public function verifier();

    /**
     * Beging traversing csv
     *
     * @return void
     */
    public function parse();

    /**
     * Row parser
     *
     * @return void
     */
    public function parseRow($row);

    public function process();

}