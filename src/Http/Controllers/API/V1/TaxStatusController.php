<?php

namespace Sunnydevbox\TWPim\Http\Controllers\API\V1;

use Sunnydevbox\TWCore\Http\Controllers\APIBaseController;

class TaxStatusController extends APIBaseController
{
	public function __construct(
		\Sunnydevbox\TWPim\Repositories\TaxStatus\TaxStatusRepository $repository,
		\Sunnydevbox\TWPim\Transformers\TaxStatusTransformer $transformer,
		\Sunnydevbox\TWPim\Validators\TaxStatusValidator $validator
	) {
		$this->transformer = $transformer;
		$this->repository = $repository;
		$this->validator = $validator;
	}

}