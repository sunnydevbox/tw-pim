<?php
namespace Sunnydevbox\TWPim\Http\Controllers\API\V1;

use Sunnydevbox\TWCore\Http\Controllers\APIBaseController;

class EmployeeBenefitController extends APIBaseController
{
	public function __construct(
		\Sunnydevbox\TWPim\Repositories\Employee\EmployeeBenefitRepository $repository,
		\Sunnydevbox\TWPim\Transformers\EmployeeBenefitTransformer $transformer,
		\Sunnydevbox\TWPim\Validators\EmployeeBenefitValidator $validator
	) {
		$this->transformer = $transformer;
		$this->repository = $repository;
		$this->validator = $validator;
	}

}