<?php
namespace Sunnydevbox\TWPim\Http\Controllers\API\V1;

use Sunnydevbox\TWCore\Http\Controllers\APIBaseController;

class LeaveTypeController extends APIBaseController
{
    public function __construct(
        \Sunnydevbox\TWPim\Repositories\LeaveApplication\LeaveTypeRepository $repository,
        \Sunnydevbox\TWPim\Transformers\LeaveTypeTransformer $transformer,
        \Sunnydevbox\TWPim\Validators\LeaveTypeValidator $validator
    ) {
        $this->transformer = $transformer;
        $this->repository = $repository;
        $this->validator = $validator;
    }
}
