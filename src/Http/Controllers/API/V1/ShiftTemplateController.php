<?php
namespace Sunnydevbox\TWPim\Http\Controllers\API\V1;

use Sunnydevbox\TWCore\Http\Controllers\APIBaseController;

class ShiftTemplateController extends APIBaseController
{
	public function __construct(
		\Sunnydevbox\TWPim\Repositories\ShiftTemplate\ShiftTemplateRepository $repository,
		\Sunnydevbox\TWPim\Transformers\ShiftTemplateTransformer $transformer,
		\Sunnydevbox\TWPim\Validators\ShiftTemplateValidator $validator
	) {
		$this->transformer = $transformer;
		$this->repository = $repository;
		$this->validator = $validator;
	}
}
