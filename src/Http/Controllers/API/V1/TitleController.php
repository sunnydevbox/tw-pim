<?php

namespace Sunnydevbox\TWPim\Http\Controllers\API\V1;

use Sunnydevbox\TWCore\Http\Controllers\APIBaseController;

class TitleController extends APIBaseController
{
	public function __construct(
		\Sunnydevbox\TWPim\Repositories\Title\TitleRepository $repository,
		\Sunnydevbox\TWPim\Transformers\TitleTransformer $transformer,
		\Sunnydevbox\TWPim\Validators\TitleValidator $validator
	) {
		$this->transformer = $transformer;
		$this->repository = $repository;
		$this->validator = $validator;
	}

}