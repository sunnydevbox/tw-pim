<?php

namespace Sunnydevbox\TWPim\Http\Controllers\API\V1;

use Sunnydevbox\TWCore\Http\Controllers\APIBaseController;

class EmployeeTemplateController extends APIBaseController
{
    public function __construct(
        \Sunnydevbox\TWPim\Repositories\Employee\EmployeeTemplateRepository $repository,
        \Sunnydevbox\TWPim\Transformers\EmployeeTemplateTransformer $transformer,
        \Sunnydevbox\TWPim\Validators\EmployeeTemplateValidator $validator
    ) {
        $this->transformer = $transformer;
        $this->repository = $repository;
        $this->validator = $validator;
    }
}
