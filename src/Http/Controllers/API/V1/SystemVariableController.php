<?php

namespace Sunnydevbox\TWPim\Http\Controllers\API\V1;

use Sunnydevbox\TWCore\Http\Controllers\APIBaseController;

class SystemVariableController extends APIBaseController
{
	public function __construct(
		\Sunnydevbox\TWPim\Repositories\SystemVariable\SystemVariableRepository $repository,
		\Sunnydevbox\TWPim\Transformers\SystemVariableTransformer $transformer,
		\Sunnydevbox\TWPim\Validators\SystemVariableValidator $validator,
		\Sunnydevbox\TWPim\Services\SystemVariableService $service
	) {
		$this->transformer = $transformer;
		$this->repository = $repository;
		$this->validator = $validator;
		$this->service = $service;
	}
}