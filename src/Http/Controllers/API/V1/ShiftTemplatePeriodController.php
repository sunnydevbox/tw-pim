<?php
namespace Sunnydevbox\TWPim\Http\Controllers\API\V1;

use Dingo\Api\Http\Request;
use Sunnydevbox\TWCore\Http\Controllers\APIBaseController;

class ShiftTemplatePeriodController extends APIBaseController
{
	public function __construct(
		\Sunnydevbox\TWPim\Repositories\ShiftTemplatePeriod\ShiftTemplatePeriodRepository $repository,
		\Sunnydevbox\TWPim\Transformers\ShiftTemplatePeriodTransformer $transformer,
		\Sunnydevbox\TWPim\Validators\ShiftTemplatePeriodValidator $validator,
		\Sunnydevbox\TWPim\Services\ShiftTemplatePeriodService $service
	) {
		$this->transformer = $transformer;
		$this->repository = $repository;
		$this->validator = $validator;
		$this->service = $service;
	}

	public function perEmployee(Request $request, $id)
	{
		$result = $this->service->perEmployee($request->data, $id);

		return $this->response->collection($result, $this->transformer);
	}
}
