<?php

namespace Sunnydevbox\TWPim\Http\Controllers\API\V1;

use Sunnydevbox\TWCore\Http\Controllers\APIBaseController;
use Dingo\Api\Http\Request;

class TableTaxController extends APIBaseController
{
	public function __construct(
		\Sunnydevbox\TWPim\Repositories\Table\TaxRepository $repository,
		\Sunnydevbox\TWPim\Transformers\TableTaxTransformer $transformer,
		\Sunnydevbox\TWPim\Validators\TableTaxValidator $validator,
		\Sunnydevbox\TWPim\Services\TableTaxService $service
	) {
		$this->transformer = $transformer;
		$this->repository = $repository;
		$this->validator = $validator;
		$this->service = $service;
	}

	public function getRange(Request $request)
	{
		$result = $this->service->getRange($request->get('period'), $request->get('salary'));

		dd($result);
	}
}