<?php
namespace Sunnydevbox\TWPim\Http\Controllers\API\V1;

use Sunnydevbox\TWCore\Http\Controllers\APIBaseController;

class JobPositionController extends APIBaseController
{
	public function __construct(
		\Sunnydevbox\TWPim\Repositories\JobPosition\JobPositionRepository $repository,
		\Sunnydevbox\TWPim\Transformers\JobPositionTransformer $transformer,
		\Sunnydevbox\TWPim\Validators\JobPositionValidator $validator
	) {
		$this->transformer = $transformer;
		$this->repository = $repository;
		$this->validator = $validator;
	}

}