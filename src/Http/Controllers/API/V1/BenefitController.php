<?php
namespace Sunnydevbox\TWPim\Http\Controllers\API\V1;

use Sunnydevbox\TWCore\Http\Controllers\APIBaseController;

class BenefitController extends APIBaseController
{
	public function __construct(
		\Sunnydevbox\TWPim\Repositories\Benefit\BenefitRepository $repository,
		\Sunnydevbox\TWPim\Transformers\BenefitTransformer $transformer,
		\Sunnydevbox\TWPim\Validators\BenefitValidator $validator
	) {
		$this->transformer = $transformer;
		$this->repository = $repository;
		$this->validator = $validator;
	}

}
