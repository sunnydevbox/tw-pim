<?php

namespace Sunnydevbox\TWPim\Http\Controllers\API\V1;

use Sunnydevbox\TWCore\Http\Controllers\APIBaseController;

class EmploymentStatusController extends APIBaseController
{
	public function __construct(
		\Sunnydevbox\TWPim\Repositories\Employee\EmploymentStatusRepository $repository,
		\Sunnydevbox\TWPim\Transformers\EmploymentStatusTransformer $transformer,
		\Sunnydevbox\TWPim\Validators\EmploymentStatusValidator $validator
	) {
		$this->transformer = $transformer;
		$this->repository = $repository;
		$this->validator = $validator;
	}

}