<?php

namespace Sunnydevbox\TWPim\Http\Controllers\API\V1;

use Sunnydevbox\TWCore\Http\Controllers\APIBaseController;
use Dingo\Api\Http\Request;
use \Prettus\Validator\Exceptions\ValidatorException;
use \Prettus\Validator\Contracts\ValidatorInterface;

class TimelogController extends APIBaseController
{
    public function __construct(
        \Sunnydevbox\TWPim\Repositories\Timelog\TimelogRepository $repository,
        \Sunnydevbox\TWPim\Transformers\TimelogTransformer $transformer,
        \Sunnydevbox\TWPim\Validators\TimelogValidator $validator,
        \Sunnydevbox\TWPim\Services\UnitedRebuildersImportService $service,
        \Sunnydevbox\TWPim\Services\TimelogService $timelogService
    ) {
        $this->transformer = $transformer;
        $this->repository = $repository;
        $this->validator = $validator;
        $this->service = $service;
        $this->timelogService = $timelogService;
    }

    public function import(Request $request)
    {
        $result = $this->service->setFile($request->file('dtr_csv'))->process();
        return $this->response->noContent();
    }

    public function setStatus($timelogId, $action)
    {
        $result = $this->timelogService->setStatus($timelogId, $action);
        
        $result = $this->repository
                            ->with('shift_template_period.shift_template')
                            ->find($timelogId);

        return $this->response->item($result, $this->transformer);
    }

    public function recalculate(Request $request, $id)
    {
        try {
            $result = $this->timelogService->recalculate($request->all(), $id);

            return $this->response->item($result, $this->transformer);
        } catch (ValidatorException $e) {

            return response()->json([
                'status_code'   => 400,
                'message' =>$e->getMessageBag()
            ], 400);
        }
    }

    public function update(Request $request, $id)
	{
		try {
            $result = $this->timelogService->update($request->all(), $id);
            return $this->response->item($result, $this->transformer);
        } catch (ValidatorException $e) {

            return response()->json([
                'status_code'   => 400,
                'message' =>$e->getMessageBag()
            ], 400);
        }
	}
}
