<?php
namespace Sunnydevbox\TWPim\Http\Controllers\API\V1;

use Sunnydevbox\TWCore\Http\Controllers\Controller;
use Sunnydevbox\TWCore\Http\Controllers\APIBaseController;
use Dingo\Api\Http\Request;

class PhilhealthController extends APIBaseController
{
    public function __construct(
        \Sunnydevbox\TWPim\Repositories\Philhealth\PhilhealthRepository $philhealthRepository,
        \Sunnydevbox\TWPim\Transformers\PhilhealthTransformer $philhealthTransformer,
        \Sunnydevbox\TWPim\Validators\PhilhealthValidator $validator
    ) {
        $this->repository = $philhealthRepository;
        $this->transformer = $philhealthTransformer;
        $this->validator = $validator;
    }
}
