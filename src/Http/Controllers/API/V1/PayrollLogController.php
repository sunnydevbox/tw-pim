<?php
namespace Sunnydevbox\TWPim\Http\Controllers\API\V1;

use Sunnydevbox\TWCore\Http\Controllers\APIBaseController;
use Dingo\Api\Http\Request;

class PayrollLogController extends APIBaseController
{
    public function store(Request $request)
	{
		 try {
            $result = $this->service->store($request->all());
            
            return $result;
        } catch (ValidatorException $e) {

            return response()->json([
                'status_code'   => 400,
                'message' =>$e->getMessageBag()
            ], 400);
        }
    }
    
    public function update(Request $request, $id)
	{
		 try {
            $result = $this->service->update($request->all(), $id);
            
            return $result;
        } catch (ValidatorException $e) {

            return response()->json([
                'status_code'   => 400,
                'message' =>$e->getMessageBag()
            ], 400);
        }
	}

    public function generate(Request $request)
    {
        $payrollLog = $this->repository
                            ->with(['payroll_template.employees'])
                            ->find($request->get('payroll_log_id'));
        if ($payrollLog) {
            if ($payrollLog->isDone()) {
                return response()->json([
                    'status_code'   => 200,
                    'message'       => 'payroll_previously_generated'
                ], 200);
            }

            if ($payrollLog->isProcessing()) {
                return response()->json([
                    'status_code'   => 200,
                    'message'       => 'payroll_currently_processing'
                ], 200);
            }

            if ($payrollLog->isPending()) {
                $result = $this->service->generate($request->all());
                //event(new \Sunnydevbox\TWPim\Events\PayrollGenerateEvent($request->all()));
                return response()->json([
                    'status_code'   => $result['status_code'],
                    'message'       => $result['message']
                ], 200);
            }
        }

        // $result = $this->service->generate($request->all());

        // if (isset($result['status_code'])) {
        //     return response()->json($result, $result['status_code']);
        // }
    }

    public function __construct(
        \Sunnydevbox\TWPim\Repositories\Payroll\PayrollLogRepository $repository,
        \Sunnydevbox\TWPim\Transformers\PayrollLogTransformer $transformer,
        \Sunnydevbox\TWPim\Validators\PayrollLogValidator $validator,
        \Sunnydevbox\TWPim\Services\PayrollLogService $service
    ) {
        $this->transformer = $transformer;
        $this->repository = $repository;
        $this->validator = $validator;
        $this->service = $service;
    }
}
