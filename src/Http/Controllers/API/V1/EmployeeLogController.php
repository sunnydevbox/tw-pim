<?php

namespace Sunnydevbox\TWPim\Http\Controllers\API\V1;

use Sunnydevbox\TWCore\Http\Controllers\APIBaseController;

class EmployeeLogController extends APIBaseController
{
	public function __construct(
		\Sunnydevbox\TWPim\Repositories\Employee\EmployeeLogRepository $repository,
		\Sunnydevbox\TWPim\Transformers\EmployeeLogTransformer $transformer,
		\Sunnydevbox\TWPim\Validators\EmployeeLogValidator $validator
	) {
		$this->transformer = $transformer;
		$this->repository = $repository;
		$this->validator = $validator;
	}

}