<?php
namespace Sunnydevbox\TWPim\Http\Controllers\API\V1;

use Sunnydevbox\TWCore\Http\Controllers\APIBaseController;
use Dingo\Api\Http\Request;
use Prettus\Validator\Exceptions\ValidatorException;

class PayrollItemController extends APIBaseController
{
    public function customEntry(Request $request)
    {
        try {
            $result = $this->service->customEntry($request->all());
            return $result;

        } catch (ValidatorException $e) {

            return response()->json([
                'status_code'   => 400,
                'message' =>$e->getMessageBag()
            ], 400);
		}
        
    }

    public function __construct(
        \Sunnydevbox\TWPim\Repositories\Payroll\PayrollItemRepository $repository,
        \Sunnydevbox\TWPim\Transformers\PayrollItemTransformer $transformer,
        \Sunnydevbox\TWPim\Validators\PayrollItemValidator $validator,
        \Sunnydevbox\TWPim\Services\PayrollItemService $service
    ) {
        $this->transformer = $transformer;
        $this->repository = $repository;
        $this->validator = $validator;
        $this->service = $service;
    }
}
