<?php
namespace Sunnydevbox\TWPim\Http\Controllers\API\V1;

use Sunnydevbox\TWCore\Http\Controllers\APIBaseController;
use Dingo\Api\Http\Request;

class PayrollTemplateController extends APIBaseController
{
    public function __construct(
        \Sunnydevbox\TWPim\Repositories\Payroll\PayrollTemplateRepository $repository,
        \Sunnydevbox\TWPim\Transformers\PayrollTemplateTransformer $transformer,
        \Sunnydevbox\TWPim\Validators\PayrollTemplateValidator $validator,
        \Sunnydevbox\TWPim\Services\PayrollTemplateService $service
    ) {
        $this->transformer = $transformer;
        $this->repository = $repository;
        $this->validator = $validator;
        $this->service = $service;
    }

    public function periodSelections(Request $request)
    {
        $result = $this->service->periodSelections($request->all());
        if (isset($result->error)) {
            return $this->response->noContent();    
        }
        
        return $this->response->noContent();        
    } 
}
