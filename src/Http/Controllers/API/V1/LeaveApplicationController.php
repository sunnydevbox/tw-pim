<?php

namespace Sunnydevbox\TWPim\Http\Controllers\API\V1;

use Sunnydevbox\TWCore\Http\Controllers\APIBaseController;

class LeaveApplicationController extends APIBaseController
{

    public function setStatus($leaveApplicationId, $action)
    {
        $result = $this->service->setStatus($leaveApplicationId, $action);
        
        return $this->response->item($result, $this->transformer);
    }

    public function __construct(
        \Sunnydevbox\TWPim\Repositories\LeaveApplication\LeaveApplicationRepository $repository,
        \Sunnydevbox\TWPim\Transformers\LeaveApplicationTransformer $transformer,
        \Sunnydevbox\TWPim\Validators\LeaveApplicationValidator $validator,
        \Sunnydevbox\TWPim\Services\LeaveApplicationService $service
    ) {
        $this->transformer = $transformer;
        $this->repository = $repository;
        $this->validator = $validator;
        $this->service = $service;
    }
}
