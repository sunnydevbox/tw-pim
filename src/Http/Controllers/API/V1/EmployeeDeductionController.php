<?php

namespace Sunnydevbox\TWPim\Http\Controllers\API\V1;

use Dingo\Api\Http\Request;
use Sunnydevbox\TWCore\Http\Controllers\APIBaseController;

class EmployeeDeductionController extends APIBaseController
{
	public function __construct(
		\Sunnydevbox\TWPim\Repositories\Employee\EmployeeDataDeductionRepository $repository,
		\Sunnydevbox\TWPim\Transformers\EmployeeDeductionTransformer $transformer,
		\Sunnydevbox\TWPim\Validators\EmployeeDeductionValidator $validator
	) {
		$this->transformer = $transformer;
		$this->repository = $repository;
		$this->validator = $validator;
	}

	public function attachDeduction(Request $request)
	{
		$result = $this->repository->firstOrCreate([
			'employee_id' => $request->get('employee_id'),
			'deduction_id' => $request->get('deduction_id'),
		]);
		
		return $this->response->noContent();
	}

	public function detachDeduction(Request $request)
	{
		$result = $this->repository->deleteWhere([
			'employee_id' => $request->get('employee_id'),
			'deduction_id' => $request->get('deduction_id'),
		]);
		return $this->response->noContent();
	}

}