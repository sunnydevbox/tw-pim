<?php

namespace Sunnydevbox\TWPim\Http\Controllers\API\V1;

use Dingo\Api\Http\Request;
use Sunnydevbox\TWCore\Http\Controllers\APIBaseController;

class HolidayController extends APIBaseController
{
    public function monthList(Request $request)
    {
        try {
            $result = $this->service->monthList($request->all());
            return $this->response->collection($result, $this->transformer);
        } catch (\Exception $e) {
            // dd(1,$e);
            return response()->json([
                'status_code'   => 400,
                'message' => $e->getMessageBag()
            ], 400);
        }
    }

    public function __construct(
        \Sunnydevbox\TWPim\Repositories\Holiday\HolidayRepository $repository,
        \Sunnydevbox\TWPim\Transformers\HolidayTransformer $transformer,
        \Sunnydevbox\TWPim\Validators\HolidayValidator $validator,
        \Sunnydevbox\TWPim\Services\HolidayService $service
    ) {
        $this->transformer = $transformer;
        $this->repository = $repository;
        $this->validator = $validator;
        $this->service = $service;
    }
}
