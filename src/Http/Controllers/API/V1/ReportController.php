<?php

namespace Sunnydevbox\TWPim\Http\Controllers\API\V1;

use Sunnydevbox\TWCore\Http\Controllers\APIBaseController;
use Dingo\Api\Http\Request;

class ReportController extends APIBaseController
{
	public function getPayrolls(Request $request)
	{
		return $this->service->getPayrolls($request->all());
		$limit = is_numeric($request->get('limit')) ? $request->get('limit') : config('repository.pagination.limit', 15);
		if ($limit == 0) {
			$result = $this->repository->all();

			return $this->response()
					->collection($result, $this->transformer)
					->withHeader('Content-Range', $result->count());
		} else {
			$result = $this->payrollRepository->paginate($limit);

			// dd($result);
			

			return $this->response
				->paginator($result, $this->transformer)
				->withHeader('Content-Range', $result->total());
		}

		return $result;
	}


	public function __construct(
		\Sunnydevbox\TWPim\Repositories\Table\TaxRepository $repository,
		\Sunnydevbox\TWPim\Transformers\TableTaxTransformer $transformer,
		\Sunnydevbox\TWPim\Validators\TableTaxValidator $validator,
		\Sunnydevbox\TWPim\Services\ReportService $service,
		\Sunnydevbox\TWPim\Repositories\Payroll\PayrollRepository $payrollRepository
	) {
		$this->transformer = $transformer;
		$this->repository = $repository;
		$this->validator = $validator;
		$this->service = $service;
		$this->payrollRepository = $payrollRepository;
	}
}