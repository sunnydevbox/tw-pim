<?php

namespace Sunnydevbox\TWPim\Http\Controllers\API\V1;

use Sunnydevbox\TWCore\Http\Controllers\APIBaseController;
use Sunnydevbox\TWUser\Traits\HasRolePermissionControllerTrait;
use Dingo\Api\Http\Request;
use \Prettus\Validator\Exceptions\ValidatorException;
use \Prettus\Validator\Contracts\ValidatorInterface;
use Sunnydevbox\TWPim\Traits\HasShiftTemplateControllerTrait;
use Sunnydevbox\TWPim\Traits\HasPayrollTemplateControllerTrait;

class EmployeeController extends APIBaseController
{
    use HasRolePermissionControllerTrait;
    use HasShiftTemplateControllerTrait;
    use HasPayrollTemplateControllerTrait;

    public function __construct(
        \Sunnydevbox\TWPim\Repositories\Employee\EmployeeRepository $repository,
        \Sunnydevbox\TWPim\Transformers\EmployeeTransformer $transformer,
        \Sunnydevbox\TWPim\Validators\EmployeeValidator $validator,
        \Sunnydevbox\TWPim\Services\EmployeeDataService $dataService
    ) {
        $this->transformer = $transformer;
        $this->repository = $repository;
        $this->validator = $validator;
        // $this->rpoUser = $repository;
        $this->dataService = $dataService;
    }

    public function store(Request $request)
	{
		 try {
			
			if (isset($this->validator) && $this->validator) {
				$this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_CREATE);
			}
			
		    $result = $this->repository->store( $request->all() );

            return $this->response->item($result, $this->transformer);

        } catch (ValidatorException $e) {

            return response()->json([
                'status_code'   => 400,
                'message' =>$e->getMessageBag()
            ], 400);
        }
    }
    
    public function update(Request $request, $id)
	{
		try {
			
			if (isset($this->validator) && $this->validator) {
				$this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_UPDATE);
			}
           
            if ($request->has('type')) {
                $result = $this->dataService->process($request->all(), $id);
            } else {
                $result = $this->repository->update($request->all(), $id);
            }
            
            return $this->response
                ->item(
                        $result, 
                $this->transformer);

        } catch (ValidatorException $e) {

            return response()->json([
                'status_code'   => 400,
                'message' =>$e->getMessageBag()
            ], 400);
        }
    }
    
    public function attachBenefit(Request $request)
    {
        return $this->dataService->attachBenefit($request->all());
    }

    public function detachBenefit(Request $request)
    {
        return $this->dataService->detachBenefit($request->all());
        
    }

    public function attachDeduction(Request $request)
    {
        return $this->dataService->attachDeduction($request->all());
    }

    public function detachDeduction(Request $request)
    {
        return $this->dataService->detachDeduction($request->all());
        
    }
}
