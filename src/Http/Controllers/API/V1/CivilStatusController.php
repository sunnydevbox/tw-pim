<?php

namespace Sunnydevbox\TWPim\Http\Controllers\API\V1;

use Sunnydevbox\TWCore\Http\Controllers\APIBaseController;

class CivilStatusController extends APIBaseController
{
	public function __construct(
		\Sunnydevbox\TWPim\Repositories\CivilStatus\CivilStatusRepository $repository,
		\Sunnydevbox\TWPim\Transformers\CivilStatusTransformer $transformer,
		\Sunnydevbox\TWPim\Validators\CivilStatusValidator $validator
	) {
		$this->transformer = $transformer;
		$this->repository = $repository;
		$this->validator = $validator;
	}

}