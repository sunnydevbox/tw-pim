<?php

namespace Sunnydevbox\TWPim\Http\Controllers\API\V1;

use Sunnydevbox\TWCore\Http\Controllers\APIBaseController;
use Dingo\Api\Http\Request;
use \Prettus\Validator\Exceptions\ValidatorException;

class CashAdvanceController extends APIBaseController
{
	public function __construct(
		\Sunnydevbox\TWPim\Repositories\Deduction\CashAdvanceRepository $repository,
		\Sunnydevbox\TWPim\Transformers\CashAdvanceTransformer $transformer,
		\Sunnydevbox\TWPim\Validators\CashAdvanceValidator $validator,
		\Sunnydevbox\TWPim\Services\CashAdvanceService $service
	) {
		$this->transformer = $transformer;
		$this->repository = $repository;
		$this->validator = $validator;
		$this->service = $service;
	}

	public function setStatus(Request $request)
	{
		try {
			
			$result = $this->service->setStatus($request->all());
			// $result = $this->service->makeDebit($request->all());

            return $result;//$this->response->item($result, $this->transformer);

        } catch (ValidatorException $e) {

            return response()->json([
                'status_code'   => 400,
                'message' =>$e->getMessageBag()
            ], 400);
		}
	}
}