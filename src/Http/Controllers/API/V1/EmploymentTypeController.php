<?php

namespace Sunnydevbox\TWPim\Http\Controllers\API\V1;

use Sunnydevbox\TWCore\Http\Controllers\APIBaseController;

class EmploymentTypeController extends APIBaseController
{
	public function __construct(
		\Sunnydevbox\TWPim\Repositories\Employee\EmploymentTypeRepository $repository,
		\Sunnydevbox\TWPim\Transformers\EmploymentStatusTransformer $transformer,
		\Sunnydevbox\TWPim\Validators\EmploymentStatusValidator $validator
	) {
		$this->transformer = $transformer;
		$this->repository = $repository;
		$this->validator = $validator;
	}

}