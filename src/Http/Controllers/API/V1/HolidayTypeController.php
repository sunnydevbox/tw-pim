<?php

namespace Sunnydevbox\TWPim\Http\Controllers\API\V1;

use Sunnydevbox\TWCore\Http\Controllers\APIBaseController;

class HolidayTypeController extends APIBaseController
{
    public function __construct(
        \Sunnydevbox\TWPim\Repositories\Holiday\HolidayTypeRepository $repository,
        \Sunnydevbox\TWPim\Transformers\HolidayTypeTransformer $transformer,
        \Sunnydevbox\TWPim\Validators\HolidayTypeValidator $validator
    ) {
        $this->transformer = $transformer;
        $this->repository = $repository;
        $this->validator = $validator;
    }
}
