<?php

namespace Sunnydevbox\TWPim\Http\Controllers\API\V1;

use Sunnydevbox\TWCore\Http\Controllers\APIBaseController;

class DeductionController extends APIBaseController
{
	public function __construct(
		\Sunnydevbox\TWPim\Repositories\Deduction\DeductionRepository $repository,
		\Sunnydevbox\TWPim\Transformers\DeductionTransformer $transformer,
		\Sunnydevbox\TWPim\Validators\DeductionValidator $validator
	) {
		$this->transformer = $transformer;
		$this->repository = $repository;
		$this->validator = $validator;
	}

}