<?php
namespace Sunnydevbox\TWPim\Http\Controllers\API\V1;

use Sunnydevbox\TWCore\Http\Controllers\Controller;
use Sunnydevbox\TWCore\Http\Controllers\APIBaseController;
use Dingo\Api\Http\Request;

class SssDeductionController extends APIBaseController
{

    public function __construct(
        \Sunnydevbox\TWPim\Repositories\Sss\SssDeductionRepository $repository,
        \Sunnydevbox\TWPim\Transformers\SssDeductionTransformer $transformer,
        \Sunnydevbox\TWPim\Validators\SssDeductionValidator $validator
    ) {
        $this->repository = $repository;
        $this->transformer = $transformer;
        $this->validator = $validator;
    }
}
