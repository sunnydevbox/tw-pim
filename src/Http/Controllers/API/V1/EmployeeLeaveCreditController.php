<?php

namespace Sunnydevbox\TWPim\Http\Controllers\API\V1;

use Sunnydevbox\TWCore\Http\Controllers\APIBaseController;

class EmployeeLeaveCreditController extends APIBaseController
{
    public function __construct(
        \Sunnydevbox\TWPim\Repositories\Employee\EmployeeLeaveCreditRepository $repository,
        \Sunnydevbox\TWPim\Transformers\EmployeeLeaveCreditTransformer $transformer,
        \Sunnydevbox\TWPim\Validators\EmployeeLeaveCreditValidator $validator,
        \Sunnydevbox\TWPim\Services\EmployeeLeaveCreditService $service
    ) {
        $this->transformer = $transformer;
        $this->repository = $repository;
        $this->validator = $validator;
        $this->service = $service;
    }
}
