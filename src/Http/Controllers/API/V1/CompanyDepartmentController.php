<?php

namespace Sunnydevbox\TWPim\Http\Controllers\API\V1;

use Sunnydevbox\TWCore\Http\Controllers\APIBaseController;

class CompanyDepartmentController extends APIBaseController
{
	public function __construct(
		\Sunnydevbox\TWPim\Repositories\CompanyDepartment\CompanyDepartmentRepository $repository,
		\Sunnydevbox\TWPim\Transformers\CompanyDepartmentTransformer $transformer,
		\Sunnydevbox\TWPim\Validators\CompanyDepartmentValidator $validator
	) {
		$this->transformer = $transformer;
		$this->repository = $repository;
		$this->validator = $validator;
	}

}