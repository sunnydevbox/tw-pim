<?php
namespace Sunnydevbox\TWPim\Http\Controllers\API\V1;

use Sunnydevbox\TWCore\Http\Controllers\APIBaseController;
use Dingo\Api\Http\Request;

class PayrollController extends APIBaseController
{
    public function generatePayslip(\Illuminate\Http\Request $request)
    {   
       return $this->service->generatePayslip($request->all());
    }

    public function recalculate(Request $request)
    {
        $result = $this->service->recalculate($request->all());

        return $result;
    }

    public function __construct(
        \Sunnydevbox\TWPim\Repositories\Payroll\PayrollRepository $repository,
        \Sunnydevbox\TWPim\Transformers\PayrollTransformer $transformer,
        \Sunnydevbox\TWPim\Validators\PayrollValidator $validator,
        \Sunnydevbox\TWPim\Services\PayrollService $service
    ) {
        $this->transformer = $transformer;
        $this->repository = $repository;
        $this->validator = $validator;
        $this->service = $service;
    }
}
