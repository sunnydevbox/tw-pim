<?php

namespace Sunnydevbox\TWPim\Http\Controllers\API\V1;

use Sunnydevbox\TWCore\Http\Controllers\APIBaseController;

class CashAdvanceLogController extends APIBaseController
{
	public function __construct(
		\Sunnydevbox\TWPim\Repositories\Deduction\CashAdvanceLogRepository $repository,
		\Sunnydevbox\TWPim\Transformers\CashAdvanceLogTransformer $transformer,
		\Sunnydevbox\TWPim\Validators\CashAdvanceLogValidator $validator
	) {
		$this->transformer = $transformer;
		$this->repository = $repository;
		$this->validator = $validator;
	}

}