<?php

namespace Sunnydevbox\TWPim;

use Sunnydevbox\TWCore\BaseServiceProvider;
use Illuminate\Support\Facades\Validator;

class TWPimServiceProvider extends BaseServiceProvider
{
    public function boot()
    {
        parent::boot();
        
        Validator::resolver(function($translator, $data, $rules, $messages)
        {
            // $messages[] = 
            //     'Invalid period format'
            // ;
            
            return new \Sunnydevbox\TWPim\Validators\CustomRules\PayrollTemplateMonthDays(
                $translator, $data, $rules, $messages);
        });

        // $this->publishes([
        //     __DIR__.'/../database/migrations/' => database_path('migrations')
        // ], 'migrations');

        // $this->publishes([
        //     __DIR__.'/../resources/views' => base_path('resources/views/recoveryhub/'),
        // ],'resources');

        // $this->publishes([
        //     __DIR__.'/../assets' => public_path('vendor/recoveryhub'),
        // ],'assets');
        $this->loadMigrationsFrom(__DIR__.'/../database/migrations');
            
        $this->loadTranslationsFrom(__DIR__ . '/../resources/lang', 'tw-pim');
        
        // $this->publishes([
        //     __DIR__.'/../resources/views' => resource_path('views/vendor/tw-pim'),
        // ], 'views');

        // dd(resource_path('vendor/tw-pim'));
        // $this->loadViewsFrom(resource_path('vendor/tw-pim'), 'twpim');

        Validator::extend('gte', function($attribute, $value, $parameters, $validator) {
            $min_field = $parameters[0];
            $data = $validator->getData();
            $min_value = $data[$min_field];
            
            return $value >= $min_value;
          });   

    }

    public function mergeConfig()
    {
        return [
           realpath(__DIR__ . '/../config/config.php') => 'tw-pim'
        ];
    }

    public function loadRoutes()
    {
        return [
            realpath(__DIR__.'/../routes/api.php')
        ];
    }

    public function loadViews()
    {
        return [
            __DIR__.'/../resources/views' => 'tw-pim',
        ];
    }
    
    public function registerProviders()
    {
        // EVENTS
        if (class_exists('\Sunnydevbox\TWPim\EventServiceProvider')
            && !$this->app->resolved('\Sunnydevbox\TWPim\EventServiceProvider')
        ) {
            $this->app->register(\Sunnydevbox\TWPim\EventServiceProvider::class);    
        }

        if (class_exists('\Sunnydevbox\TWUser\TWUserServiceProvider')
            && !$this->app->resolved('\Sunnydevbox\TWUser\TWUserServiceProvider')
        ) {
            $this->app->register(\Sunnydevbox\TWUser\TWUserServiceProvider::class);    
        }
    }

    public function registerCommands()
    {
        if ($this->app->runningInConsole()) {

            $this->commands([
                \Sunnydevbox\TWPim\Console\Commands\PublishConfigCommand::class,
                \Sunnydevbox\TWPim\Console\Commands\MigrateCommand::class,
            ]);
        }
    }

    public function toPublishConfig()
    {
        return [
            __DIR__ . '/../config/config.php' => config_path('tw-pim.php'),
        ];

    }
}