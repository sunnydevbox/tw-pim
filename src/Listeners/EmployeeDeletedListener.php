<?php

namespace Sunnydevbox\TWPim\Listeners;


use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;

class EmployeeDeletedListener
{
    public $rpoEmployee;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(
        \Sunnydevbox\TWPim\Repositories\Employee\EmployeeRepository $rpoEmployee
    ) {
        $this->rpoEmployee = $rpoEmployee;
    }

    /**
     * Handle the event.
     *
     * @param  Event  $event
     * @return void
     */
    public function handle($event)
    {

    }
}
