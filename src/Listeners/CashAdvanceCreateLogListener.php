<?php

namespace Sunnydevbox\TWPim\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;

class CashAdvanceCreateLogListener
{
    
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    /**
     * Handle the event.
     *
     * @param  Event  $event
     * @return void
     */
    public function handle($event)
    {
        $event->cashAdvance;

        $rpo = app(config('tw-pim.repository_cash_advance_log'));
        $log = $rpo->makeModel();
        $log->cash_advance_id = $event->cashAdvance->id;
        $log->notes = 'CA record created';
        $log->save();   
    }
}
