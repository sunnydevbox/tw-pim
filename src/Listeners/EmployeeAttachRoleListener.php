<?php

namespace Sunnydevbox\TWPim\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;

class EmployeeAttachRoleListener
{
    public $rpoEmployeeLog;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(
        \Sunnydevbox\TWPim\Repositories\Employee\EmployeeLogRepository $rpoEmployeeLog
    ) {
        $this->rpoEmployeeLog = $rpoEmployeeLog;
    }

    /**
     * Handle the event.
     *
     * @param  Event  $event
     * @return void
     */
    public function handle($event)
    {
        $event->employee->assignRole('employee');

        event(new \Sunnydevbox\TWPim\Events\EmployeeLogEvent(
            'Role assigned', 
            $event->employee, 
            '"Employee" role assigned to account'
        ));
    }
}
