<?php

namespace Sunnydevbox\TWPim\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;
use Auth;

class TimelogImportEventListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(
        \Sunnydevbox\TWPim\Repositories\Timelog\TimelogImportRepository $timelogImportRepository
    ) {
        $this->timelogImportRepository = $timelogImportRepository;
    }

    /**
     * Handle the event.
     *
     * @param  Event  $event
     * @return void
     */
    public function handle($event)
    {
        $action = strtolower($event->action);

        
        switch($action) {

            case 'create':
                $this->timelogImportRepository->create([
                    'user_id' => Auth::user()->id,
                ]);
            break;

            case 'set-status':
                
            break;
        }


    }
}
