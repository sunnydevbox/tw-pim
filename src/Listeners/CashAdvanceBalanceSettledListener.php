<?php

namespace Sunnydevbox\TWPim\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;

class CashAdvanceBalanceSettledListener
{
    public $service;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(\Sunnydevbox\TWPim\Services\CashAdvanceService $service)
    {
        $this->service = $service;
    }

    /**
     * Handle the event.
     *
     * @param  Event  $event
     * @return void
     */
    public function handle($event)
    {
        // GENERATE LOG FILE    
    
        if (!$event->cashAdvance->hasBalance()) {
            $this->service->close(
                $event->cashAdvance, 
                [
                    'notes' => 'CA settled',
                ]
            );
        }
    }
}
