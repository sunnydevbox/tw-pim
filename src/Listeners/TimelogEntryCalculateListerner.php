<?php

namespace Sunnydevbox\TWPim\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;

class TimelogEntryCalculateListerner implements ShouldQueue
{
    public $rpoTimelog;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(
        \Sunnydevbox\TWPim\Repositories\Timelog\TimelogRepository $rpoTimelog
    ) {
        $this->rpoTimelog = $rpoTimelog;
    }

    /**
     * Handle the event.
     *
     * @param  Event  $event
     * @return void
     */
    public function handle($event)
    {
        //\Log::info('calucaltion: ' . $event->timelog->id);
        $this->rpoTimelog->calculate($event->timelog);
    }
}
