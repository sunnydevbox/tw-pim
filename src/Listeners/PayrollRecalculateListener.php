<?php

namespace Sunnydevbox\TWPim\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;


class PayrollRecalculateListener
{
    public $payrollRepository;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(
        \Sunnydevbox\TWPim\Services\PayrollService $payrollService
    ) {
        $this->payrollRepository = app(config('tw-pim.model_payroll'));
        $this->payrollService = $payrollService;
    }

    /**
     * Handle the event.
     *
     * @param  Event  $event
     * @return void
     */
    public function handle($event)
    {
        $this->payrollService->calculatePayroll($event->payrollId);
        
        // $this->payrollRepository->find
        // $this->rpoEmployee->deactivate($event->employee);

        // event(new \Sunnydevbox\TWPim\Events\EmployeeLogEvent(
        //     'Role assigned', 
        //     $event->employee, 
        //     '"Employee" role assigned to account'
        // ));
    }
}
