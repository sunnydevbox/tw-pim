<?php

namespace Sunnydevbox\TWPim\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;

class LogEmployeeActionListener
{
    public $rpoEmployeeLog;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(
        \Sunnydevbox\TWPim\Repositories\Employee\EmployeeLogRepository $rpoEmployeeLog
    ) {
        $this->rpoEmployeeLog = $rpoEmployeeLog;
    }

    /**
     * Handle the event.
     *
     * @param  Event  $event
     * @return void
     */
    public function handle($event)
    {
        $this->rpoEmployeeLog->log([
            'action'        => $event->action,
            'employee_id'   => $event->employee->id,
            'description'   => $event->description,
        ]);
    }
}
