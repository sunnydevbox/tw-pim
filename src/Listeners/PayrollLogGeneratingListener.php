<?php

namespace Sunnydevbox\TWPim\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;

class PayrollLogGeneratingListener implements ShouldQueue
{
    public $rpoEmployeeLog;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(
        \Sunnydevbox\TWPim\Services\PayrollLogService $service
    ) {
        $this->service = $service;
    }

    /**
     * Handle the event.
     *
     * @param  Event  $event
     * @return void
     */
    public function handle($event)
    {
        $this->service->generate($event->data);
    }
}
