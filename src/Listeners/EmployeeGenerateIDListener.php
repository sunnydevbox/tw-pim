<?php

namespace Sunnydevbox\TWPim\Listeners;

use Sunnydevbox\TWPim\Models\Employee;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;

class EmployeeGenerateIDListener
{
    public $rpoEmployeeId;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(
        \Sunnydevbox\TWPim\Repositories\Employee\EmployeeDataEmployeeIdRepository $rpoEmployeeId
    ) {
        $this->rpoEmployeeId = $rpoEmployeeId;
    }

    /**
     * Handle the event.
     *
     * @param  Event  $event
     * @return void
     */
    public function handle($event)
    {
        // BASED ON Prettus Repository Event Listener
        $employee = (method_exists($event, 'getModel') &&  $event->getModel() instanceof Employee) 
                    ? $event->getModel()
                    : $event->employee;

        if ($this->rpoEmployeeId->generateId($employee)) {
            event(new \Sunnydevbox\TWPim\Events\EmployeeLogEvent(
                'Employee ID',
                $employee,
                'Employee ID generated'
            ));
        }
    }
}
