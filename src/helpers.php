<?php

if (!function_exists('dayOfWeekConversion')) {
    
    /**
     * dayOfWeekConversion
     * 
     * MONDAY is start of week
     *
     * @param [type] $src
     * @return void
     */
    function dayOfWeekConversion($src)
    {
        $days = [
            1 => 'm',
            2 => 't',
            3 => 'w',
            4 => 'th',
            5 => 'f',
            6 => 's',
            7 => 'sun',
        ];
        
        
        if (is_numeric($src)) {

        }
    }
}

if (! function_exists('str_ordinal')) {
    /**
     * Append an ordinal indicator to a numeric value.
     *
     * @param  string|int  $value
     * @param  bool  $superscript
     * @return string
     */
    function str_ordinal($value, $superscript = false)
    {
        $number = abs($value);
 
        $indicators = ['th','st','nd','rd','th','th','th','th','th','th'];
 
        $suffix = $superscript ? '<sup>' . $indicators[$number % 10] . '</sup>' : $indicators[$number % 10];
        if ($number % 100 >= 11 && $number % 100 <= 13) {
            $suffix = $superscript ? '<sup>th</sup>' : 'th';
        }
 
        return number_format($number) . $suffix;
    }
}

if (! function_exists('convertMinutesToDays')) {
    function convertMinutesToDays($minutes)
    {
        // 60minutes * 24 hours
        return (float) number_format($minutes / (60*24), 2);
    } 
}

if (! function_exists('convertMinutesToHours')) {
    function convertMinutesToHours($minutes)
    {
        // 60minutes
        return (float) number_format($minutes / 60, 2);
    }
}

if (!function_exists('convertToHoursMinutes')) {
    function convertToHoursMinutes($time) {
        return [
            'hours' => floor($time / 60),
            'minutes' => ($time % 60),
        ];
    }
    
}