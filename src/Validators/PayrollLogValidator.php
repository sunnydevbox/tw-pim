<?php

namespace Sunnydevbox\TWPim\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

class PayrollLogValidator extends LaravelValidator {

    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'month'                 => 'numeric|min:1|max:12',
            'year'                  => 'numeric',
            'payroll_template_id'   => 'required',
            'custom'                => 'boolean',
        ],
        ValidatorInterface::RULE_UPDATE => [
            'month'                 => 'numeric|min:1|max:12',
            'year'                  => 'numeric',
            'payroll_template_id'   => 'required',
        ],
        'payroll_generate' => [
            'payroll_log_id' => 'required',
        ],

        'custom_create' => [
            'start'                 => 'required',
            'end'                   => 'required',
            'payroll_template_id'   => 'required',
            'pay_date'              => 'required',
            'pay_coverage_start'    => 'required',
            'pay_coverage_end'      => 'required',
        ]
   ];

}