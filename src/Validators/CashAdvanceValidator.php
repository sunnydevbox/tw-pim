<?php
namespace Sunnydevbox\TWPim\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

class CashAdvanceValidator extends LaravelValidator
{

    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'employee_id'           => 'required',
            'total_amount'          => 'required|numeric|min:1|gte:payable_per_period',
            'payable_per_period'    => 'required|numeric|min:1',
            'duration_start'        => 'required|date',
            'notes'                 => 'required',
        ],
        ValidatorInterface::RULE_UPDATE => [
            // 'employee_id'         => 'required',
        ],

        'SET_STATUS' => [
            'id'        => 'required',
            'status'    => 'required|string',
            
            // THIS notes field is intended for cash_advance_logs
            // NOT for cash_advances
            'notes'     => 'required', 
        ],

        'MAKE_DEBIT'    => [
            'id'        => 'required',
            
            // THIS notes field is intended for cash_advance_logs
            // NOT for cash_advances
            // 'notes'     => 'required', 
        ],
   ];

}