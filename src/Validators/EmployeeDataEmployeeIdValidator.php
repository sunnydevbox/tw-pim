<?php

namespace Sunnydevbox\TWPim\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

class EmployeeDataEmployeeIdValidator extends LaravelValidator {

    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'employee_id_number'    => 'min::3',
        ],
        ValidatorInterface::RULE_UPDATE => [
            'employee_id_number'    => 'min::3',
        ]
   ];

}