<?php

namespace Sunnydevbox\TWPim\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

class TableTaxValidator extends LaravelValidator {

    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            // 'compensation_level_period',
            // 'salary_range_from',
            // 'salary_range_to',
            // 'withholding_tax',
            'withholding_percentage' => 'required|min:0|max:100',
        ],
        ValidatorInterface::RULE_UPDATE => [
           // 'name'         => 'required',
        ]
   ];

}