<?php

namespace Sunnydevbox\TWPim\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

class JobPositionValidator extends LaravelValidator {

    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'name'         => 'required',
        ],
        ValidatorInterface::RULE_UPDATE => [
            'name'         => 'required',
        ]
   ];

}