<?php

namespace Sunnydevbox\TWPim\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

class SystemVariableValidator extends LaravelValidator {

    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            // 'value'         => 'required',
        ],
        ValidatorInterface::RULE_UPDATE => [
            // 'key'         => 'required',
        ]
   ];

}