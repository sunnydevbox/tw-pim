<?php
namespace Sunnydevbox\TWPim\Validators\CustomRules;

class PayrollTemplateMonthDays extends \Illuminate\Validation\Validator {

    public function validateMonthDaysFormat($attribute, $value, $parameters)
    {
        // dd($attribute, $value, $parameters);
        $days = explode(',', $value);
        $keywords = [
            'last_day',
            'day_before_last_day',
        ];

        // foreach ($days as $day) {
        //     $v = trim($day);
        //     if (!strlen($v)) {
                
        //         return false;
        //     }
        // }
            
        $days = collect(array_map(function($item) use ($keywords) {
            $v = trim($item);

            if (!strlen($v)) {
                
                return false  ;
            }
            
            // VALIDATE PATTERN
            // TO DO: 
            //  - ADD THE keywords 'last_day', etc

            // var_dump(preg_match('/[\w]+\-[\w]+/', $v, $matches));

            if (preg_match('/[\w]+\-[\w]+/', $v)) {
                $d = explode('-', $v);
                
                foreach($d as $day) {
                    if (!is_numeric($day) && array_search($day, $keywords) === false) {
                        return false;
                    }
                }

                return true;
            }

            return false;
            
        }, $days));
        
        return $days->contains(false) ? false : true;
    }

}