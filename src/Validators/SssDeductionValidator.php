<?php

namespace Sunnydevbox\TWPim\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

class SssDeductionValidator extends LaravelValidator {

    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'compensation_from'             => 'nullable|numeric|min:1',
            'compensation_to'               => 'nullable|numeric|min:1',
            'monthly_salary_credit'         => 'required|numeric|min:1',
            'social_security_er'            => 'required|numeric|min:1',
            'social_security_ee'            => 'required|numeric|min:1',
            'social_security_total'         => '',
            'ec_er'                         => 'required|numeric|min:1',
            'total_contribution_er'         => 'required|numeric|min:1',
            'total_contribution_ee'         => 'required|numeric|min:1',
            'total_contribution_er_ee'      => '',
            'total_contribution_se_vm_ofw'  => 'required|numeric|min:1',
        ],
        ValidatorInterface::RULE_UPDATE => [
            'compensation_from'             => 'nullable|numeric|min:1',
            'compensation_to'               => 'nullable|numeric|min:1',
            'monthly_salary_credit'         => 'required|numeric|min:1',
            'social_security_er'            => 'required|numeric|min:1',
            'social_security_ee'            => 'required|numeric|min:1',
            'social_security_total'         => '',
            'ec_er'                         => 'required|numeric|min:1',
            'total_contribution_er'         => 'required|numeric|min:1',
            'total_contribution_ee'         => 'required|numeric|min:1',
            'total_contribution_er_ee'      => '',
            'total_contribution_se_vm_ofw'  => 'required|numeric|min:1',
        ],
   ];

}