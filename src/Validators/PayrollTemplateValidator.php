<?php

namespace Sunnydevbox\TWPim\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

class PayrollTemplateValidator extends LaravelValidator
{
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            // 'name'          => 'required',
            // 'month_days'       => [
            //     'required'  => new \Sunnydevbox\TWPim\Validators\CustomRules\PayrollTemplateMonthDays
            // ],
            'month_days'    => 'required|month_days_format',
        ],
        ValidatorInterface::RULE_UPDATE => [
            // 'name'         => 'required',
            // 'country'       => 'required',
            'month_days'    => 'required|month_days_format',
        ],

        'template_selection' => [
            'payrollTemplateId' => 'required',
            'month'             => 'required|numeric',
            'year'              => 'required|numeric',

        ],
   ];

}