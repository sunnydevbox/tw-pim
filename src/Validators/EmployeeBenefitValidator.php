<?php
namespace Sunnydevbox\TWPim\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

class EmployeeBenefitValidator extends LaravelValidator
{

    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'employee_id' => 'required',
            'benefit_id'  => 'required',
        ],
        ValidatorInterface::RULE_UPDATE => [
            'employee_id' => 'required',
            'benefit_id'  => 'required',
        ]
   ];

}