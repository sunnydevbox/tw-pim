<?php

namespace Sunnydevbox\TWPim\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

class PayrollItemValidator extends LaravelValidator {

    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            // 'name'          => 'required',
            // 'country'       => 'required',
        ],
        ValidatorInterface::RULE_UPDATE => [
            // 'name'         => 'required',
            // 'country'       => 'required',
        ],

        'CUSTOM_ENTRY' => [
            'type'          => 'required',
            'payroll_id'    => 'required',
            'name'          => 'required',
            'value'         => 'required|numeric|min:1',
            'multiplier'    => 'required',
        ]
   ];

}