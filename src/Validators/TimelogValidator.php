<?php

namespace Sunnydevbox\TWPim\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

class TimelogValidator extends LaravelValidator {

    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            // 'employee_id'       => 'required',
            // 'time_in'           => 'required',
            // 'time_out'          => 'required',
        ],
        ValidatorInterface::RULE_UPDATE => [
            //'employee_id'       => 'required',
            'time_in'           => 'required',
            'time_out'          => 'required',
        ]
   ];

}