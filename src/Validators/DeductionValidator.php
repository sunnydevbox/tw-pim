<?php

namespace Sunnydevbox\TWPim\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

class DeductionValidator extends LaravelValidator {

    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'name'          => 'required',
            'country'       => 'required',
        ],
        ValidatorInterface::RULE_UPDATE => [
            'name'         => 'required',
            'country'       => 'required',
        ]
   ];

}