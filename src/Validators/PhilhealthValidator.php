<?php

namespace Sunnydevbox\TWPim\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

class PhilhealthValidator extends LaravelValidator {

    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'salary_bracket'    => 'required|numeric|min:1',
            'salary_range_from' => 'nullable|numeric|min:1',
            'salary_range_to'   => 'nullable|numeric|min:1',
            'salary_base'       => 'required|numeric|min:1',
            'monthly_premium'   => 'required|numeric|min:1',
            'employee_share'    => 'required|numeric|min:1',
            'employer_share'    => 'required|numeric|min:1',
        ],
        ValidatorInterface::RULE_UPDATE => [
            'salary_bracket'    => 'required|numeric|min:1',
            'salary_range_from' => 'nullable|numeric|min:1',
            'salary_range_to'   => 'nullable|numeric|min:1',
            'salary_base'       => 'required|numeric|min:1',
            'monthly_premium'   => 'required|numeric|min:1',
            'employee_share'    => 'required|numeric|min:1',
            'employer_share'    => 'required|numeric|min:1',
        ],
   ];

}