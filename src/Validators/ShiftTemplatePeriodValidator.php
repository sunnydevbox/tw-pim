<?php
namespace Sunnydevbox\TWPim\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

class ShiftTemplatePeriodValidator extends LaravelValidator
{
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
        	'shift_template_id' => 'required',
            'name' => 'required',
        ],
        ValidatorInterface::RULE_UPDATE => [
        	'shift_template_id' => 'required',
            'name' => 'required',
        ]
   ];
}