<?php

namespace Sunnydevbox\TWPim\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

class EmployeeLeaveCreditValidator extends LaravelValidator {

    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'employee_id'       => 'required',
            'leave_type_id'     => 'required',
            'amount'            => 'numeric',
        ],
        ValidatorInterface::RULE_UPDATE => [
            'employee_id'       => 'required',
            'leave_type_id'     => 'required',
            'amount'            => 'numeric',
        ]
   ];

}