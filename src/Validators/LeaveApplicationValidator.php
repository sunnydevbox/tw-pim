<?php

namespace Sunnydevbox\TWPim\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

class LeaveApplicationValidator extends LaravelValidator {

    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'leave_type_id' => 'required',
            'employee_id'   => 'required',
            'purpose'       => 'required',
            'start'         => 'required',
            'end'           => 'required',
        ],
        ValidatorInterface::RULE_UPDATE => [
            // 'employee_id' => 'required',
            // 'purpose'     => 'required',
        ]
   ];

}