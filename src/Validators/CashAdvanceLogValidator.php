<?php
namespace Sunnydevbox\TWPim\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

class CashAdvanceLogValidator extends LaravelValidator
{

    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'cash_advance_id'         => 'required',
        ],
        ValidatorInterface::RULE_UPDATE => [
            // 'employee_id'         => 'required',
        ]
   ];

}