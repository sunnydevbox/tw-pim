<?php

namespace Sunnydevbox\TWPim\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

class PayrollValidator extends LaravelValidator {

    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            // 'name'          => 'required',
            // 'country'       => 'required',
        ],
        ValidatorInterface::RULE_UPDATE => [
            // 'name'         => 'required',
            // 'country'       => 'required',
        ],

        'recalculate_payslip' => [
            'payroll_id' => 'required',
        ],
   ];

}