<?php

namespace Sunnydevbox\TWPim\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Sunnydevbox\TWPim\Models\Employee;

class EmployeeLogEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;
    
    public $employee;

    /**
     * ***************************************
     * EMPLOYEE RECORD
     * ***************************************
     * created 
     * deleted 
     * updated 
     * 
     * ***************************************
     * EMPLOYEE ID
     * ***************************************
     * id_created
     * id_deleted
     * id_updated
     * 
     * 
     * ***************************************
     * EMPLOYEE PROFILE PICTURE
     * ***************************************
     * picture_uploaded
     * picture_deleted
     * 
     * 
     * 
     * ***************************************
     * EMPLOYEMENT DETAILS
     * ***************************************
     * employment_details_created
     * employment_details_updated
     * 
     * 
     *
     * 
     */
    public $action;

    public $description;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($action, Employee $employee, $description)
    {
        $this->action       = $action;
        $this->employee     = $employee;
        $this->description  = $description;
    }
}
