<?php
namespace Sunnydevbox\TWPim\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class TimelogImportEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;
    
    public $timelogImport;
    public $action;
    public $data;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(
        $action, 
        $data = [], 
        $timelogImport = null
    ) {
        $this->action           = $action;
        $this->timelogImport    = $timelogImport;
        $this->data             = $data;
    }
}
