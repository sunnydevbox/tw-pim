<?php

namespace Sunnydevbox\TWPim\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Sunnydevbox\TWPim\Models\Timelog;

class TimelogEntryEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;
    
    public $timelog;
    public $action;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(
        Timelog $timelog,
        $action = null
    ) {
        $this->timelog = $timelog;
        $this->action = $action;
    }
}
