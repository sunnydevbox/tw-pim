<?php

namespace Sunnydevbox\TWPim\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class CashAdvanceUpdatedEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;
    
    public $cashAdvance;

    public function __construct($cashAdvance)
    {
        $this->cashAdvance     = $cashAdvance;
    }
}
