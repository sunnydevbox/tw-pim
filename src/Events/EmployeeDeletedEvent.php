<?php

namespace Sunnydevbox\TWPim\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Sunnydevbox\TWPim\Models\Employee;

class EmployeeDeletedEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;
    
    public $employee;

    public function __construct(Employee $employee)
    {
        $this->employee     = $employee;
    }
}
