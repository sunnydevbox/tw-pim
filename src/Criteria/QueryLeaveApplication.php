<?php

namespace Sunnydevbox\TWPim\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;
use Sunnydevbox\TWPim\Providers\EmployeeAuthProvider;
use Carbon\Carbon;

/**
 * Class SearchRelationshipsCriteria.
 *
 * @package namespace App\Criteria;
 */
class QueryLeaveApplication implements CriteriaInterface
{
    /**
     * Apply criteria in query repository
     *
     * @param string              $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        // query=column|value|operator;

        $query = request()->get('query');
        
        if ($query) {
            $model = $model->select('leave_applications.*');
            $queries = explode(';', $query);

            foreach($queries as $q) {
                $q = explode('|', $q);
                $column = $q[0];
                $value = isset($q[1]) ? $q[1] : null;
                $operator = isset($q[2]) ? $q[2] : '=';

                if ($value) {
                    switch ($column) {
                        case 'name':
                            $model = $model
                                        ->join('employees', 'employees.id', '=', 'leave_applications.employee_id')
                                        
                                        ->where(function ($query) use ($value) {
                                            $query->where('employees.first_name', 'LIKE', "%{$value}%")
                                                ->orWhere('employees.last_name', 'LIKE', "%{$value}%")
                                                ->orWhere('employees.employee_id_number', 'LIKE', "{$value}%")
                                                ;
                                        });
                        break;
                        case 'status':
                            if ($value) {
                                $model = $model->where('leave_applications.status', $operator, $value);
                            }
                        break;
                    
                        case 'type':
                            if ($value) {
                                $model = $model->where('leave_applications.leave_type_id', $operator, $value);
                            }
                        break;

                        case 'start':
                            if ($value != 'null' && !is_null($value)) {
                                $model = $model->date(Carbon::parse($value));
                            }   
                        break;
                    }
                }
            }
            
             //dd($model->toSql(), $model->getBindings());
        }
        
        return $model;
    }

}
