<?php

namespace Sunnydevbox\TWPim\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;
use Sunnydevbox\TWPim\Providers\EmployeeAuthProvider;

use \Carbon\Carbon;

/**
 * Class SearchRelationshipsCriteria.
 *
 * @package namespace App\Criteria;
 */
class PayrollCriteria implements CriteriaInterface
{
    /**
     * Apply criteria in query repository
     *
     * @param string              $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        $dateRange = request()->get('dateRange');
    
        if ($dateRange) {
            $startDate = null;
            $endDate = null;
            
            $dateRanges = explode('|', $dateRange);

            $startDate = $dateRanges[0];
            if (isset($dateRanges[1])) {
                $endDate = $dateRanges[1];
            }


            if ($startDate) {
                $startDate = Carbon::parse($startDate);
            }
            if ($endDate) {
                $endDate = Carbon::parse($endDate);
            }

            // $model = $model->where('start', '>=', $startDate)
            //             ->where('end', '<=', $endDate)
            //             ;
        }


        return $model;
    }

}
