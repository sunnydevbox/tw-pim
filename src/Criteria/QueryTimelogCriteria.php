<?php

namespace Sunnydevbox\TWPim\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;
use Sunnydevbox\TWPim\Providers\EmployeeAuthProvider;
use Carbon\Carbon;

/**
 * Class SearchRelationshipsCriteria.
 *
 * @package namespace App\Criteria;
 */
class QueryTimelogCriteria implements CriteriaInterface
{
    /**
     * Apply criteria in query repository
     *
     * @param string              $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        // query=column|value|operator;

        $query = request()->get('query');
        
        if ($query) {
            $model = $model->select('timelogs.*');

            $queries = explode(';', $query);

            foreach($queries as $q) {
                $q = explode('|', $q);
                $column = $q[0];
                $value = isset($q[1]) ? $q[1] : null;
                $operator = isset($q[2]) ? $q[2] : '=';

                if (strtolower($column) == 'name' && $value) {
                    $model = $model
                                ->join('employees', 'employees.id', '=', 'timelogs.employee_id')
                                
                                ->where(function($query) use ($value) {
                                    $query->where('employees.first_name', 'LIKE', "%{$value}%")
                                        ->orWhere('employees.last_name', 'LIKE', "%{$value}%")
                                        ->orWhere('employees.employee_id_number', 'LIKE', "{$value}%")
                                        ;
                                })
                                
                                ;
                } else if (strtolower($column) == 'status' && $value) {
                    if ($value != 'all' && $value != 'approved') {
                        $model = $model->where('timelogs.status', $value);
                    } else {
                        if ($value == 'approved') {
                            $model = $model->approved();
                        }
                    }
                } else if (strtolower($column) == 'time_in') {
                    if ($value && $value != 'null') {
                        $date = Carbon::parse($value);
                        // $model = $model->whereBetween('timelogs.time_in', [$date->copy()->startOfDay(), $date->copy()->endOfDay()]);
                        $model = $model->whereDate('timelogs.time_in', '>=', $date->copy()->startOfDay());
                    }
                } else if (strtolower($column) == 'time_out') {
                    if ($value && $value != 'null') {
                        // _dd($value);
                        $date = Carbon::parse($value);
                        // $model = $model->whereBetween('timelogs.time_in', [$date->copy()->startOfDay(), $date->copy()->endOfDay()]);
                        $model = $model->whereDate('timelogs.time_out', '<=', $date->copy()->endOfDay());
                    }
                } else if (strtolower($column) == 'is_rest_day') {
                    $model = $model->isRestDay($value);
                } else if (strtolower($column) == 'is_auto_approve') {
                    $model = $model->isAutoApprove($value);
                }

            }
            
            // _dd($model->toSql(), $model->getBindings());
        }
        
        return $model;
    }

}
