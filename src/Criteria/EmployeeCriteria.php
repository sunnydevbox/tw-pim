<?php

namespace Sunnydevbox\TWPim\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;
use Sunnydevbox\TWPim\Providers\EmployeeAuthProvider;
/**
 * Class SearchRelationshipsCriteria.
 *
 * @package namespace App\Criteria;
 */
class EmployeeCriteria implements CriteriaInterface
{
    /**
     * Apply criteria in query repository
     *
     * @param string              $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        $payload = \JWTAuth::getPayload(\JWTAuth::getToken()->get());
        
        // OTHER Models have NULL Auth::check
        // Find a way for this
        if (is_null(\Auth::check())) {

            $employee = \Sunnydevbox\TWPim\Models\Employee::find($payload['sub']); 
            // dd(config('tw-pim.models.employee'));
            $employee = \Sunnydevbox\TWPim\Models\Employee::find($payload['sub']);
            if ($employee->hasRole(config('tw-pim.role'))) {
            
                if (in_array(get_class($model), ['Sunnydevbox\TWPim\Models\EmployeeLog'])) {
                    $model = $model->where('employee_id', $payload['sub']);
                }
            }
        }
        
        // var_dump($model->getBindings());
        // dd($model->toSql());
        return $model;
    }

}
