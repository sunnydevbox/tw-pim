<?php

namespace Sunnydevbox\TWPim\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;
use Sunnydevbox\TWPim\Providers\EmployeeAuthProvider;
/**
 * Class SearchRelationshipsCriteria.
 *
 * @package namespace App\Criteria;
 */
class ScopeCriteria implements CriteriaInterface
{
    /**
     * Apply criteria in query repository
     *
     * @param string              $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        //scope=scopname1:arg1|arg2|arg3;scopname2:arg1|arg2|arg3
        $scope = request()->get('scope');


        if ($scope) {
            $scopes = explode(';', $scope);

            foreach($scopes as $scope) {
                $segments = explode(':', $scope);
                $scopeName = $segments[0];

                if (isset($segments[1])) {
                    $args = explode('|', $segments[1]);   
                }
               
                //dd(get_class($model) . '::' . $scopeName);
                //call_user_func_array(get_class($model) . '::scopeEmployee', $args);
                // $model->employee(1)->get();
                
            }
        }
        
       
        
        // var_dump($model->getBindings());
        // //dd($repository->model());
        // dd(get_class($model));
        // dd($model->toSql());
        // exit;
        return $model;
    }

}
