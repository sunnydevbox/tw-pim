<?php

namespace Sunnydevbox\TWPim\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;
use Sunnydevbox\TWPim\Providers\EmployeeAuthProvider;
use Carbon\Carbon;

/**
 * Class SearchRelationshipsCriteria.
 *
 * @package namespace App\Criteria;
 */
class TaxTableCriteria implements CriteriaInterface
{
    /**
     * Apply criteria in query repository
     *
     * @param string              $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        // query=column|value|operator;

        $model = $model->orderByRaw("FIELD(compensation_level_period , 'DAILY', 'WEEKLY', 'SEMI_MONTHLY', 'MONTHLY') ASC")
                        ->orderBy('salary_range_from', 'asc')
                        ;

        return $model;
    }

}
