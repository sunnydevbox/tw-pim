<?php

namespace Sunnydevbox\TWPim\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;
use Sunnydevbox\TWPim\Providers\EmployeeAuthProvider;
use Carbon\Carbon;

/**
 * Class SearchRelationshipsCriteria.
 *
 * @package namespace App\Criteria;
 */
class QueryEmployeeCriteria implements CriteriaInterface
{
    /**
     * Apply criteria in query repository
     *
     * @param string              $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        // query=column|value|operator;

        $query = request()->get('query');
        
        if ($query) {
            $model = $model->select('employees.*');

            $queries = explode(';', $query);

            foreach($queries as $q) {
                $q = explode('|', $q);
                $column = $q[0];
                $value = isset($q[1]) ? $q[1] : null;
                $operator = isset($q[2]) ? $q[2] : '=';
                
                if (!$value) {
                    continue;
                }

                $columnToCheck = strtolower($column);

                if ($columnToCheck == 'name') {
                    $model = $model
                                ->where(function($query) use ($value) {
                                    $query
                                        ->where('employees.first_name', '=', $value)
                                        ->orWhere('employees.last_name', '=', $value)
                                        ->orWhere('employees.first_name', 'LIKE', "%{$value}%")
                                        ->orWhere('employees.last_name', 'LIKE', "%{$value}%")
                                        ->orWhere('employees.employee_id_number', 'LIKE', "%{$value}%")
                                        ;
                                });
                } else if ($columnToCheck == 'status') {
                    if ($value != 'all' && $value != 'approved') {
                        $model = $model->where('employees.status', $value);
                    } else {
                        if ($value == 'active') {
                            $model = $model->approved();
                        }
                    }
                } else if ($columnToCheck == 'company_department_id') {

                    $model = $model->department($value);

                } else if ($columnToCheck == 'job_position_id') {

                    $model = $model->jobPosition($value);

                } else if ($columnToCheck == 'employment_status_id') {

                    $model = $model->employmentStatus($value);

                } else if ($columnToCheck == 'employment_type_id') {

                    $model = $model->employmentType($value);

                } else if ($columnToCheck == 'with') {

                } else if ($columnToCheck == 'withtimeloginvalid') {
                    // $model = $model->withCount(['timelogs' => function($query) {
                    //     $query->select('timelogs.status')
                    //         ->where(function($query) {
                    //         $query->invalid();
                    //     })
                    //     ->orWhere(function($query) {
                    //         $query->pending();
                    //     });
                        
                    // }]);
                }

            }
            
            // dd($model->toSql(), $model->getBindings());
        }
        
        return $model;
    }

}
