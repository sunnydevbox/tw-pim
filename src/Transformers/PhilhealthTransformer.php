<?php
namespace Sunnydevbox\TWPim\Transformers;

use League\Fractal\TransformerAbstract;

class PhilhealthTransformer extends TransformerAbstract
{

    public function transform($obj)
    {
        return $obj->toArray();
    }

}
