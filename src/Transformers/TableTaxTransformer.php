<?php

namespace Sunnydevbox\TWPim\Transformers;

use Dingo\Api\Http\Request;
use Dingo\Api\Transformer\Binding;
use Dingo\Api\Contract\Transformer\Adapter;
use League\Fractal\TransformerAbstract;

class TableTaxTransformer extends TransformerAbstract
{
     /**
     * Include user profile data by default
     */
    public function transform($obj)
    {
        return $obj->toArray();
    }
}