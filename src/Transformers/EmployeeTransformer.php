<?php

namespace Sunnydevbox\TWPim\Transformers;

use Dingo\Api\Http\Request;
use Dingo\Api\Transformer\Binding;
use Dingo\Api\Contract\Transformer\Adapter;
use League\Fractal\TransformerAbstract;

class EmployeeTransformer extends TransformerAbstract
{
     /**
     * Include user profile data by default
     */
    public function transform($obj)
    {
        $data = $obj->toArray();
        // if (request()->get('query') && request()->get('query') == 'withTimelogCount') {
            $data['timelog_pending'] = $obj->total_pending;
            $data['timelog_invalid'] = $obj->total_invalid;
            $data['timelog_approved'] = $obj->total_approved;
            // dd($obj);   
        // }

        $data['roles'] = [];
        $data['permissions'] = [];
        if ($obj->roles) {
            foreach ($obj->roles as $role) {
                $data['roles'][] = [
                    'name' => $role['name'],
                ];
            }
        }

        if (method_exists('getAllPermissions', $obj)) {
            foreach ($obj->getAllPermissions() as $permission) {
                $data['permissions'][] = [
                    'name' => $permission['name'],
                ];
            }
        }

        return $data;
    }
}