<?php

namespace Sunnydevbox\TWPim\Transformers;

use Dingo\Api\Http\Request;
use Dingo\Api\Transformer\Binding;
use Dingo\Api\Contract\Transformer\Adapter;
use League\Fractal\TransformerAbstract;

class TimelogTransformer extends TransformerAbstract
{
     /**
     * Include user profile data by default
     */
    public function transform($obj)
    {
        // dd($obj->employee->timelogs()->valid()->get());

        return $obj->toArray();
    }
}