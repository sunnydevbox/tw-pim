<?php

namespace Sunnydevbox\TWPim\Transformers;

use Dingo\Api\Http\Request;
use Dingo\Api\Transformer\Binding;
use Dingo\Api\Contract\Transformer\Adapter;
use League\Fractal\TransformerAbstract;

class PayrollTransformer extends TransformerAbstract
{
     /**
     * Include user profile data by default
     */
    public function transform($obj)
    {
        $payrollItem = app(config('tw-pim.model_payroll_item'));

        if (request()->has('groupitems') && $obj->relationLoaded('items')) {
            $d = [];

            foreach($obj->items as $item) {                
                $d[$item->type][] = $item;
            }

            $obj->i = $d;
            // unset($obj->items);
        }

        return $obj->toArray();
    }
}