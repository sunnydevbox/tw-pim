<?php
namespace Sunnydevbox\TWPim\Services;

use Sunnydevbox\TWPim\Repositories\Payroll\PayrollRepository;

class ReportService
{
    public function getPayrolls($data)
    {
        $limit = is_numeric($data['limit']) ? $data['limit'] : config('repository.pagination.limit', 15);

        $result = $this->payrollRepository->paginate($limit);
        // dd($result->items()[0]);
        //return view('twpim::reports.payrolls', ['payrolls' => $result->items()]);
        $pdf = \PDF::loadHTML(view('twpim::reports.payrolls', ['payrolls' => $result]))
        ->setPaper('a4', 'landscape')
        ->setWarnings(false);

        return $pdf->download('p.pdf');
        dd($result->items());
        
		
		if ($limit == 0) {
			$result = $this->repository->all();

			return $this->response()
					->collection($result, $this->transformer)
					->withHeader('Content-Range', $result->count());
		} else {
			

                return $this->response
                    ->paginator($result, $this->transformer)
                    ->withHeader('Content-Range', $result->total());
		}
    }

    public function __construct(
        PayrollRepository $payrollRepository
    ) {
        $this->payrollRepository = $payrollRepository;
    }

}
