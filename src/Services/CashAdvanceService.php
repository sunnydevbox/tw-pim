<?php
namespace Sunnydevbox\TWPim\Services;

// use \Prettus\Validator\Exceptions\ValidatorException;

class CashAdvanceService
{
    public function setStatus($data)
    {
        $this->cashAdvanceValidator->with($data)->passesOrFail('SET_STATUS');

        if ($this->cashAdvanceRepository->isValidStatus($data['status'])) {
            return \DB::transaction(function () use ($data) {
            
                $cashAdvance = $this->cashAdvanceRepository->find($data['id']);

                switch($this->cashAdvanceRepository->getStatusConstValue($data['status'])) {
                    case $this->cashAdvanceRepository->makeModel()::STATUS_APPROVED:
                        $this->approve($cashAdvance, $data, true);
                    break;

                    case $this->cashAdvanceRepository->makeModel()::STATUS_DISAPPROVED:
                        $this->disapprove($cashAdvance, $data, true);
                    break;

                    case $this->cashAdvanceRepository->makeModel()::STATUS_CLOSED:
                        $this->close($cashAdvance, $data, true);
                    break;

                    case $this->cashAdvanceRepository->makeModel()::STATUS_ACTIVE:
                        $this->activate($cashAdvance, true);
                    break;

                    default:
                        $cashAdvance = $this->cashAdvanceRepository->setStatus($data['id'], $data['status']);
                        $cashAdvanceLog = $this->cashAdvanceLogRepository->addByStatus($data['id'], $data['notes']);
                        $cashAdvance->touch();
                    break;
                }


                
                
                return $cashAdvance;
            });
        } else {
            abort(400, 'Invalid Status: '.$data['status']);
        }
    }

    public function makeDebit($data)
    {
        $this->cashAdvanceValidator->with($data)->passesOrFail('MAKE_DEBIT');

        return \DB::transaction(function () use ($data) {
            
            $cashAdvance = $this->cashAdvanceRepository->find($data['id']);
            
            $cashAdvanceLog = $this->debit($cashAdvance);
    
            $cashAdvance->touch();
    
            event(new \Sunnydevbox\TWPim\Events\CashAdvanceUpdatedEvent($cashAdvance));
            
            return $cashAdvanceLog;
        });
    }

    public function activate($cashAdvance, $strict = false)
    {
        if ($this->cashAdvanceRepository->isActivatable($cashAdvance, $strict)) {
            
            $this->cashAdvanceRepository->activate($cashAdvance);
            $cashAdvanceLog = $this->cashAdvanceLogRepository->addByStatus($cashAdvance->id, 'CA application ACTIVATED');
            return $cashAdvanceLog;
        } else {
            throw new \Exception('Cash Advance application not activatable. Current Status: ' . $cashAdvance->status, 400);
        }
    }

    public function approve($cashAdvance, $data = [], $strict = false)
    {
        if ($this->cashAdvanceRepository->isApprovable($cashAdvance, $strict)) {
            
            $this->cashAdvanceRepository->approve($cashAdvance);

            $notes = 'CA application APPROVED';

            if (isset($data['notes'])) {
                $notes .= "\n\n Notes: \n {$data['notes']}";
            }

            $cashAdvanceLog = $this->cashAdvanceLogRepository->addByStatus($cashAdvance->id, $notes);
            return $cashAdvanceLog;
        } else {
            throw new \Exception('Cash Advance application not activatable. Current Status: ' . $cashAdvance->status, 400);
        }
    }

    public function disapprove($cashAdvance, $data = [], $strict = false)
    {       
        $this->cashAdvanceRepository->disapprove($cashAdvance);

        $notes = 'CA application DISAPPROVED';

        if (isset($data['notes'])) {
            $notes .= "\n\n Notes: \n {$data['notes']}";
        }

        $cashAdvanceLog = $this->cashAdvanceLogRepository->addByStatus($cashAdvance->id, $notes);
        return $cashAdvanceLog;
    }

    public function close($cashAdvance, $data = [], $strict = false)
    {       
        $this->cashAdvanceRepository->close($cashAdvance);

        $notes = 'CA application CLOSED';

        if (isset($data['notes'])) {
            $notes .= "\n\n Notes: \n {$data['notes']}";
        }

        $cashAdvanceLog = $this->cashAdvanceLogRepository->addByStatus($cashAdvance->id, $notes);
        return $cashAdvanceLog;
    }

    public function debit($cashAdvance, $strict = false)
    {
        $debitable = $this->cashAdvanceRepository->isDebitable($cashAdvance, $strict);
        
        if ($debitable) {
            $this->cashAdvanceRepository->debit($cashAdvance);

            $cashAdvanceLog = $this->cashAdvanceLogRepository->addByDebit(
                $cashAdvance->id, 
                $cashAdvance->payable_per_period,
                'CA Debited: ' . $cashAdvance->payable_per_period
            );
            return $cashAdvanceLog;
        } else {
            throw new \Exception('Cash Advance application not debitable.', 400);
        }
    }
    

    public function __construct(
        \Sunnydevbox\TWPim\Repositories\Deduction\CashAdvanceRepository $cashAdvanceRepository,
        \Sunnydevbox\TWPim\Repositories\Deduction\CashAdvanceLogRepository $cashAdvanceLogRepository,
        \Sunnydevbox\TWPim\Validators\CashAdvanceValidator $cashAdvanceValidator
    ) {
        $this->cashAdvanceRepository = $cashAdvanceRepository;
        $this->cashAdvanceValidator = $cashAdvanceValidator;
        $this->cashAdvanceLogRepository = $cashAdvanceLogRepository;
    }
}
