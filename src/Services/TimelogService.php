<?php
namespace Sunnydevbox\TWPim\Services;

use Sunnydevbox\TWPim\Services\EmployeeIDAbstract;
use PDF;
use ReflectionObject;
use \Prettus\Validator\Exceptions\ValidatorException;
use \Prettus\Validator\Contracts\ValidatorInterface;

class TimelogService
{
    public function setStatus($timelogId, $action)
    {
        $timelog = $this->timelogRepository->find($timelogId);

        if ($timelog) {

            switch(strtoupper(trim($action))) {
                case $timelog::STATUS_APPROVED: 
                    if (!$timelog->hasShiftTemplate()) {
                        throw new \Exception('missing_shift_template_period');
                    }
                break;
                
                default:
                    
                break;
            }

            $result = $this->timelogRepository->setStatus($timelogId, $action);

            return $result;
        }   
    }

    public function update($data, $id)
    {
        if (isset($this->timelogValidator) && $this->timelogValidator) {
            $this->timelogValidator->with($data)->passesOrFail(ValidatorInterface::RULE_UPDATE);
        }
        
        $result = $this->timelogRepository->makeModel()
                        ->find($id)
                        ->update($data);

        $result = $this->timelogRepository->makeModel()
                        ->with('shift_template_period.shift_template')
                        ->find($id);

        return $result;
    }

    public function recalculate($data, $id)
    {
        $timelog = $this->timelogRepository->find($id);
        return $this->timelogRepository->calculate($timelog);
    }

    public function __construct(
        \Sunnydevbox\TWPim\Repositories\Timelog\TimelogRepository $timelogRepository,
        \Sunnydevbox\TWPim\Validators\TimelogValidator $timelogValidator
    ) {
        $this->timelogRepository = $timelogRepository;
        $this->timelogValidator = $timelogValidator;
    }
}
