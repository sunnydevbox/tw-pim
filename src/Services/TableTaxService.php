<?php
namespace Sunnydevbox\TWPim\Services;

use Sunnydevbox\TWPim\Services\EmployeeIDAbstract;
use PDF;
use ReflectionObject;
use \Prettus\Validator\Exceptions\ValidatorException;
use \Prettus\Validator\Contracts\ValidatorInterface;

class TableTaxService
{

    public function getRange($period, $salary)
    {
        $result = $this->taxRepository->getRange($period, $salary);

        return $result;
    }

    public function __construct(
        \Sunnydevbox\TWPim\Repositories\Table\TaxRepository $taxRepository
    ) {
        $this->taxRepository = $taxRepository;
    }
}
