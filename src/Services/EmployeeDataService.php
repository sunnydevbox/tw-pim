<?php
namespace Sunnydevbox\TWPim\Services;

use \Carbon\Carbon;

class EmployeeDataService
{
    public function process($attributes, $id)
    {
        $Employee = $this->rpoEmployee->find($id); 

        $type = $attributes['type'];
        $attributes['employee_id'] = $id;

        $EmployeeDataFactory = $this->factory->store($type, $attributes);
        if ($type == 'profile-picture') {
            return $EmployeeDataFactory;
        } else {
            return $EmployeeDataFactory->saveData($Employee, $attributes);
        }
    }

    public function attachShiftTemplate($id, $shiftTemplateId)
    {
        if ($employee = $this->rpoEmployee->find($id)) {
            if ($this->rpoShiftTemplate->findByField('id', $shiftTemplateId)->count()) {
                $result = $employee->shift_templates()->syncWithoutDetaching($shiftTemplateId);
                return $result;
            }

            throw new \Exception('shift_template_not_found', 404);
        }
    }

    public function detachShiftTemplate($id, $shiftTemplateId)
    {
        if ($employee = $this->rpoEmployee->find($id)) {
            
            if ($this->rpoShiftTemplate->findByField('id', $shiftTemplateId)->count()) {
                $result = $employee->shiftTemplates()->detach($shiftTemplateId);
                return $result;
            }

            throw new \Exception('shift_template_not_found', 404);
        }
    }

    public function attachPayrollTemplate($id, $payrollTemplateId)
    {
        if ($employee = $this->rpoEmployee->find($id)) {
            if ($this->rpoPayrollTemplate->findByField('id', $payrollTemplateId)->count()) {
                $result = $employee->payroll_templates()->syncWithoutDetaching($payrollTemplateId);
                return $result;
            }

            throw new \Exception('payroll_template_not_found', 404);
        }
    }

    public function detachPayrollTemplate($id, $payrollTemplateId)
    {
        if ($employee = $this->rpoEmployee->find($id)) {
            
            if ($this->rpoPayrollTemplate->findByField('id', $payrollTemplateId)->count()) {
                $result = $employee->payroll_templates()->detach($payrollTemplateId);
                return $result;
            }

            throw new \Exception('payroll_template_not_found', 404);
        }
    }

    public function attachBenefit($data)
    {
        // ValIDATION HERE

        if ($employee = $this->rpoEmployee->find($data['employee_id'])) {
            if ($this->rpoBenefit->find($data['benefit_id'])) {
                $result = $employee->benefits()->syncWithoutDetaching($data['benefit_id']);
                return $result;
            }

            throw new \Exception('benefit_not_found', 404);
        }
    }

    public function detachBenefit($data)
    {
        // ValIDATION HERE

        if ($employee = $this->rpoEmployee->find($data['employee_id'])) {
            if ($this->rpoBenefit->find($data['benefit_id'])) {
                $result = $employee->benefits()->detach($data['benefit_id']);
                return $result;
            }

            throw new \Exception('benefit_not_found', 404);
        }
    }

    public function attachDeduction($data)
    {
        // ValIDATION HERE

        if ($employee = $this->rpoEmployee->find($data['employee_id'])) {
            if ($this->rpoDeduction->find($data['deduction_id'])) {
                $result = $employee->deductions()->syncWithoutDetaching($data['deduction_id']);
                return $result;
            }

            throw new \Exception('deduction_not_found', 404);
        }
    }

    public function detachDeduction($data)
    {
        // ValIDATION HERE

        if ($employee = $this->rpoEmployee->find($data['employee_id'])) {
            if ($this->rpoDeduction->find($data['deduction_id'])) {
                $result = $employee->deductions()->detach($data['deduction_id']);
                return $result;
            }

            throw new \Exception('deduction_not_found', 404);
        }
    }

    public function __construct(
        \Sunnydevbox\TWPim\Repositories\Employee\EmployeeRepository $rpoEmployee,
        \Sunnydevbox\TWPim\Repositories\ShiftTemplate\ShiftTemplateRepository $rpoShiftTemplate,
        \Sunnydevbox\TWPim\Repositories\Payroll\PayrollTemplateRepository $rpoPayrollTemplate,
        \Sunnydevbox\TWPim\Repositories\Benefit\BenefitRepository $rpoBenefit,
        \Sunnydevbox\TWPim\Repositories\Deduction\DeductionRepository $rpoDeduction,
        \Sunnydevbox\TWPim\Factories\EmployeeDataFactory $factory
    ) {
        $this->rpoEmployee = $rpoEmployee;
        $this->factory = $factory;
        $this->rpoShiftTemplate = $rpoShiftTemplate;
        $this->rpoPayrollTemplate = $rpoPayrollTemplate;
        $this->rpoBenefit = $rpoBenefit;
        $this->rpoDeduction = $rpoDeduction;
    }
}
