<?php
namespace Sunnydevbox\TWPim\Services;

use Sunnydevbox\TWPim\Services\EmployeeIDAbstract;
use PDF;
use ReflectionObject;
use \Prettus\Validator\Exceptions\ValidatorException;
use \Prettus\Validator\Contracts\ValidatorInterface;

class SystemVariableService
{
    public function __construct(
        \Sunnydevbox\TWPim\Repositories\SystemVariable\SystemVariableRepository $systemVariableRepository,
        \Sunnydevbox\TWPim\Validators\SystemVariableValidator $systemVariableValidator
    ) {
        $this->systemVariableRepository = $systemVariableRepository;
        $this->systemVariableValidator = $systemVariableValidator;
    }
}
