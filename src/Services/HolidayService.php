<?php
namespace Sunnydevbox\TWPim\Services;

use \Prettus\Validator\Exceptions\ValidatorException;

class HolidayService
{
    public function monthList($data)
    {
        try {
            $this->holidayValidator->with($data)->passesOrFail('GET_MONTH_LIST');
            return $this->holidayRepository->getMonthList($data['start'], $data['end']);
        } catch(ValidatorException $e) {
            // throw new ValidatorException($e);
            // return response()->json([
            //     'status_code'   => 400,
            //     'message' => $e->getMessageBag()
            // ], 400);
        }
        
    }

    public function getHolidayByDate($timeIn)
    {
        if (is_object($timeIn) && $timeIn instanceof Carbon) {
            $timeIn = $timeIn->toDateString();
        }

        return $this->holidayRepository->with('type')
                                ->findWhere(['start_date' => $timeIn]);
    }

    public function __construct(
        \Sunnydevbox\TWPim\Repositories\Holiday\HolidayRepository $holidayRepository,
        \Sunnydevbox\TWPim\Validators\HolidayValidator $holidayValidator
    ) {
        $this->holidayRepository = $holidayRepository;
        $this->holidayValidator = $holidayValidator;
    }
}
