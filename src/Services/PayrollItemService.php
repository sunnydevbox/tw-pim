<?php
namespace Sunnydevbox\TWPim\Services;

class PayrollItemService
{
    public function customEntry($data)
    {
        $data['is_custom'] = 1;

        // $this->mockCalculation($data);
        $result = $this->payrollItemRepository->create($data);
        $payrollItem = $this->payrollItemRepository->find($result->id);

        return $payrollItem;
    }
    

    public function adjustment($payrollItem)
    {
        $value = null;

        if ($payrollItem->is_custom) {
            
            $value = 0;
            $reference = $payrollItem->data['reference'];
        
            if ($reference) {

                if (is_object($reference)) {

                    $reference = $reference->toArray();
                    $reference = $reference['id'];

                }
            }

            if (is_numeric($reference)) {
                if ($payrollItem->data['multiplier'] == 'percentage') {

                    $value = $payrollItem->data['value'] * $payrollItem->find($reference)->total_net;

                } else if ($payrollItem->data['multiplier'] == 'flat') {

                    $value = $this->payrollItemRepository->find($reference)->total_net - $payrollItem->data['value'];
                }
            } else {    
                $value = $payrollItem->data['value'];
            }
            
            if ($payrollItem->type == 'ADJ_DEDUCTION') {
                $value = 0 - abs($value);
            }
        } 
        else {
            $value = $payrollItem->total_net;
        }
        
        return $value;
    }

    public function mockCalculation($data)
    {
        $payroll = $this->payrollRepository->makeModel()
                    ->with([
                        // 'employee', 'employee.deductions', 'employee.benefits', 
                        'payroll_log',])
                    ->find($data['payroll_id']);

        if (!$payroll) {
            throw new \Exception('invalid_payroll_id', 404);
        }

        $payroll = $this->payrollRepository->makeModel()
                ->with([
                    'employee.deductions', 
                    'employee.benefits', 
                    'payroll_log',
                    'employee.timelogs' => function($query) use ($payroll) {
                        $query->dateRange($payroll->payroll_log->start, $payroll->payroll_log->end)
                                ->where('employee_id', $payroll->employee_id)
                                ->approved()
                                ->orderBy('time_in');
                    }
                ])
                ->find($data['payroll_id']);

        $test = $this->adjustment((object) $data);
        
        dd(
            $test, 
            $payroll->total_gross,
            $payroll->total_gross * 0.4,
            $payroll->total_gross - abs($test)
        );
    }

    public function __construct(
        \Sunnydevbox\TWPim\Repositories\Payroll\PayrollItemRepository $payrollItemRepository,
        \Sunnydevbox\TWPim\Repositories\Payroll\PayrollRepository $payrollRepository
    ) {
        $this->payrollItemRepository = $payrollItemRepository;
        $this->payrollRepository = $payrollRepository;
    }
}
