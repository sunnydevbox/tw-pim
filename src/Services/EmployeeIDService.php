<?php
namespace Sunnydevbox\TWPim\Services;

use Sunnydevbox\TWPim\Services\EmployeeIDAbstract;

class EmployeeIDService extends EmployeeIDAbstract
{
    protected function format()
    {
        return 'EM-' . $this->object->id;
    }

    protected function fallbackFormat()
    {
        if (request()->get('employee_id_number')) {
            return request()->get('employee_id_number');
        }
    }

    protected function conditionsToGenerate()
    {
        return empty(request()->get('employee_id_number'))? true : false;
    }
}
