<?php
namespace Sunnydevbox\TWPim\Services;

use Sunnydevbox\TWPim\Services\EmployeeIDAbstract;
use PDF;
use ReflectionObject;
use Carbon\Carbon;
use Sunnydevbox\TWPim\Models\Timelog;


class LeaveApplicationService
{
    public function setStatus($leaveApplicationId, $action)
    {
        $leaveApplication = $this->leaveApplicationRepository->find($leaveApplicationId);

        if ($leaveApplication) {
            try {
                $const = 'STATUS_' . strtoupper($action);
                $class = get_class($this->leaveApplicationRepository->makeModel());
                $statusAction = @constant("$class::$const");
               
                if (!$statusAction) {
                    throw new \Exception('invalid_status_constant');
                }

                $result = $this->leaveApplicationRepository->update([
                    'status' => $statusAction,
                ], $leaveApplicationId);

                return $result;
            } catch (Exception $e) {
                // _dd($e);
            }
        }
    }

    public function getApplicationsByTimelog(Timelog $timelog, $type = null)
    {
        $start = $timelog->time_in->toDateString();
    
        $employeeId = $timelog->employee->id;
        
        $query = $this->repository->makeModel()
            ->with('leave_type', 'holiday.holiday_type')
            ->setEmployee($employeeId)
            ->findMatch($start);
        
        if ($type) {
            $typeName = camel_case(strtolower($type));

            // dd($query->{$typeName}());
            $query = $query->{$typeName}();
        }
            
        $result = $query->get();
            
        return $result;
    }

    public function getApplicationsByUser(
        $employeeId, 
        Timelog $timelog = null,
        // $this->timelogObject->time_in->format('Y-m-d'), 
        $type = null
    ) {
        $start = $timelog->time_in->toDateString();
        
        $query = $this->repository->makeModel()
            ->with('leave_type', 'holiday.holiday_type')
            ->setEmployee($employeeId)
            ->findMatch($start);

        if ($type) {
            $typeName = camel_case(strtolower($type));
            $query = $query->{$typeName}();
        }

        $result = $query->get();
        
        return $result;
    }

    public function __construct(
        \Sunnydevbox\TWPim\Repositories\LeaveApplication\LeaveApplicationRepository $leaveApplicationRepository
    ) {
        $this->repository = $leaveApplicationRepository;
        $this->leaveApplicationRepository = $leaveApplicationRepository;
    }
}
