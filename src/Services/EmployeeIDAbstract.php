<?php
namespace Sunnydevbox\TWPim\Services;

use Sunnydevbox\TWPim\Services\EmployeeIDInterface;

abstract class EmployeeIDAbstract implements EmployeeIDInterface
{
    public $object;

    public function generate()
    {
        if ($this->conditionsToGenerate()) {
            return $this->format();
        }

        return $this->fallbackFormat();
    }

    protected function conditionsToGenerate()
    {
        return true;
    }

    abstract protected function format();
    abstract protected function fallbackFormat();
}
