<?php
namespace Sunnydevbox\TWPim\Services;

use Sunnydevbox\TWPim\Services\EmployeeIDAbstract;
use \Carbon\Carbon;
use Sunnydevbox\TWPim\Core\TimelogImporterAbstract;

/**
 * UNITEDREBUILDERS specific DTR parser
 * 
 * NOTE: TO DO: this should implement na interface
 */
class UnitedRebuildersImportService extends TimelogImporterAbstract
{
    private $bio_id = null;

    /**
     * OVERRIDE THIS
     *
     * @return void
     */
    public function parse()
    {
        $file = $this->timelogImport->attachments()->first(); 
    
        if ($file) {
            if ($file->disk == 'local') {
                $csvFile = storage_path('app/' . $file->filepath);
            }
        }
    
        $handle = fopen($csvFile, 'r');
        
        while ($csvLine = fgetcsv($handle, 1000, ",")) {
            if (isset($csvLine[8]) && $csvLine[8]  == 'In 1') {
                
                // PARSING THE employee's bio_id
                // based on the client's own csv format
                if (preg_match('/(\([\w]+\))/', $csvLine[5], $this->bio_id)) {
                    $this->bio_id = preg_replace('/(\(|\))/', '', $this->bio_id[0]);
                }
            } else {
                // 1) Validate EMPLOYEE ID (column 1)
                // 2) Check for duplicate entries
                $parsed = $this->parseRow($csvLine);
            }
        } 
    }

    /**
     * parseRow
     * 
     * United Rebuiilders has a speciific format of their 
     * DTR CSV. This is the parser for that
     */
    public function parseRow($entry)
    {
        $day = Carbon::parse($entry[5]);

        $ctr = 0;
        $pair = 'in';
        foreach(range(6,19) as $index) {
            ${$pair . $ctr} = $this->cleanTime($day, $entry[$index]);
            if ($index % 2) {
                $ctr++;
                $pair = 'in';
            } else {
                $pair = 'out';
            }
        }
        
        $data = [
            'employeeId'    => $this->bio_id, 
            'date'          => $day,
            'in0'           => $in0,
            'out0'          => $out0,
            'in1'           => $in1,
            'out1'          => $out1,
            'in2'           => $in2,
            'out2'          => $out2,
            'in3'           => $in3,
            'out3'          => $out3,
            'in4'           => $in4,
            'out4'          => $out4,
            'in5'           => $in5,
            'out5'          => $out5,
            'in6'           => $in6,
            'out6'          => $out6,
        ];

        // PERSIST TO DB
        if ($employee = $this->getEmployee($this->bio_id)) {
            \Log::info('Empl_found: ' .  $data['employeeId']);
            //\DB::transaction(function () use ($data, $employee) {

                // 2) Check for duplicate entries
                // if ($data['in0'] && $data['out0']) {
                $cntr=0;
                foreach (range(0, 6) as $index) {
                    if (isset($data['in' . $index]) && ($data['in' . $index] || $data['out' . $index])) {

                        // CALL THE recorder
                        $this->saveRecord($employee->id, $data['in' . $index], $data['out' . $index]);   
                    }
                }
            // }, 5);

        } else {
            // EMPLOYEE not found
            // LOG this error
            \Log::info('Empl_not_found: ' .  $data['employeeId']);
        }
    }

    public function getEmployee($employeeId)
    {
        $query = $this->employeeRepository->makeModel()
                        ->whereBioId($employeeId)
                        ->limit(1)
                    ;
        return $query->first();
    }

    private function cleanTime($day, $time)
    {  

        if (is_string($time) && strlen($time) && $time != 0) {
            $time = trim($time);
            // var_dump($this->bio_id,strlen($time), $time, $day->format('Y-m-d ') . ' ' . $time);
            return Carbon::parse($day->format('Y-m-d ') . ' ' . $time);// $time;
        }

        return null;
    }


    /**
     * verifier
     * 
     * This verifies if the CSV contains the correct columns needed by
     * United Rebuilder's parser
     * 
     */
    public function verifier()
    {
        return true;
        $file = $this->file();
        
        if ($file && $file->isValid()) {

            // In this EXACT order
            $fields = [
                'Employee_ID',
                'Date',
                'In_0',
                'Out_0',
                'In_1',
                'Out_1',
                'In_2',
                'Out_2',
                'In_3',
                'Out_3',
                'In_4',
                'Out_4',
                'In_5',
                'Out_5',
                'In_6',
                'Out_6'
            ];

            $handle = fopen($file, 'r');
            $ctr = 1;
            $header = true;

            while ($csvLine = fgetcsv($handle, 1000, ",")) {
                if ($header) {
                    $header = false;
                    foreach($csvLine as $k => $v) {
                        if ($fields[$k] != $csvLine[$k]) {
                            throw new \Exception('invalid_format_unmatched_columns');
                        }
                    }
                }
            }

            return true;
        } else {
            throw new \Exception('invalid_format');
        }

        return true;
    }

}
