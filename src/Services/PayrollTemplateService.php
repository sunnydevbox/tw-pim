<?php
namespace Sunnydevbox\TWPim\Services;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\Exceptions\ValidatorException;

class PayrollTemplateService
{
    public function periodSelections($data)
    {
        try {
            $this->payrollTemplateValidator->with($data)->passesOrFail('template_selection');

            $result = $this->payrollTemplateRepository->generateSelectablePeriods(
                $data['payrollTemplateId'], $data['month'], $data['year']
            );
            
            return $result;
        } catch (ValidatorException $e) {
            return response()->json([
                'status_code'   => 400,
                'message'       => $e->getMessageBag()
            ], 400);
        }
    }

    public function __construct(
        \Sunnydevbox\TWPim\Repositories\Payroll\PayrollTemplateRepository $payrollTemplateRepository,
        \Sunnydevbox\TWPim\Validators\PayrollTemplateValidator $payrollTemplateValidator
    ) {
        $this->payrollTemplateRepository = $payrollTemplateRepository;
        $this->payrollTemplateValidator = $payrollTemplateValidator;
    }
}