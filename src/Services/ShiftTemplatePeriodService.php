<?php
namespace Sunnydevbox\TWPim\Services;

class ShiftTemplatePeriodService
{


    public function perEmployee($data, $id)
    {
        $model = $this->shiftTemplatePeriodRepository->makeModel();
		$result = $model
			->select($model->getTable() . '.*')
			->employee($id)
            ->get();
            
        return $result;
    }

    public function __construct(
        \Sunnydevbox\TWPim\Repositories\ShiftTemplatePeriod\ShiftTemplatePeriodRepository $shiftTemplatePeriodRepository
    ) {
        $this->shiftTemplatePeriodRepository = $shiftTemplatePeriodRepository;
    }
}