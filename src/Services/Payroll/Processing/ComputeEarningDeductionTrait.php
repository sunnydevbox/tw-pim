<?php
namespace Sunnydevbox\TWPim\Services\Payroll\Processing;

trait ComputeEarningDeductionTrait
{
    private $trashedItems = [];
    public function processEarningDeduction($employee, $payroll)
    {
        if (!$employee) {
            $employee = $payroll->employee;
        }

        // Get the recently deleted items
        $this->trashedItems = $payroll->items()->withTrashed()->get();

        $employeeTemplate = $employee->employee_template;
        $employeeTemplateItems = $employeeTemplate->getItems();
        
        if (!$employeeTemplate) {
            throw new \Exception('employee_missing_employee_template' , 400);
        }
        
        $employeeTemplateRepository = app('\Sunnydevbox\TWPim\Repositories\Employee\EmployeeTemplateRepository');

        $benefitRepository = app('\Sunnydevbox\TWPim\Repositories\Benefit\BenefitRepository');
        $deductionRepository = app('\Sunnydevbox\TWPim\Repositories\Deduction\DeductionRepository');
        
        /*********************
         ********************* 
         ****** EARNINGS *****
         *********************
         * 
         *  These are governemnt mandated items 
         *  that the employee earns such as 
         *  OT, Night Diff, Basic Pay
         * 
         **/
        foreach($benefitRepository->all() as $benefit) {
            
            if ($employeeTemplateItems && !$benefit->machine_name) {
                continue;
            }

            $calc = null;
            $name = $benefit->name;
            $machineName = $benefit->machine_name;
            $totalNet = 0;
            $totalGross = 0;

            // CHECK IF CALCULATOR for this benefit exists
            $this->computeItem(
                $employee,
                $employeeTemplate,
                $payroll,
                $this->payrollItemRepository->makeModel()::TYPE_EARNING,
                $benefit,
                $name,
                $machineName
            );
        }

        /*********************
         ********************* 
         ***** DEDUCTIONS ****
         *********************
         * 
         *  These are governemnt mandated items 
         *  that the employee owes to the govt 
         *  such as Philhealth, SSS, Pag-ibig, 
         *  tardiness. Cash Advance is not included 
         * 
         **/
        $hasTax = false;
        foreach($deductionRepository->all() as $deduction) {
            $name = $deduction->name;
            $machineName = $deduction->machine_name;
            // var_dump(in_array(strtolower($name), ['cashadvance', 'cash advance']));
            // var_dump(strtolower($name), in_array(strtolower($name), ['cashadvance', 'cash advance']));
            // BIR or TAX should be separate because it requires all benefits and deductions
            if (!in_array(strtolower($name), ['bir', 'tax', 'cashadvance', 'cash advance'])) {
                $this->computeItem(
                    $employee,
                    $employeeTemplate,
                    $payroll,
                    $this->payrollItemRepository->makeModel()::TYPE_DEDUCTION,
                    $deduction,
                    $name,
                    $machineName
                );
            } else if (in_array(strtolower($name), ['cashadvance', 'cash advance'])) {
                $this->computeItem(
                    $employee,
                    $employeeTemplate,
                    $payroll,
                    $this->payrollItemRepository->makeModel()::TYPE_ADJ_DEDUCTION,
                    $deduction,
                    $name,
                    $machineName
                );
            } else {

                $hasTax = true;
            }
        }

        $searchingFor = [
            $this->payrollItemRepository->makeModel()::TYPE_ADJ_EARNING,
            $this->payrollItemRepository->makeModel()::TYPE_ADJ_DEDUCTION
        ];


        /*********************
         ********************* 
         **** Adjustments ****
         *********************
         * 
         *  Are payroll adjustments on top of
         *  the gross pay
         * 
         **/
        $adjustments = $this->trashedItems->filter(function($item) use ($searchingFor) {
            return in_array($item->type, $searchingFor);
        });
        foreach($adjustments as $adjustment) {
            $adjustment->restore();
        } 
        


        /*********************
         ********************* 
         ******** TAX ********
         *********************
         *
         *  This is calculated Last
         * 
         **/
        if ($hasTax) {
            $name = 'Tax';
            $machineName = 'BIR';
            
            $this->computeItem(
                $employee,
                $employeeTemplate,
                $payroll->fresh(),
                $this->payrollItemRepository->makeModel()::TYPE_DEDUCTION_WTAX,
                $deduction,
                $name,
                $machineName
            );
        }
    }

    public function computeItem(
        $employee,
        $employeeTemplate,
        $payroll,
        $type,
        $benefit,
        $name,
        $machineName
    ) {
        $calc = null;
        $totalNet = 0;
        $totalGross = 0;

        $calculatorClass = '\Sunnydevbox\TWPim\Core\Payroll\Calculators\\' . $machineName . '\\' . $machineName;
        
        if ($machineName && class_exists($calculatorClass)) {
            $class = $calculatorClass;
            $calc = new $class($benefit);
            $calc = $calc->setPayrollObject($payroll)->setEmployee($employee)->setEmployeeTemplate($employeeTemplate);
        }

        $i = $this->trashedItems->where('type', $type)->where('name', $name)->first();


        if ($calc && $calc->enabled()) {
            $calc = $calc->process();
            $totalNet = $calc->getTotalNet();
            $totalGross = $calc->getTotalGross();
            
            $data = [
                'payroll_id'    => $payroll->id,
                'type'          => $type,
                'name'          => $name,
                'total_gross'   => $totalGross,
                'total_net'     => $totalNet,
                'data'          => $calc->getData(),
            ];

            if ($i) {
                $i->restore();
                $i->fill($data)->update();
            } else {
                $this->payrollItemRepository->create([
                    'payroll_id'    => $payroll->id,
                    'type'          => $type,
                    'name'          => $name,
                    'total_gross'   => $totalGross,
                    'total_net'     => $totalNet,
                    'data'          => $calc->getData(),
                ]);
            }
        }
    }

}