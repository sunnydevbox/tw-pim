<?php
namespace Sunnydevbox\TWPim\Services;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\Exceptions\ValidatorException;
use \Carbon\Carbon;
use DB;
use \Sunnydevbox\TWPim\Services\Payroll\Processing\ComputeEarningDeductionTrait;
use Mockery\CountValidator\Exception;

class PayrollLogService
{
    use ComputeEarningDeductionTrait;

    /**
     * Triggers the generation of payrolls
     */
    public function generate($data)
    {
        // Create log entry
        try {
            $this->payrollLogValidator->with($data)->passesOrFail('payroll_generate');

            $payrollLog = $this->payrollLogRepository
                            ->with(['payroll_template.employees'])
                            ->find($data['payroll_log_id']);
            if ($payrollLog) {
                if ($payrollLog->isDone()) {
                    return [
                        'status_code'   => 200,
                        'message'       => 'payroll_previously_generated'
                    ];
                }

                if ($payrollLog->isProcessing()) {
                    return [
                        'status_code'   => 200,
                        'message'       => 'payroll_currently_processing'
                    ];
                }

                if ($payrollLog->isPending()) {


                    $payrollLog->setStatus('processing');

                    $startDate = $payrollLog->start;
                    $endDate = $payrollLog->end;

                    $employees = $payrollLog->payroll_template->employees()
                                    ->with([
                                        'benefits',
                                        'deductions',
                                        'timelogs' =>  function($query) use ($startDate, $endDate) {
                                            $query
                                                ->approved()
                                                ->dateRange($startDate, $endDate)
                                            ;
                                        }
                                    ])
                                    ->has('timelogs')
                                    ->get();

                    $payrollLog->setPayrollCount($employees->count());
                    $payrollLog->setCurretProgressCount(0);
                    
                    foreach($employees as $employee) {
                        if ($employee->timelogs->count()) {
                            // CHECK IF EMPLOYEE ALREADY HAS A GENERATED PAYROLL. 
                            // IF YES THEN SKIP

                            if (!$this->payrollRepository->exists($employee->id, $payrollLog->id)) {

                                DB::transaction(function () use ($payrollLog, $employee) {  
                                    
                                    $payroll = $this->payrollRepository->init($payrollLog, $employee);

                                    $this->processEarningDeduction($employee, $payroll);

                                    // Calculate totals
                                    // ...
                                    // 1) Calculate GROSS
                                    $earnings = $payroll->items->where('type', $this->payrollItemRepository->makeModel()::TYPE_EARNING);
                                    $totalGross = $earnings->reduce(function($carry, $item) {
                                        return $carry + $item->total_net;
                                    });

                                    // 2) Calculate 
                                    $deductions = $payroll->items->where('type', $this->payrollItemRepository->makeModel()::TYPE_DEDUCTION);
                                    $totalDeduction = $earnings->reduce(function($carry, $item) {
                                        return $carry + $item->total_net;
                                    });

                                    $employee->timelogs->each(function($timelog) {
                                        $timelog->update([
                                        'is_payroll_generated' => true,
                                        ]);
                                    });

                                    $payroll->total_gross = $totalGross;
                                    $payroll->total_net = $totalGross - $totalDeduction;
                                    $payroll->save();

                                    // CLOSE THE ENTRY
                                    // $payroll->status = $payroll::STAUTS_COMPUTED;
                                    $payroll->save();

                                    //dd($payroll);
                                });

                                $payrollLog->setCurretProgressCount();
                            }

                        } 
                        // else {
                        //     echo $employee->id . ' NO timelogs';
                        // }
                    }

                    $payrollLog->setStatus('done');

                    return [
                        'status_code'   => 200,
                        'message'       => 'payroll_generation_done',
                    ];
                }
            } else {
                throw new \Exception('invalid_payroll_template');
            }
        } catch(ValidatorException $e) {
            return [
                'status_code'   => 400,
                'message'       => $e->getMessageBag()
            ];
        }
    }

    public function store($data)
    {
        // CHECK if payroll logs  already exists
        try {
            $validationRule = (isset($data['custom'])  &&  $data['custom']) ? 'custom_create' : ValidatorInterface::RULE_CREATE;

            $this->payrollLogValidator->with($data)->passesOrFail($validationRule);
            
            if ($validationRule == 'custom_create') {
                $d = collect($data)->only([
                    'start', 
                    'end', 
                    'payroll_template_id', 
                    'is_custom', 
                    'include_sss',
                    'pay_date',
                    'period_count',
                    'pay_coverage_start',
                    'pay_coverage_end',
                ])->all();

                $d['is_custom'] = true;

                $row = $this->payrollLogRepository->firstOrCreate($d);

            } else {
                $periods = $this->payrollTemplateRepository->generateSelectablePeriods(
                    $data['payroll_template_id'], $data['month'], $data['year']
                );

                foreach($periods as $period) {
                    $row = $this->payrollLogRepository->firstOrCreate([
                        'start'                 => $period[0],
                        'end'                   => $period[1],
                        'payroll_template_id'   => $data['payroll_template_id'],
                    ]);
                    
                }
            }
            // dd($periods);

            
        } catch (ValidatorException $e) {
            // throw new Exception(())
            return response()->json([
                'status_code'   => 400,
                'message' => $e->getMessageBag()
            ], 400);
        }
    }

    public function update($data, $id)
    {
        try {
            $this->payrollLogValidator->with($data)->passesOrFail('custom_create');
            
            $d = collect($data)->only([
                'start', 
                'end', 
                'payroll_template_id', 
                'is_custom', 
                'include_sss',
                'pay_date',
                'period_count',
                'pay_coverage_start',
                'pay_coverage_end',
            ])->all();
            $d['is_custom'] = true;
            
            $result = $this->payrollLogRepository->update($d, $id);

            return $result;
        } catch (ValidatorException $e) {
            return response()->json([
                'status_code'   => 400,
                'message' => $e->getMessageBag()
            ], 400);
        }
    }

    public function __construct(
        \Sunnydevbox\TWPim\Repositories\Payroll\PayrollRepository $payrollRepository,
        \Sunnydevbox\TWPim\Repositories\Payroll\PayrollTemplateRepository $payrollTemplateRepository,
        \Sunnydevbox\TWPim\Repositories\Payroll\PayrollLogRepository $payrollLogRepository,
        \Sunnydevbox\TWPim\Validators\PayrollLogValidator $payrollLogValidator,
        \Sunnydevbox\TWPim\Repositories\Payroll\PayrollItemRepository $payrollItemRepository
    ) {
        $this->payrollRepository = $payrollRepository;
        $this->payrollTemplateRepository = $payrollTemplateRepository;
        $this->payrollLogRepository = $payrollLogRepository;
        $this->payrollLogValidator = $payrollLogValidator;
        $this->payrollItemRepository = $payrollItemRepository;
    }
}