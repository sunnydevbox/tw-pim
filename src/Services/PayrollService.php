<?php
namespace Sunnydevbox\TWPim\Services;

use Sunnydevbox\TWPim\Services\EmployeeIDAbstract;
use DB;
use PDF;

class PayrollService
{
    use \Sunnydevbox\TWPim\Services\Payroll\Processing\ComputeEarningDeductionTrait;

    public function generatePayslip($data)
    {
        $payroll = $this->payrollRepository
                    ->with(['items', 'employee.timelogs', 'employee.civil_status', 'payroll_log.payroll_template'])
                    ->find($data['payroll_id']);
        
        $filename = 'payslip-' . $payroll->id . '-' . $payroll->employee_id .'.pdf';
        $targetFile = storage_path($filename);
        
        // return view('vendor.tw-pim.pdf.payslip.template1', ['payroll' => $payroll]);
        $pdf = PDF::loadHTML(view('vendor.tw-pim.pdf.payslip.template1', ['payroll' => $payroll]))
                ->setPaper('a4', 'landscape')
                ->setWarnings(false);
        
        if ($data['action'] == 'view') {
            return $pdf->stream($filename);
        } else {
            return $pdf->download($filename);
        }
    }

    public function recalculate($data)
    {
        if (isset($data['payroll_id']) && $data['payroll_id'] == 'all') {
            $payrolls = $this->payrollRepository->makeModel()
            ->with([
                // 'employee', 'employee.deductions', 'employee.benefits', 
                'payroll_log',])
            ->get();
        } else { 
            // VALIDATION
            $payroll = $this->payrollRepository->makeModel()
                        ->with([
                            // 'employee', 'employee.deductions', 'employee.benefits', 
                            'payroll_log',])
                        ->find($data['payroll_id']);
            $payrolls[] = $payroll;
        }

        if (!$payrolls) {
            throw new \Exception('invalid_payroll_id', 404);
        }

        foreach($payrolls as $payroll) {
            
            $payroll = $this->payrollRepository->makeModel()
                    ->with([
                        'employee.deductions', 
                        'employee.benefits', 
                        'payroll_log',
                        'employee.timelogs' => function($query) use ($payroll) {
                            $query->dateRange($payroll->payroll_log->start, $payroll->payroll_log->end)
                                    ->where('employee_id', $payroll->employee_id)
                                    ->approved()
                                    ->orderBy('time_in');
                        }
                    ])
                    // ->find($data['payroll_id'])
                    ->find($payroll->id)
                    ;

            if ($payroll) {
                // DELETE THE EXISTING ITEMS OF THIS PAYROLL
                // RESET THE PAYROLL'S STATUS
                
                $payroll->items()->delete();
                $payroll->initialize();
                
                $this->process($payroll);
                // $result = $this->payrollLogService->generate(['payroll_log_id' => $payroll->payroll_log_id]);
            }
        }

        
    }


    public function process($payroll)
    {
        
        DB::transaction(function () use ($payroll) {                          
            
            $this->processEarningDeduction($payroll->employee, $payroll);
            
            $payroll->employee->timelogs->each(function($timelog) {
                $timelog->update([
                    'is_payroll_generated' => true,
                ]);
            });

            $payroll->save();

            // CLOSE THE ENTRY
            //$payroll->status = $payroll::STAUTS_COMPUTED;
        });
    }


    public function calculatePayroll($payrollId)
    {
        $payroll = $this->payrollRepository->with('items')->find($payrollId);
        
        if ($payroll->items->count() == 0) {
            return null;
        }    

        if ($payroll) {
            $earnings = 0;
            $deductions = 0;
            $adjEarnings = 0;
            $adjDeductions = 0;
            $totalGross = 0;
            $totalNet = 0;
            $wTax = 0;

            $basicPayBreakdown = [];

            foreach($payroll->items as $item) {
                if ($item->type == $item::TYPE_EARNING) {
                    $earnings += $item->total_net;

                    $name = strtolower($item->name);
                    if ($name == 'basic pay') {
                        $payroll->hourly_rate = isset($item->data['hourly_rate']) ? $item->data['hourly_rate'] : 0;
                        $payroll->daily_rate = isset($item->data['daily_rate']) ? $item->data['daily_rate'] : 0;
                        $payroll->total_hours = isset($item->data['total_hours']) ? $item->data['total_hours'] : 0;
                        $payroll->total_days = isset($item->data['total_days']) ? $item->data['total_days'] : 0;
                        $payroll->basic_pay = isset($item->data['basic_pay']) ? $item->data['basic_pay'] : 0;
                    } else if ($name == 'overtime') {


                        // total_minutes' => $this->totalOvertimeMinutes,
                        // 'total_hours'   => $this->totalOvertimeMinutes / 60,
                        // 'hourly_rate'   => $this->hourlyRate,
                        
                        // 'total_overtime_pay' => ($this->totalOvertimeMinutes / 60) * $this->hourlyRate,
                        // 'total_overtime' => $this->totalOvertimeMinutes,


                        $payroll->total_overtime = isset($item->data['total_hours']) ? $item->data['total_hours'] : 0;
                        $payroll->total_overtime_pay = isset($item->data['total_overtime_pay']) ? $item->data['total_overtime_pay'] : 0;
                        
                        
                        // $payroll->total_hours = isset($item->data['total_hours']) ? $item->data['total_hours'] : 0;
                        // $payroll->total_days = isset($item->data['total_days']) ? $item->data['total_days'] : 0;
                        // $payroll->basic_pay = isset($item->data['basic_pay']) ? $item->data['basic_pay'] : 0;
                    } else if ($name == 'rest day') {
                        $payroll->total_restday = isset($item->data['total_restday']) ? $item->data['total_restday'] : 0;
                        $payroll->total_restday_pay = isset($item->data['total_restday_pay']) ? $item->data['total_restday_pay'] : 0;

                    } else if ($name == 'holiday pay') {
                        $payroll->total_holiday = 2;
                        $payroll->total_holiday_pay = 2;
                    }
                    

                } else if ($item->type == $item::TYPE_DEDUCTION) {
                    $deductions += $item->total_net;
                } else if ($item->type == $item::TYPE_ADJ_EARNING) {
                    $adjEarnings += $item->total_net;
                } else if ($item->type == $item::TYPE_ADJ_DEDUCTION) {
                    $adjDeductions += $item->total_net;
                }  else if ($item->type == $item::TYPE_DEDUCTION_WTAX) {
                    $wTax += $item->total_net;
                } 
                // else if ($item->type == $item::TYPE_DEDUCTION_CA) {
                //     $adjDeductions += $item->total_net;
                // }
            }

            // Govt mandated earnings/deductions 
            $totalGross = $earnings;
            $totalDeduction = $deductions + $wTax; 

            // Adjustments. not included in tax calculation
            $totalAdjustments = $adjEarnings - $adjDeductions;
            
            $totalNet = ($totalGross - $deductions) + $totalAdjustments;

            $payroll->total_deduction = $totalDeduction;
            $payroll->total_gross = $totalGross;

            $payroll->total_adj_earnings = $adjEarnings;
            $payroll->total_adj_deductions = $adjDeductions;
            $payroll->total_adjustments = $totalAdjustments;
            $payroll->wtax = $wTax;
            
            $payroll->total_net = $totalGross - $totalDeduction;
            
            
            

            //$totalNet-=$wTax;

            /**
             * 40% - Threshold of takehome pay amount 
             */
            $variable = 0.40;

            $payroll->status = ($payroll->total_net <= ($payroll->total_gross & $variable)) 
                                ? $payroll::STATUS_BELOW40 
                                : $payroll::STATUS_COMPUTED;

                                
            $payroll->update();

        }
    }

    public function __construct(
        \Sunnydevbox\TWPim\Repositories\Employee\EmployeeRepository $employeeRepository,
        \Sunnydevbox\TWPim\Repositories\Payroll\PayrollRepository $payrollRepository,
        \Sunnydevbox\TWPim\Repositories\Payroll\PayrollItemRepository $payrollItemRepository,
        \Sunnydevbox\TWPim\Repositories\Payroll\PayrollTemplateRepository $payrollTemplateRepository,
        \Sunnydevbox\TWPim\Repositories\Payroll\PayrollLogRepository $payrollLogRepository,
        \Sunnydevbox\TWPim\Repositories\Timelog\TimelogRepository $timelogRepository,
        \Sunnydevbox\TWPim\Services\PayrollLogService $payrollLogService,
        \Sunnydevbox\TWPim\Services\PayrollItemService $payrollItemService
    ) {
        $this->payrollRepository = $payrollRepository;
        $this->employeeRepository = $employeeRepository;
        $this->payrollTemplateRepository = $payrollTemplateRepository;
        $this->payrollLogRepository = $payrollLogRepository;
        $this->timelogRepository = $timelogRepository;
        $this->payrollItemRepository = $payrollItemRepository;
        $this->payrollLogService = $payrollLogService;
        $this->payrollItemService = $payrollItemService;
    }
}
