<?php
namespace Sunnydevbox\TWPim\Core;

use Sunnydevbox\TWPim\Core\WorkDay;

class Rate
{
    private $WorkDay = null;
    protected $employee = null;
    protected $averageMonthDays = null;
    protected $monthlyRate = null;
    protected $totalWorkingDaysYear = null;
    protected $totalWorkingHoursDay = null;

    public function getHourly() 
    {
        // monthly gross salary is P25,000, and you go to work at an average of 20 days per month at 8 hours per day,
        if (!$this->getMonthlyRate() || !$this->getAverageMonthDays() || !$this->getTotalWorkingHoursDay()) {
            return null;
        }

        return $this->getMonthlyRate() / $this->getAverageMonthDays() / $this->getTotalWorkingHoursDay();
    }

    public function getDaily()
    {
        // (Monthly Rate X 12) / total working days in a year / total working hours per day

        if (!$this->getMonthlyRate() && !$this->getTotalWorkingDaysYear() && !$this->getTotalWorkingHoursDay()) {
            return null;
        }

        return ($this->getMonthlyRate() * 12) / $this->getTotalWorkingDaysYear() / $this->getTotalWorkingHoursDay();
    }

    public function getMonthly()
    {
        return $this->getMonthlyRate();
    }


    /*******************/

    public function setEmployee($employee)
    {
        $this->employee = $employee;
        // dd($employee->template[0]->total_days_in_year);
        // dd(
        //     $this->WorkDay->setShiftTemplate($employee->shiftTemplates[0])
        //         ->countByMonth()
        // );

        $this->WorkDay->setEmployee($this->employee);
    
        return $this;
    }

    public function getEmployee()
    {
        return $this->employee;
    }

    public function setAverageMonthDays($averageMonthDays)
    {
        $this->averageMonthDays = $averageMonthDays;

        return $this;
    }

    public function getAverageMonthDays()
    {
        return $this->getTotalWorkingDaysYear() / 12;
    }


    public function setMonthlyRate($monthlyRate)
    {
        $this->monthlyRate = $monthlyRate;

        return $this;
    }

    public function getMonthlyRate()
    {
        if ($this->monthlyRate) {
            return $this->monthlyRate;
        } else if ($this->getEmployee()) {
            return $this->employee->monthly_rate;
        }
        
        return null;
    }

    public function setTotalWorkingDaysYear($totalWorkingDaysYear)
    {
        $this->totalWorkingDaysYear = $totalWorkingDaysYear;

        return $this;
    }

    public function getTotalWorkingDaysYear()
    {
        if ($this->totalWorkingDaysYear) {
            return $this->totalWorkingDaysYear;
        } else {
            return $this->employee->employee_template->total_days_in_year;
        }
    }

    public function setTotalWorkingHoursDay($totalWorkingHoursDay)
    {
        $this->totalWorkingHoursDay = $totalWorkingHoursDay;

        return $this;
    }

    public function getTotalWorkingHoursDay()
    {
        if ($this->totalWorkingHoursDay) {
            return $this->totalWorkingHoursDay;
        } else {
            return $this->employee->employee_template->total_hours_in_day;
        }
    }


    public function __construct($employee = null)
    {
        $this->WorkDay = new WorkDay;

        if ($employee) {
            $this->setEmployee($employee);
            $this->WorkDay->setEmployee($employee);
        }
    }
}