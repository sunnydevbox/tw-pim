<?php
namespace Sunnydevbox\TWPim\Core;

use Carbon\Carbon;
use Sunnydevbox\TWPim\Repositories\Holiday\HolidayRepository;
use Sunnydevbox\TWPim\Services\LeaveApplicationService;
use Sunnydevbox\TWPim\Services\HolidayService;
USE Sunnydevbox\TWPim\Models\Timelog;
use Sunnydevbox\TWPim\Core\Rate;

class DateCalculator
{
    public function isOverlapped_2($date1Start, $date1End, $date2Start, $date2End)
    {
        if (
            ($date1Start->between($date2Start, $date2End) 
            || 
            $date1End->between($date2Start, $date2End) 
            ||
            $date2Start->between($date1Start, $date1End))
        ) {
            return true;
        }
        


        // if (
        //     $date1Start->between($date2Start, $date2End) 
        //     || 
        //     $date1End->between($date2Start, $date2End) 
        //     ||
        //     // $date2Start->between($date1Start, $date1End))
        //     ($date1Start->gte($date2Start) && $date1End->lte($date2End))
        //     || 
        //     ($date1Start->lte($date2Start) && $date1End->gte($date2End))
        // ) {


        // if (
        //     ($date1Start >= $date2Start && $date1Start <= $date2End) 
        //     || ($date1End >= $date2Start && $date1End <= $date2End) 
        //     || ($date2Start >= $date1Start && $date2Start <= $date1End) 
        //     || ($date2End >= $date1Start && $date2End <= $date1End)
        // ) {
        
        // Lord help me with this logic
        // if (
        //     ($date1Start->gte($date2Start) && $date1End->lte($date2End))
        //     || ($date1Start->lte($date2Start) && $date1End->gte($date2End))
        //     || ($date1Start->gte($date2Start) && $date1End->gte($date2End))
        //     || ($date1Start->lte($date2Start) && $date1End->lte($date2End))
        // ) {
        //     return true;
        // }

        return false;
    }

    public function isOverlapped($date1Start, $date1End, $date2Start, $date2End)
    {
        if (
            ($date1Start->gte($date2Start) && $date1End->lte($date2End)) 
            || ($date1Start->lte($date2Start) && $date1End->gte($date2End))
                || ($date1End->between($date2Start, $date2End))
            || ($date1Start->between($date2Start, $date2End))
        ) {
            return true;
        }

        return false;
    }

    public function isNightDifferential($dateStart, $dateEnd = null)
    {
        if (
            (
                // 12AM - 5:59AM
                $this->isNightDifferential_AM($dateStart)
                // 10PM - 11:59PM
                || $this->isNightDifferential_PM($dateStart)
            )
            ||
            (
                $dateEnd &&
                (// 12AM - 5:59AM
                    $this->isNightDifferential_AM($dateEnd)
                // 10PM - 11:59PM
                || $this->isNightDifferential_PM($dateEnd))
            )
            ||
            ($dateEnd && ($this->isOverlapped(
                $dateStart, 
                $dateEnd,
                $dateStart->copy()->hour(22)->minute(0), 
                $dateEnd->copy()->tomorrow()->hour(5)->minute(59))
            ))
        ) {
            return true;
        }

        return false;
    }

    /**
     * Returns breakdown of time that fall into differential period
     * Determins the time periods for night diff and regular hours 
     */
    public function hoursBreakDown($dateStart, $dateEnd = null)
    {   
        $maxRegularHours = 8; // 8 hours
        $timelogStart = null;
        $timelogEnd = null;

        if ($dateStart instanceof Timelog) {
            $ds = $dateStart;
            $dateStart = $dateStart->time_in;
            $dateEnd = $ds->time_out;

            $timelogStart = $dateStart->copy();
            $timelogEnd = $dateEnd->copy();
        } else {
            $timelogStart = $dateStart->copy();
            $timelogEnd = $dateEnd->copy();
        }

        $hasOvertime = $timelogStart->diffInHours($timelogEnd) > $maxRegularHours ? true : false;
        $overtimeStart = ($hasOvertime) ? $timelogStart->copy()->addHours($maxRegularHours) : null;
        $overtimeEnd = ($hasOvertime) ? $timelogEnd->copy() : null;

        $data = collect([
            'night_differential' => collect(),
            'regular' => collect(),
            'night_differential_pairs' => collect(),
            'regular_pair' => collect(),

            'stats' => collect([
                'total_night_differential' => 0,
                'total_minutes' => 0,
            ]), 
    
            'total_regular' => 0,
            'total_regular_night_differential' => 0,
            'total_regular_overtime' => 0,
            'total_overtime_night_differential' => 0,        
        ]);

        // START IN
        if ($this->isNightDifferential_AM($dateStart)) {
            $data['night_differential'][] = $dateStart;
        } else if ($this->isNightDifferential_PM($dateStart)) {
            $data['night_differential'][] = $dateStart;
        } else if ($this->isRegularHours($dateStart)) {
            $data['regular'][] = $dateStart;
        }

        // START OUT
        if ($this->isNightDifferential_PM($dateEnd)) {
            if ($this->isRegularHours($dateStart)) {
                $data['regular'][] = $this->regularHours_End($dateStart);
                $dateStart = $this->nightDifferentialPM_Start($dateStart);
            } else if ($this->isNightDifferential_AM($dateStart)) {
                $data['night_differential'][] = $this->nightDifferentialAM_End($dateStart);

                // RESET $dateStart
                $dateStart = $this->regularHours_Start($dateStart);
            } else if ($this->isNightDifferential_PM($dateStart)) {
                $data['night_differential'][] = $dateEnd;
                $dateStart = null;
            }

        } else if ($this->isNightDifferential_AM($dateEnd)) { 
            $data['night_differential'][] = $dateEnd;
            $dateStart = null;
            $dateEnd = null;

        } else if ($this->isRegularHours($dateEnd)) {
            if ($this->isNightDifferential_AM($dateStart)) {
                $data['night_differential'][] = $this->nightDifferentialAM_End($dateStart);
                $dateStart = $this->regularHours_Start($dateStart);
            } else if ($this->isRegularHours($dateEnd)) {
                $data['regular'][] = $dateEnd;
                $dateStart = null;
                $dateEnd = null;
            }
        }

        // MIDDLE IN
        if ($this->isRegularHours($dateStart)) {
            $data['regular'][] = $dateStart;
        }

        // MIDDLE OUT
        if ($this->isNightDifferential_PM($dateEnd)) {
            if ($this->isRegularHours($dateStart)) {
                $data['regular'][] = $this->regularHours_End($dateStart);
                $dateStart = $this->nightDifferentialPM_Start($dateStart);
            }
        } else if ($this->isRegularHours($dateEnd)) {
            $data['regular'][] = $dateEnd;
        }

        // END IN
        if ($this->isNightDifferential_PM($dateStart)) {
            $data['night_differential'][] = $dateStart;
        }

        // END OUT
        if ($this->isNightDifferential_PM($dateEnd)) {
            if ($dateStart) {
                $data['night_differential'][] = $dateEnd;
            }
        }

        $ndPair = [];
        foreach($data['night_differential'] as $nd) {
            if (count($ndPair) < 2) {
                $ndPair[] = $nd;   
            }
            
            if (count($ndPair) == 2) {
                $data['night_differential_pairs'][] = $ndPair;
                $ndPair = [];
            }
        }

        $regularPair = [];
        foreach($data['regular'] as $regular) {
            if (count($regularPair) < 2) {
                $regularPair[] = $regular;   
            }
            
            if (count($regularPair) == 2) {
                $data['regular_pair'][] = $regularPair;
                $regularPair = [];
            }
        }

        foreach($data['night_differential_pairs'] as $pair) {
            $s1 = $s2 = null;
            $data['stats']['total_night_differential'] += ($pair[0]->diffInMinutes($pair[1]));

            // THis is to check if the time range 
            // falls inside the OVERTIME time range
            if ($hasOvertime) {

                if ($this->nightDifferentialPM_Start($overtimeStart)->between($pair[0], $pair[1])) {
                    
                    $s1 = $pair[0];
                    $s2 = $overtimeEnd;
                
                    $data['total_overtime_night_differential'] += $s1->diffInMinutes($s2);
                } 
                // else if ($overtimeStart->between($pair[0], $pair[1])) {
                //     var_dump($s1, $s2,  $pair[0], $pair[1], $overtimeStart, $overtimeEnd);//, $s1->diffInMinutes($s2));
                //     echo '<hr/>';
                // }    
            }
            
            if ($this->isNightDifferential_AM($pair[0])) {
                $data['total_regular_night_differential'] += $pair[0]->diffInMinutes($pair[1]);
            }
        }

        foreach($data['regular_pair'] as $pair) {
            $s1 = $s2 = null;

            // THis is to check if the time range 
            // falls inside the OVERTIME time range
            if ($hasOvertime) {
                if ($overtimeStart->between($pair[0], $pair[1])) {
                    $s1 = $overtimeStart->copy();
                }

                if ($overtimeEnd->gte($pair[1])) {
                    $s2 = $pair[1];
                }

                $data['total_regular_overtime'] += $s1->diffInMinutes($s2);
            }

            // GET the time range that is regular
            
            $data['total_regular'] += $pair[0]->diffInMinutes($overtimeStart);
        
                
            
        }

        $data['stats']['total_minutes'] = $timelogStart->diffInMinutes($timelogEnd);

        return collect($data);
    }

    public function isNightDifferential_AM($date)
    {
        if (!$date) return null;

        return $date->between(
            $this->nightDifferentialAM_Start($date),
            $this->nightDifferentialAM_End($date)
        ) ? true : false;
    }

    public function isNightDifferential_PM($date)
    {
        if (!$date) return null;

        return $date->between(
            $this->nightDifferentialPM_Start($date),
            $this->nightDifferentialPM_End($date)
        ) ? true : false;
    }

    public function isRegularHours($date)
    {
        if (!$date) return null;

        return $date->between(
           $this->regularHours_Start($date),
           $this->regularHours_End($date)
        ) ? true : false;
    }

    public function regularHours_Start($date = null)
    {
        if (!$date) {
            $date = Carbon::now();
        }

        $h = 6;
        $m = 0;
        return $date->copy()->hour($h)->minute($m);
    }

    public function regularHours_End($date = null)
    {
        if (!$date) {
            $date = Carbon::now();
        }

        $h = 21;
        $m = 59;
        $s = 59;
        return $date->copy()->hour($h)->minute($m)->second($s);
    }

    public function nightDifferentialAM_Start($date = null)
    {
        if (!$date) {
            $date = Carbon::now();
        }

        return $date->copy()->startOfDay();
    }

    public function nightDifferentialAM_End($date = null)
    {
        if (!$date) {
            $date = Carbon::now();
        }

        $h = 5;
        $m = 59;
        $s = 59;
        return $date->copy()->hour($h)->minute($m)->second($s);
    }

    public function nightDifferentialPM_Start($date = null)
    {
        if (!$date) {
            $date = Carbon::now();
        }

        $h = 22;
        $m = 0;

        return $date->copy()->hour($h)->minute($m);
    }

    public function nightDifferentialPM_End($date = null)
    {
        if (!$date) {
            $date = Carbon::now();
        }

        return $date->copy()->endOfDay();
    }


    /**** OT *****/
    public function getActualOTRenderd($renderedStart, $renderedEnd, $OTstart, $OTend)
    {
        $data = [
            'time_in' => null,
            'time_out' => null
        ];

        if ($this->isOverlapped($renderedStart, $renderedEnd, $OTstart, $OTend)) {

            $data['time_in'] = ($renderedStart->lte($OTstart))? $OTstart : $renderedStart;
            $data['time_out'] = ($renderedEnd->gte($OTend))? $OTend : $renderedEnd;

            return $data;
        }

        return false;
    }



    public function computer(Timelog $timelog, $hourlyRate = null, $returnBreakDown = false)
    {
        if (is_null($hourlyRate)) {
            $rate = new Rate;
            $rate = $rate->setEmployee($timelog->employee);
            $hourlyRate = $rate->getHourly();
        }

        $breakDown = $this->hoursBreakDown($timelog->time_in, $timelog->time_out);

        $holiday = app(HolidayRepository::class);
        $leaveApplicationService = app(LeaveApplicationService::class);
        $applications = $leaveApplicationService->getApplicationsByTimelog($timelog);

        $isDoubleHoliday = false;
        $isSpecialHoliday = null;
        $isRegularHoliday = null;
        $isRestDay = $timelog->isRestDay();
        $isNightShift = false;
    
        // THERE are leave applications        
        if (count($applications)) {

            $applications->each(function($item) use (&$isRegularHoliday, & $isSpecialHoliday, &$isDoubleHoliday) {
                if($item->holiday) {
                    if (is_null($isSpecialHoliday)) {
                        $isSpecialHoliday = $item->holiday->isSpecialHoliday();
                    }
                    if (is_null($isRegularHoliday)) {
                        $isRegularHoliday = $item->holiday->isRegularHoliday();
                    }
                    
                    if (is_bool($isSpecialHoliday) && is_bool($isRegularHoliday)) {
                        $isDoubleHoliday = ($isSpecialHoliday && $isRegularHoliday);
                    }
                }
    
            });
        } else {
            // $applications
            // THIS MUST BE OVERRIDEN BY LEAVE APPLICATION
            $matchedHoliday = $holiday->with('type')->findWhere(['start_date' => $timelog->time_in->format('Y-m-d')]);
        }

        $computed = collect();
        
        // Regular hours
        $basePay = $hourlyRate * ($breakDown['total_regular'] / 60);
        $isOvertime = false;
        $isNightShift = false;
        $computed['regular_hours'] = number_format($basePay * $this->getMultiplier($isDoubleHoliday, $isSpecialHoliday, $isRegularHoliday, $isRestDay, $isOvertime,$isNightShift), 2);
        

        // Regular hours OT time range
        $basePay = $hourlyRate * ($breakDown['total_regular_overtime'] / 60);
        $isOvertime = false;
        $isNightShift = false;
        $computed['ovetime_regular_hours'] = number_format($basePay * $this->getMultiplier($isDoubleHoliday, $isSpecialHoliday, $isRegularHoliday, $isRestDay, $isOvertime,$isNightShift), 2);

        // Night shift hours
        $basePay = $hourlyRate * ($breakDown['total_regular_night_differential'] / 60);
        $isOvertime = false;
        $isNightShift = true;
        $computed['regular_night_differential'] = number_format($basePay * $this->getMultiplier($isDoubleHoliday, $isSpecialHoliday, $isRegularHoliday, $isRestDay,$isOvertime,$isNightShift), 2);

        // Night shift hours with OT time range
        $basePay = $hourlyRate * ($breakDown['total_overtime_night_differential'] / 60);
        $isOvertime = true;
        $isNightShift = true;
        $computed['overtime_night_differential'] = number_format($basePay * $this->getMultiplier($isDoubleHoliday, $isSpecialHoliday, $isRegularHoliday, $isRestDay,$isOvertime,$isNightShift), 2);

        $return = ['computed' => $computed];

        if ($returnBreakDown) {
            $return['breakdown'] = $breakDown;
        }
        
        return collect($return);
        dd($computed, $breakDown);

    }


    public function getMultiplier(
        $isDoubleHoliday,
        $isSpecialHoliday,
        $isRegularHoliday,
        $isRestDay,
        $isOvertime = false,
        $isNightShift = false
    ) {
        // Doublie Holiday
        if ($isDoubleHoliday) {

            $multiplier = 3;

            if ($isRestDay) {
                $multiplier = 3.9;
            }

            if ($isOvertime) {
                $multiplier = 3.9;
            }

            if ($isRestDay && $isOvertime) {
                $multiplier = 5.07;
            }

            if ($isNightShift) {
                $multiplier = 3.3;
            }

            if ($isRestDay && $isNightShift) {
                $multiplier = 3.9;
            }

        } else if (!$isDoubleHoliday && $isSpecialHoliday) {
            
            $multiplier = 1.3;

            if ($isRestDay) {
                $multiplier = 1.5;
            }

            if ($isOvertime) {
                $multiplier = 1.69;
            }

            if ($isRestDay && $isOvertime) {
                $multiplier = 1.95;
            }

            if ($isNightShift) {
                $multiplier = 1.3;
            }

            if ($isRestDay && $isNightShift) {
                $multiplier = 1.5;
            }

        } else if (!$isDoubleHoliday && $isRegularHoliday) {

            $multiplier = 2;

            if ($isRestDay) {
                $multiplier = 2.6;
            }

            if ($isOvertime) {
                $multiplier = 2.6;
            }

            if ($isRestDay && $isOvertime) {
                $multiplier = 3.38;
            }

            if ($isNightShift) {
                $multiplier = 2;
            }

            if ($isRestDay && $isNightShift) {
                $multiplier = 2.6;
            }

        } else if ($isRestDay) {

            $multiplier = 1.3;

            if ($isOvertime) {
                $multiplier = 1.69;
            }

            if ($isNightShift) {
                $multiplier = 1.3;
            }

        } else {
            // REGULAR DAY

            $multiplier = 1;
            
            if ($isOvertime) {
                $multiplier = 1.25;
            }
        }

        if ($isNightShift) {
            $multiplier *= .10;
        }

        return $multiplier;
    }
}