<?php
namespace Sunnydevbox\TWPim\Core\Payroll;

use Sunnydevbox\TWPim\Core\Payroll\Factories\CalculatorFactory;
use Sunnydevbox\TWPim\Core\Rate;

class BenefitCalculatorAbstract
{
    var $employee = null;
    protected $payroll = null;
    protected $formula = null;
    protected $rate = null;
    protected $totalNet = null;
    protected $totalGross = null;
    protected $benefit = null;
    protected $data = [];

    public function process()
    {
        // Calculate gross
        $total = $this->calculator();

        // Calculate Net
        return $this;
    }

    public function setEmployee($employee)
    {
        $this->employee = $employee;

        $this->formula->setEmployee($employee);
        $this->rate->setEmployee($employee);
        return $this;
    }

    public function setEmployeeTemplate($employeeTemplate)
    {
        $this->employeeTemplate = $employeeTemplate;
        return $this;
    }

    public function getTotalNet()
    {
        return $this->totalNet;
    }

    public function getTotalGross()
    {
        return $this->totalGross;
    }

    public function getData()
    {
        return $this->data;
    }

    public function setBenefit($benefit)
    {
        $this->benefit = $benefit;
        return $this;
    }


    public function setPayrollObject($payrollObject)
    {
        $this->payroll = $payrollObject;
        return $this;
    }


    public function __construct($benefit) {
        $this->formula = new Rate;
        $this->rate = new Rate;
        $this->benefit = $benefit;
        $this->calculator = new CalculatorFactory($this);
        return $this;
    }
}