<?php
namespace Sunnydevbox\TWPim\Core\Payroll\Calculators\BasicPay;

use \Sunnydevbox\TWPim\Services\LeaveApplicationService;

class BasicPay 
    extends \Sunnydevbox\TWPim\Core\Payroll\BenefitCalculatorAbstract
    implements \Sunnydevbox\TWPim\Core\Payroll\Calculators\PayrollCalculatorInterface
{
    protected $hourlyRate = null;   
    protected $renderedMinutes = null; 

    public function calculator()
    {
   
        if ($this->enabled()) {

            // CHECK APPLICATON
            // dd(app(LeaveApplicationService::class));

            // GET HOURS and RENDERED MINUTES
            //dd($this->employee->timelogs);
            // $billableMinutes = $this->employee->timelogs->reduce(function($carry, $item) {
            //     return $carry + ($item->billable_minutes / 60);
            // }, 0);


            $renderedMinutes = $this->employee->timelogs->reduce(function($carry, $item) {

                // must not be restday , holiday, ov.. just regular day
                return $carry + ($item->billable_minutes);
            }, 0);
            
            // dd($this->employee->hourly_rate);
            $this->totalNet = $this->employee->monthly_rate; // $this->employee->hourly_rate * $billableMinutes;
            $renderedMinutes = 1961;
            
            $totalDays = convertMinutesToDays($renderedMinutes);
            $totalHours = convertMinutesToHours($renderedMinutes);
            $hourlyRate = $this->employee->hourly_rate;
            $dailyRate = $hourlyRate * 8;
            
            $this->data = [
                'billableMinutes' => $renderedMinutes,
                'total_days'    => $totalDays,
                'total_hours'   => $totalHours,
                'hourly_rate'   => $hourlyRate,
                'daily_rate'    => $dailyRate,
                'basic_pay'     => $dailyRate * $totalDays,
            ];
        //     'hourly_rate',
        // 'daily_rate',
        // 'total_hours',
        // 'total_days',

            // dd($this->data);
        }
        
    }

    public function getName()
    {
        return $this->benefit->name . " ({$this->employee->hourly_rate}/hr)";
    }

    public function enabled()
    {
        return $this->employeeTemplate->benefit_basic_pay;
    }
}