<?php
namespace Sunnydevbox\TWPim\Core\Payroll\Calculators\LoyaltyIncentive;

class DailyExtra 
    extends \Sunnydevbox\TWPim\Core\Payroll\BenefitCalculatorAbstract
    implements \Sunnydevbox\TWPim\Core\Payroll\Calculators\PayrollCalculatorInterface
{
    protected $hourlyRate = null;   
    protected $renderedMinutes = null; 

    public function calculator()
    {
   
        if ($this->enabled()) {
            /*
                    - conditions:
                    - # of service
                            - 6 - 10:  P15.00 added daily
                            - 11 - 15: P20
                            - 16 -20: P25
                            - 21 - 25: P30
                            - ….
                    - forumula:
                        - minimum wage / 8 / 2


                        ****************************************
            */

                        
            $daysRendered = $this->payroll->totalDays;
            $serviceDuration = $this->employee->serviceDurationInYears();

            $bonus = 0;
            if ($serviceDuration >= 6 && $serviceDuration <= 10) {
                $bonus = 15;
            } else if ($serviceDuration >= 11 && $serviceDuration <= 15) {
                $bonus = 20;
            } else if ($serviceDuration >= 16 && $serviceDuration <= 20) {
                $bonus = 25;
            } else if ($serviceDuration >= 21 && $serviceDuration <= 25) {
                $bonus = 30;
            } else if ($serviceDuration >= 26 && $serviceDuration <= 30) {
                $bonus = 35;
            }
            // dd($serviceDuration,$bonus);

            $this->totalNet = 0;$bonus;


            // $this->data = [
            //     'cash_advance_id' => 1,
            //     'cash_advance_log_id' => 1,
            //     'paid_amount' => 1
            // ];
        };
        
    }

    public function getName()
    {
        return 'Daily Extra';
    }

    public function enabled()
    {
        return $this->employeeTemplate->benefit_daily_extra;
    }
}