<?php
namespace Sunnydevbox\TWPim\Core\Payroll\Calculators\Philhealth;

use Sunnydevbox\TWPim\Core\Rate;

class Philhealth
    extends \Sunnydevbox\TWPim\Core\Payroll\BenefitCalculatorAbstract
    implements \Sunnydevbox\TWPim\Core\Payroll\Calculators\PayrollCalculatorInterface
{
    public function calculator()
    {
        $philhealthRepository = app('\Sunnydevbox\TWPim\Repositories\Philhealth\PhilhealthRepository');

        $this->totalNet = $philhealthRepository->getRange($this->rate->getMonthly())->employee_share;
    }

    public function getName()
    {
        return $this->benefit->name ;
    }

    public function enabled()
    {
        return false;
        return $this->employeeTemplate->deduction_philhealth;
    }
}