<?php
namespace Sunnydevbox\TWPim\Core\Payroll\Calculators\BIR;

use Sunnydevbox\TWPim\Repositories\Table\TaxRepository;

class BIR
    extends \Sunnydevbox\TWPim\Core\Payroll\BenefitCalculatorAbstract
    implements \Sunnydevbox\TWPim\Core\Payroll\Calculators\PayrollCalculatorInterface
{
    protected $hourlyRate = null;   
    protected $renderedMinutes = null; 

    public function calculator()
    {
        if ($this->enabled()) {

            // $earnings = $this->payroll->items->where('type', 'EARNING')->sum('total_net');
            // $deductions = $this->payroll->items->where('type', 'DEDUCTION')->sum('total_net');

            // reference: http://payrollhero.ph/bir_taxes

            $taxableIncome = $this->payroll->total_net;
            // dd($this->payroll);
            $taxRepository = app('\Sunnydevbox\TWPim\Repositories\Table\TaxRepository');
            $tax = $taxRepository->getRange(
                $this->employeeTemplate->tax_period,
                $taxableIncome
            );            

            //dd ($salary, $earnings, $deductions, $taxableIncome, $tax);
            /*
                Here’s an easy way on how to compute employee taxes for the month

                Basic Salary: Php 40,000.00
                Status: Single without dependent
                Late deduction: Php 357.83
                SSS Contribution: Php 581.30
                Philhealth Contribution: Php 437.50
                Pag-Ibig Contribution: Php 100.00

            
                Taxable income 
                = Monthly Basic Pay + Overtime Pay + Holiday Pay + Night 
                Differential - Tardiness - Absences - SSS/Philhealth/PagIbig deductions 
                = 40,000 - 357.83 - 581.30 - 437.50 - 100 
                = Php 38,523.37

                amount is 33,333 and the tax is Php 2,500.00 + 25% in excess of 33,333. 

                (($taxableIncome - $tax->salary_range_from) * $tax->withholding_percentage) + $tax->withholding_tax;

            */

           

            if (!$tax->salary_range_from) {
                $this->totalNet = 0; // $taxableIncome;
            } else {
                 // 1406.25
                $excess = $taxableIncome - $tax->salary_range_from;
                $compensationLevel = ($excess * $tax->withholding_percentage) + $tax->withholding_tax;
                $this->totalNet = $compensationLevel;
            }

            // dd( $taxableIncome, $excess, $tax->withholding_percentage,$tax->withholding_tax, $compensationLevel, $this->totalNet);



            $this->data = [
                'TAXABLE_INCOME'    => $taxableIncome,
                'PERIOD'            => $this->employeeTemplate->tax_period,
                'WITHHOLDING_TAX'   => $tax->withholding_tax,
                'CL_PERCENTAGE'     => $tax->withholding_percentage,
            ];

            //dd($this->data);
        }
    }

    public function getName()
    {
        return $this->benefit->name;
    }

    public function enabled()
    {
        return $this->employeeTemplate->deduction_bir;
    }
}