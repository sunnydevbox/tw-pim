<?php
namespace Sunnydevbox\TWPim\Core\Payroll\Calculators\RestDayPay;

use Sunnydevbox\TWPim\Services\LeaveApplicationService;
use Sunnydevbox\TWPim\Core\DateCalculator;
use Sunnydevbox\TWPim\Core\Rate;
use Sunnydevbox\TWPim\Repositories\Holiday\HolidayRepository;
use Sunnydevbox\TWPim\Repositories\SystemVariable\SystemVariableRepository;


class RestDayPay 
    extends \Sunnydevbox\TWPim\Core\Payroll\BenefitCalculatorAbstract
    implements \Sunnydevbox\TWPim\Core\Payroll\Calculators\PayrollCalculatorInterface
{
    protected $hourlyRate = null;   
    protected $renderedMinutes = null; 
    protected $restDayMultiplier = 1.3; // 130%


    public function calculator() 
    {
        if ($this->enabled()) {

            // DETERMINE IF THIS timelog has rendered hours
            // CHeck if there are any overrides in the calendar or applied vacation or OT
            // 

            // IF yes, do the necessary calculation
            $nighDiffMinutes = 0;
            $regularMinutes = 0;
            $regularPay = 0;
            $nightDiffPay = 0;

            $regular = 0;
            $regularND = 0;
            $regularOT = 0;
            $OTND = 0;

            $dateCalculator = new DateCalculator;
            
            $rate = new Rate;
            $rate = $rate->setEmployee($this->employee);
            $this->hourlyRate = $rate->getHourly(); 
            
            $total_restday = 0;
            $total_restday_pay = 0;

            $this->employee->timelogs->each(function($item, $key) use (
                $dateCalculator,
                &$total_restday_pay,
                &$total_restday
            ) {
                
                if ($item->isRestDay()) {
            
                    $computed = $dateCalculator->computer($item, $this->hourlyRate, true);
                    
                    // dd($computed['breakdown']);
                    $total_restday_pay += $computed['computed']->sum();
                    $total_restday += $computed['breakdown']['stats']['total_minutes'];
                }
            });

            
            $this->data = [
                'total_restday' => $total_restday,
                'total_restday_pay' => $total_restday_pay,

                // 'total_night_differential' => $nighDiffMinutes,
                // 'total_restdaytotal_night_differential_pay' => $nightDiffPay,
            ];

            // dd($this->data);
        }
        
    }

    public function getName()
    {
        return $this->benefit->name . " ({$this->employee->hourly_rate}/hr)";
    }

    public function enabled()
    {
        return $this->employeeTemplate->benefit_restday_pay;
    }
}