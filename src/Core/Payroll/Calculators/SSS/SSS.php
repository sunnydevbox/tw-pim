<?php
namespace Sunnydevbox\TWPim\Core\Payroll\Calculators\SSS;

use Sunnydevbox\TWPim\Core\Rate;

class SSS
    extends \Sunnydevbox\TWPim\Core\Payroll\BenefitCalculatorAbstract
    implements \Sunnydevbox\TWPim\Core\Payroll\Calculators\PayrollCalculatorInterface
{
    public function calculator()
    {
        // dd($this->employee->timelogs);
        // GET HOURS and RENDERED MINUTES


        // $tardy = $this->employee->timelogs->reduce(function($carry, $item) {
        //     $this->tardyMinutes = $carry + ($item->tardy_minutes);
        //     return $this->tardyMinutes;
        // }, 0);

        // Determine if SSS is included in this payroll.

        $sssRepository = app('\Sunnydevbox\TWPim\Repositories\Sss\SssDeductionRepository');
        $rate = new Rate;
        $rate = $rate->setEmployee($this->employee);

        if ($this->employee->employment_type->isRegular()) {

            $sss = $sssRepository->getRange($rate->getMonthly());
             //dd($sss);
            $this->totalNet = $sssRepository->getRange($rate->getMonthly())->ee;
        }


        $this->data = [
            'employer_share' => $sss->total_contribution_er,
        ];

        
        // $this->totalNet = 0;//($tardy / 60) * $this->employee->hourly_rate;
    }

    public function getName()
    {
        return $this->benefit->name ;
    }

    public function enabled()
    {
        return $this->employeeTemplate->deduction_sss;
    }
}