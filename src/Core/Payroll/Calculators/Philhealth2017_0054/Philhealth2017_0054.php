<?php
namespace Sunnydevbox\TWPim\Core\Payroll\Calculators\Philhealth2017_0054;

use Sunnydevbox\TWPim\Core\Rate;

class Philhealth2017_0054
    extends \Sunnydevbox\TWPim\Core\Payroll\BenefitCalculatorAbstract
    implements \Sunnydevbox\TWPim\Core\Payroll\Calculators\PayrollCalculatorInterface
{
    public function calculator()
    {
        $monthlyRate = $this->rate->getMonthly();

        $philhealthRepository = app('\Sunnydevbox\TWPim\Repositories\Philhealth\PhilhealthRepository');

        $this->totalNet = $philhealthRepository->getRange($this->rate->getMonthly())->employee_share;


        /*** DEPRECATED  */
        // dd($this->rate->getMonthly(), $philhealthRepository->getRange($this->rate->getMonthly()));

        // if ($monthlyRate <= 10000) {
        //     $mothlyPremium = 275;
        // } else if ($monthlyRate >= 40000) {
        //     $mothlyPremium = 1100;
        // } else {
        //     $mothlyPremium = $monthlyRate * 0.0275; // 2.75%
        // }

        // $employeeShare = $mothlyPremium / 2;
        // $employerShare = $mothlyPremium / 2;

        // $this->totalNet = $employeeShare;
    }

    public function getName()
    {
        return 'Philhealth 2017-0054';
        return $this->benefit->name ;
    }

    public function enabled()
    {
        return $this->employeeTemplate->deduction_philhealth;
    }
}