<?php
namespace Sunnydevbox\TWPim\Core\Payroll\Calculators\Pagibig;

use Sunnydevbox\TWPim\Core\Rate;

class Pagibig
    extends \Sunnydevbox\TWPim\Core\Payroll\BenefitCalculatorAbstract
    implements \Sunnydevbox\TWPim\Core\Payroll\Calculators\PayrollCalculatorInterface
{
    public function calculator()
    {
        $ceiling = 5000;
        $monthlyRate = $this->rate->getMonthly();

        if ($monthlyRate >= $ceiling) {
            $monthlyRate = $ceiling;
        }


        if ($monthlyRate <= 1500) {
            $multiplier = 0.01;
            $employerShareMultiplier = 0.02;
        } else {
            $multiplier = 0.02;
            $employerShareMultiplier = 0.02;
        }

        
        $this->totalNet = $monthlyRate * $multiplier;
        $employerShare = $monthlyRate * $employerShareMultiplier;

        // view docu
        $employerShare = 100;

        $this->data = [
            'employer_share' => $employerShare,
        ];
    }

    public function getName()
    {
        return $this->benefit->name ;
    }

    public function enabled()
    {
        return $this->employeeTemplate->deduction_pag_ibig;
    }
}