<?php
namespace Sunnydevbox\TWPim\Core\Payroll\Calculators\CashAdvance;

use \Carbon\Carbon;
use Sunnydevbox\TWPim\Core\Rate;
use Sunnydevbox\TWPim\Core\DateCalculator;
use Sunnydevbox\TWPim\Repositories\Deduction\CashAdvanceRepository;
use Sunnydevbox\TWPim\Repositories\SystemVariable\SystemVariableRepository;

class CashAdvance
    extends \Sunnydevbox\TWPim\Core\Payroll\BenefitCalculatorAbstract
    implements \Sunnydevbox\TWPim\Core\Payroll\Calculators\PayrollCalculatorInterface
{
    protected $hourlyRate = null;   
    protected $renderedMinutes = null;
    protected $totalOvertimeMinutes = 0;
    protected $type = 'CADV';

    public function calculator()
    {
        $cashAdvanceRepository = app('\Sunnydevbox\TWPim\Repositories\Deduction\CashAdvanceRepository');
        $eid = $this->employee->id;

        $cashAdvance = $cashAdvanceRepository->getEmployeeCashAdvances($eid)->first();
        if ($cashAdvanceRepository->isDebitable($cashAdvance)) {

            $cashAdvanceService = app('\Sunnydevbox\TWPim\Services\CashAdvanceService');

            $cashAdvanceLog = $cashAdvanceService->makeDebit([
                'id'        => $cashAdvance->id,
            ]);
            
            $this->totalNet = $cashAdvance->payable_per_period;
            $this->data = [
                'cash_advance_id' => $cashAdvance->id,
                'cash_advance_log_id' => $cashAdvanceLog->id,
                'paid_amount' => $cashAdvance->payable_per_period
            ];
        }
    }



    public function getName()
    {
        return $this->benefit->name;
    }

    public function settings()
    {

    }

    public function getType()
    {
        return 'ADCADV';
    }

    public function enabled()
    {
        $cashAdvanceRepository = app('\Sunnydevbox\TWPim\Repositories\Deduction\CashAdvanceRepository');

        $cashAdvance = $cashAdvanceRepository->getEmployeeCashAdvances($this->employee->id)->first();
        $debitable = false;
        
        if ($cashAdvance) {

            $debitable = $cashAdvanceRepository->isDebitable($cashAdvance);
        }

        return $this->employeeTemplate->deduction_cash_advance && $debitable;
    }
}