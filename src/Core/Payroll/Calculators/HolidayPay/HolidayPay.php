<?php
namespace Sunnydevbox\TWPim\Core\Payroll\Calculators\HolidayPay;

class HolidayPay 
    extends \Sunnydevbox\TWPim\Core\Payroll\BenefitCalculatorAbstract
    implements \Sunnydevbox\TWPim\Core\Payroll\Calculators\PayrollCalculatorInterface
{
    protected $hourlyRate = null;   
    protected $renderedMinutes = null; 

    public function calculator()
    {
   
        if ($this->enabled()) {

            // DETERMINE IF THIS timelog has rendered hours
            // CHeck if there are any overrides in the calendar or applied vacation or OT

            // IF yes, do the necessary calculation
            if (true) {
                
            }



            // // GET HOURS and RENDERED MINUTES
            // //dd($this->employee->timelogs);
            // $billableMinutes = $this->employee->timelogs->reduce(function($carry, $item) {
            //     return $carry + ($item->billable_minutes / 60);
            // }, 0);


            // $renderedMinutes = $this->employee->timelogs->reduce(function($carry, $item) {
            //     return $carry + ($item->billable_minutes);
            // }, 0);
            
            // // dd($this->employee->hourly_rate);
            // $this->totalNet = $this->employee->monthly_rate; // $this->employee->hourly_rate * $billableMinutes;
            // $renderedMinutes = 1961;
            
            // $totalDays = convertMinutesToDays($renderedMinutes);
            // $totalHours = convertMinutesToHours($renderedMinutes);
            // $hourlyRate = $this->employee->hourly_rate;
            // $dailyRate = $hourlyRate * 8;
            
            // $this->data = [
            //     'billableMinutes' => $renderedMinutes,
            //     'total_days'    => $totalDays,
            //     'total_hours'   => $totalHours,
            //     'hourly_rate'   => $hourlyRate,
            //     'daily_rate'    => $dailyRate,
            //     'basic_pay'     => $dailyRate * $totalDays,
            // ];
        //     'hourly_rate',
        // 'daily_rate',
        // 'total_hours',
        // 'total_days',

            // dd($this->data);
        }
        
    }

    public function getName()
    {
        return $this->benefit->name . " ({$this->employee->hourly_rate}/hr)";
    }

    public function enabled()
    {
        return $this->employeeTemplate->benefit_holiday_pay;
    }
}