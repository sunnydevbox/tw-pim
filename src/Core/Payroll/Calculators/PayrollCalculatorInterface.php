<?php
namespace Sunnydevbox\TWPim\Core\Payroll\Calculators;


interface PayrollCalculatorInterface
{
    const configuration = [];
    public function calculator();
    public function getName();
    public function enabled();
}