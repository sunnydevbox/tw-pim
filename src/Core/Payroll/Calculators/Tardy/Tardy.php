<?php
namespace Sunnydevbox\TWPim\Core\Payroll\Calculators\Tardy;

class Tardy 
    extends \Sunnydevbox\TWPim\Core\Payroll\BenefitCalculatorAbstract
    implements \Sunnydevbox\TWPim\Core\Payroll\Calculators\PayrollCalculatorInterface
{
    protected $hourlyRate = null;   
    protected $renderedMinutes = null; 
    protected $tardyMinutes = 0;

    public function calculator()
    {
        if ($this->enabled()) {
            // dd($this->employee->timelogs);
            // GET HOURS and RENDERED MINUTES
            $tardy = $this->employee->timelogs->reduce(function($carry, $item) {
                $this->tardyMinutes = $carry + ($item->tardy_minutes);
                return $this->tardyMinutes;
            }, 0);
            
            $this->totalNet = ($tardy / 60) * $this->employee->hourly_rate;
        }
    }

    public function getName()
    {
        return $this->benefit->name . ' (-' . number_format($this->tardyMinutes / 60, 2) . ' hrs)';
    }

    public function enabled()
    {
        return $this->employeeTemplate->deduction_tardiness;
    }
}