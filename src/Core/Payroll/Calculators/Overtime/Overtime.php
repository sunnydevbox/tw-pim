<?php
namespace Sunnydevbox\TWPim\Core\Payroll\Calculators\Overtime;

use \Carbon\Carbon;
use Sunnydevbox\TWPim\Core\Rate;
use Sunnydevbox\TWPim\Core\DateCalculator;
use Sunnydevbox\TWPim\Repositories\Holiday\HolidayRepository;
use Sunnydevbox\TWPim\Repositories\SystemVariable\SystemVariableRepository;

class Overtime
    extends \Sunnydevbox\TWPim\Core\Payroll\BenefitCalculatorAbstract
    implements \Sunnydevbox\TWPim\Core\Payroll\Calculators\PayrollCalculatorInterface
{
    protected $hourlyRate = null;   
    protected $renderedMinutes = null;
    protected $totalOvertimeMinutes = 0;

    public function calculator()
    {
        $rate = new Rate;
        $rate = $rate->setEmployee($this->employee);
        
        $this->hourlyRate = $rate->getHourly(); 

        $total = collect($this->employee->timelogs)
        // 1) CHECK if approved by HR
        //   return 0 if not approved
        // ->where('overtime_minutes', '>', 0)
        ->where('ot_application_id', '!=', null)

        // 2) GET HOURS and RENDERED MINUTES
        ->reduce(function($carry, $timelog) use ($rate) {
            
            // CALCULATION HERE
            $this->totalOvertimeMinutes += $timelog->overtime_minutes;
            // var_dump(($rate->getHourly() . ' * '. $this->condition($timelog)) . '* '.($timelog->overtime_minutes / 60));
            // var_dump(($rate->getHourly() * $this->condition($timelog)) * ($timelog->overtime_minutes / 60));
            return $carry + $this->condition($timelog);
        }, 0);

        $type = 'standard';

        $this->totalNet = $total;
        $this->data = [
            'total_minutes' => $this->totalOvertimeMinutes,
            'total_hours'   => $this->totalOvertimeMinutes / 60,
            'hourly_rate'   => $this->hourlyRate,
            
            'total_overtime_pay' => ($this->totalOvertimeMinutes / 60) * $this->hourlyRate,
            'total_overtime' => $this->totalOvertimeMinutes,
        ];
        
    }


    public function condition($timelog)
    {
        // IMPORTANT:
        // Get the actual start and time of the rendered OT period

        // DEFAULT: Regular work day
        // Multiplier of hourly rate
        $multiplier = 1.25; // 125%
        $totalNightDifferentialMinutes = 0;
        $totalRegularMinutes = 0;
        $nightDifferentialMultiplier = 0;
        
        $nightDifferentialTotal = 0;
        $regularTotal = 0;
        
        $dateCalulator = new DateCalculator;
        $systemRepository = app(SystemVariableRepository::class);
        $holiday = app(HolidayRepository::class);

        // THIS MUST BE OVERRIDEN BY LEAVE APPLICATION
        $matchedHoliday = $holiday->with('type')->findWhere(['start_date' => $timelog->time_in->format('Y-m-d')]);

        // START BREAK DOWN
        $actualRenderedOT = $dateCalulator->getActualOTRenderd($timelog->time_in, $timelog->time_out, $timelog->leave_application->start, $timelog->leave_application->end);
        $breakdownHours = $dateCalulator->hoursBreakDown($actualRenderedOT['time_in'], $actualRenderedOT['time_out']);

        if (!$timelog->hasOvertime()) {
            return 0;
        }

        if (count($breakdownHours['regular'])) {
            $output=array();
            for ($i=0; $i<count($breakdownHours['regular']); $i++) {
                $k = $breakdownHours['regular'][$i];
                for ($j=$i+1; $j<count($breakdownHours['regular']); $j++) {
                    $v = $breakdownHours['regular'][$j];
                    $totalRegularMinutes += $k->diffInMinutes($v);
                }
            }
        }

        if ($matchedHoliday && count($matchedHoliday) >= 2) {
            $multiplier = $systemRepository->var('overtime_double_holiday');
            
        } else if ($matchedHoliday  && count($matchedHoliday) == 1 && $matchedHoliday[0]->isSpecialHoliday()) {
            
            $multiplier = $systemRepository->var('overtime_specialholiday_multiplier');

            if ($timelog->isRestDay()) {
                $multiplier = $systemRepository->var('overtime_specialholiday_restday_multiplier');
            }
        } else if ($matchedHoliday  && count($matchedHoliday) == 1 && $matchedHoliday[0]->isRegularHoliday()) {
            $multiplier = $systemRepository->var('overtime_regularholiday_multiplier');

            if ($timelog->isRestDay()) {
                $multiplier = $systemRepository->var('overtime_regularholiday_restday_multiplier');
            }
        } else if ($timelog->isRestDay()) {
            $multiplier = $systemRepository->var('overtime_restday_multiplier');
        } else {
            $multiplier = $systemRepository->var('overtime_regular_multiplier');
        }

        $regularTotal = $this->hourlyRate * $multiplier * ($totalRegularMinutes / 60);

        if (count($breakdownHours['night_differential'])) {
            $output=array();
            for ($i=0; $i<count($breakdownHours['night_differential']); $i++) {
                $k = $breakdownHours['night_differential'][$i];
                for ($j=$i+1; $j<count($breakdownHours['night_differential']); $j++) {
                    $v = $breakdownHours['night_differential'][$j];
                    $totalNightDifferentialMinutes += $k->diffInMinutes($v);
                }
            }
        }

        // TIME TO START THE CALCULATION
        if ($timelog->hasNightDifferential()) {
                    
            $nightShiftOvertimeHourlyRate = 0;

            if ($matchedHoliday && count($matchedHoliday) >= 2) {
                $nightDifferentialMultiplier = 3.3; 
                if ($timelog->isRestDay()) {
                    $nightDifferentialMultiplier = 3.9; 
                }
            } else if ($matchedHoliday  && count($matchedHoliday) == 1 && $matchedHoliday[0]->isSpecialHoliday()) {

                $nightDifferentialMultiplier = 1.3;
                
                if ($timelog->isRestDay()) {
                    $nightDifferentialMultiplier = 1.5;
                }

            } else if ($matchedHoliday  && count($matchedHoliday) == 1 && $matchedHoliday[0]->isRegularHoliday()) {
                
                $nightDifferentialMultiplier = 2;
    
                if ($timelog->isRestDay()) {
                    $nightDifferentialMultiplier = 2.6;
                }

            }  else if ($timelog->isRestDay()) {

                $nightDifferentialMultiplier = 1.3;

            } else {
                /*
                    Ordinary day
                */

                $nightDifferentialMultiplier = 1;
            }

            // Formula for OT night diff: 
            // Hourly rate x multiplier x 10% x hours worked
            $nightDifferentialTotal = $this->hourlyRate * $nightDifferentialMultiplier * 1.1 * ($totalNightDifferentialMinutes / 60);
        }

        return $regularTotal + $nightDifferentialTotal;
        /*
        Rules
        - check type of day - 
            - rest day, 
            - special holiday, - 200%
                - If unworked, employee shall be paid 100% of his/her daily rate – (Daily Rate x 100%)
            - special holiday and rest day - 260%
                - If unworked, employee shall be paid 100% of his/her daily rate – (Daily Rate x 100%)
            - Special Non-Working Day - 130% ; Excess of eight (8) hours – plus 30% of hourly rate on said day
            - Special Non-Working Day and Rest Day - 150% ; Excess of eight (8) hours – plus 30% of hourly rate on said day
            - regular day, 
        - check if has night differnetial
        - check if falls in overtime
            
        */


        /* 1) Is regular day?
            If you’re planning to put in overtime on a regular day, just multiply your hourly wage by the rate to get your overtime pay per hour:

            P100 x 125% = P125 per hour
        */

        /* 2) Is Rest Day?
            If you choose to work on a rest day, your hourly wage for the first eight hours will be:
            P100 x 130% = P130 per hour 

            But once you’re on your ninth hour, your hourly wage goes up to:
            P100 x 169% = P169 per hour
        */

        /* 3) Finally, if you overtime on a regular night shift:

            P100 x 137.5% = P137.5 per hour

            But if that night shift also happens to be a rest day:

            P100 x 185.9% = P185.9 per hour
            */

        
        // https://docs.google.com/document/d/14uvkhM0gq3FGM24JlTO5BCybH5HTaEGVwUH4KjcMbQM/edit#heading=h.s1zpme4ah8et
    }


    public function getName()
    {
        return $this->benefit->name . " ({$this->totalOvertimeMinutes}mins)";
    }


    public function settings()
    {

    }


    public function fixed($billableOvertimeMinutes)
    {
        
    }

    public function enabled()
    {
        return $this->employeeTemplate->benefit_overtime_pay;
    }
}