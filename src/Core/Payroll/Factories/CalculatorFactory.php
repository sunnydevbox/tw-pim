<?php
namespace Sunnydevbox\TWPim\Core\Payroll\Factories;
            
class CalculatorFactory
{
    protected $benefitClassName = null;
    protected $minutes = null;
    protected $benefit = null;

    public function calculate($type, $minutes)
    {
        // $type = ucwords(strtolower($type));
        $c = '\Sunnydevbox\TWPim\Core\Payroll\Calculators\\'. $this->benefitClassName .'\Calculators\\' . $type;

        if (class_exists($c)) {
            $c = new $c;
            $c->setObject($this);
            return $c->calculate($minutes);
        }
        _dd($c, class_exists($c),  $this->benefitClassName, $type, $minutes);
        return $this;
    }

    public function setBenefitClassName($name)
    {
        $this->benefitClassName = $name;
    }

    public function setMinutes($minutes)
    {
        $this->minutes = $minutes;
    }

    public function getMinutes()
    {
        return $this->minutes;
    }

    public function getBenefitClassName()
    {
        return $this->benefitClassName;
    }

    public function benefit()
    {
        return $this->benefit;
    }

    public function __construct($benefitClass, $minutes = null)
    {
        $this->benefit = $benefitClass;
        $benefitClassName = (new \ReflectionClass($benefitClass))->getShortName();
        $this->setBenefitClassName($benefitClassName);
        $this->setMinutes($minutes);
    }
}