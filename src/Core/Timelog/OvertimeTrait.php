<?php
namespace Sunnydevbox\TWPim\Core\Timelog;

use Carbon\Carbon;
use Sunnydevbox\TWPim\Core\DateCalculator;

trait OvertimeTrait
{
    public function calculateBillableOvertime($otApplication)
    {
        if (!$this->isValidTime()) {
            return null;
        }

        $startPeriod = $this->getStartPeriod($otApplication->start);
        $endPeriod = $this->getEndPeriod($otApplication->end);        
        
        if ($this->timelogObject->time_out->lte($endPeriod)) {
            $endPeriod = $this->timelogObject->time_out;
        }

        if ($this->timelogObject->time_in->gte($startPeriod)) {
            $startPeriod = $this->timelogObject->time_in;
        }

        return $startPeriod->diffInMinutes($endPeriod);
    }

    public function employeeHasFiledOT($timeToCheck = null)
    {
        if (!$timeToCheck) {
            $timeToCheck = $this->timelogObject->time_in;
        }

        $note = 'OT application: ';
        $otApplication = $this->timelogObject->employee
                                ->leaveApplications()
                                ->range(
                                    Carbon::parse($timeToCheck)->startOfDay(),
                                    Carbon::parse($timeToCheck)->endOfDay()
                                )
                                ->approved()
                                ->with(['leave_type' => function($query) {
                                    $query->typeIsOvertime();
                                }])
                                ->limit(1)
                                ->first();

        
        $this->timelogObject->ot_application_id = null;
                                
        if ($otApplication) {
            $applicationStart = $otApplication->start;//->subHours(config('tw-pim.allowed_advanced_hours'));
            $applicationEnd = $otApplication->end;

            $dateCalculator = new DateCalculator();
            $result = $dateCalculator->isOverlapped($this->timelogObject->time_in, $this->timelogObject->time_out, $applicationStart, $applicationEnd);
            
            // CHECK FOR A MATCH in the filed OT's
            // DEPRECATE: if ($timeToCheck->between($applicationStart, $applicationEnd)) {
            if ($result) {
                $remainingOTMinutes = $otApplication->checkRemaining();
                $renderedDuration = $this->calculateBillableOvertime($otApplication);

                if ($remainingOTMinutes > 0) {
                    if ($remainingOTMinutes < $renderedDuration) {
                        $renderedDuration = $renderedDuration - $remainingOTMinutes;
                    }
                }
                
                $this->timelogObject->overtime_minutes = $renderedDuration;
                $this->timelogObject->ot_application_id = $otApplication->id;
                
                $note = $note . '"' . $otApplication->purpose . '" (' . $this->timelogObject->overtime_minutes . ' minutes)';
                $this->addNote($note);

                $otApplication->use($renderedDuration);

                return true;
            }
        }

        $this->addNote($note . 'None');
        return false;
    }

    public function startOvertimeCalculation()
    {
        $this->employeeHasFiledOT();
    }

}
