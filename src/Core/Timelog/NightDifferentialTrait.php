<?php
namespace Sunnydevbox\TWPim\Core\Timelog;

use Carbon\Carbon;
use Sunnydevbox\TWPim\Repositories\Holiday\HolidayRepository;
use Sunnydevbox\TWPim\Core\DateCalculator;

trait NightDifferentialTrait
{
    public function startNightDifferentialCalculation()
    {
        $dateCalculator = new DateCalculator;

        $this->timelogObject->has_night_differential = false;

        if ($dateCalculator->isNightDifferential($this->timelogObject->time_in, $this->timelogObject->time_out)) {
            $this->timelogObject->has_night_differential = true;
        }
    }
}
