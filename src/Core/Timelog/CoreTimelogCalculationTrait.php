<?php
namespace Sunnydevbox\TWPim\Core\Timelog;

use \Carbon\Carbon;
use Sunnydevbox\TWPim\Core\Timelog\OvertimeTrait;
use Sunnydevbox\TWPim\Core\Timelog\RestDayTrait;
use Sunnydevbox\TWPim\Core\Timelog\HolidayTrait;
use Sunnydevbox\TWPim\Core\Timelog\ElapsedTimeTrait;
use Sunnydevbox\TWPim\Core\Timelog\NightDifferentialTrait;
use Sunnydevbox\TWPim\Core\DateCalculator;

trait CoreTimelogCalculationTrait 
{
    use ElapsedTimeTrait;
    use OvertimeTrait;
    use RestDayTrait;
    use HolidayTrait;
    use NightDifferentialTrait;

    private $timelogObject = null;
    private $matchedPeriod = null;
    public $matchedRestDay = null;
    public $matchedRestDayShiftPeriod = null;
    public $employee = null;

    /**
     * calculate
     *
     * Triggers all calculations for the timelog
     * 
     * @param [type] $timelogObject
     * @return void
     */
    public function calculate($timelogObject = null)
    {
        $this->timelogObject = $timelogObject;
        // $this->timelogObject = $timelogObject->with(['employee.leaveApplications', 'employee.shiftTemplate.periods'])->first();

        $this->employee = $this->timelogObject->employee;//()->with(['shiftTemplate.periods', 'leaveApplications'])->first();

        if ($this->employee->shiftTemplates->count()) {
            $this->matchedPeriod = $this->findMatchingPeriod(); 
            $this->timelogObject->shift_template_period_id = null;
            $this->timelogObject->time_elapsed = null;
            $this->timelogObject->tardy_minutes = null;
            $this->timelogObject->billable_minutes = null;
            $this->timelogObject->overtime_minutes = null;
            $this->timelogObject->status = $this->timelogObject::STATUS_INVALID;
            $this->addNote('*** Calcuation started: ' . Carbon::now()->format('Y-m-d H:i:s'));
            
            if ($this->matchedPeriod) {
                $this->addNote($this->matchedPeriod['name']);
                $this->timelogObject->shift_template_period_id = $this->matchedPeriod['id'];
                
                $this->timelogObject->status = ($this->isValidTime()) ?  $this->timelogObject::STATUS_PENDING : $this->timelogObject::STATUS_INVALID;

                // TARDINESS
                $this->timelogObject->tardy_minutes = $this->calculateTardyTime();

                // BILLABLE TIME
                $this->timelogObject->billable_minutes = $this->calculateBillableTime();  

            } else {
                $this->addNote('No matching shift/period found');
                $this->timelogObject->status = $this->timelogObject::STATUS_INVALID;
            }


            // OT
            $this->startOvertimeCalculation();

            // CHECK IF REST DAY
            $this->startRestDayCalculation();

            // CHECK FOR TYPE OF HOLIDAY
            $this->startHolidayCalculation();

            // CHECK NIGHT DIFF
            $this->startNightDifferentialCalculation();

            // FINALLY, should we set status to auto approve?
            // DEBUG ONLY
            // $this->timelogObject->status = 'APPROVED';



            $this->timelogObject->time_elapsed = $this->calculateElepsedTime();
            
            $this->addNote('*** Calcuation ended: ' . Carbon::now()->format('Y-m-d H:i:s') . "\n");
            $this->timelogObject->save();

            return $timelogObject;
        } else {
            $this->addNote('*** No assigned Shift Template: ' . Carbon::now()->format('Y-m-d H:i:s'));
            $this->timelogObject->status = $this->timelogObject::STATUS_INVALID;
            $this->timelogObject->save();
        }
    }

    /**
     * findMatchingPeriod
     *  
     * Determines a matching period from shift template periods
     * assigned by admin/HR 
     * 
     * @param [type] $periods
     * @return void
     */
    public function findMatchingPeriod($templates = null)
    {
        if(!$templates) {
            $templates = $this->timelogObject->employee->shift_templates()->with('periods')->get();
        }

        if (!$this->isValidTime()) {
            return null;
        }

        foreach($templates as $template) {
        
            foreach($template->periods as $period) {

                $startPeriod = $this->getStartPeriod($period);
                $endPeriod = $this->getEndPeriod($period);
                
                // Give allowance for early login
                // THIS value MUST BE DYNAMIC
                $startPeriod = $startPeriod->copy()->subHours(config('tw-pim.allowed_advanced_hours'));
                
                $dateCalculator = new DateCalculator();
                $result = $dateCalculator->isOverlapped($startPeriod, $endPeriod, $this->timelogObject->time_in, $this->timelogObject->time_out);

                if ($result) {

                    // dd($this->timelogObject);
                    // dd($period->workDays, $period->restDays, $this->timelogObject->time_in->dayOfWeekIso);
                    // dd($period->workDays, $period->restDays);//->contains($this->timelogObject->time_in->dayOfWeek));
                    if ($period->workDays->contains($this->timelogObject->time_in->dayOfWeekIso)) {
                        return $period;
                    } else if ($period->restDays->contains($this->timelogObject->time_in->dayOfWeekIso)) {
                        $this->matchedRestDay = $this->timelogObject->time_in->dayOfWeekIso;
                        $this->matchedRestDayShiftPeriod = $period;
                    } else {
                        return null;
                    }
                }


                // if (
                //     ($startPeriod->between($this->timelogObject->time_in, $this->timelogObject->time_out) 
                //     || 
                //     $endPeriod->between($this->timelogObject->time_in, $this->timelogObject->time_out) 
                //     ||
                //     $this->timelogObject->time_in->between($startPeriod, $endPeriod))
                // ) {

                //     // if (in_array($this->timelogObject->time_in->dayOfWeek, $daysPeriod)) 
                //     if ($period->workDays->contains($this->timelogObject->time_in->dayOfWeek)) {
                //         return $period;
                //     } else if ($period->restDays->contains($this->timelogObject->time_in->dayOfWeek)) {
                //         $this->matchedRestDay = $this->timelogObject->time_in->dayOfWeek;
                //         $this->matchedRestDayShiftPeriod = $period;
                //     } else {
                //         return null;
                //     }
                // }

            }

        }

        return ($this->matchedRestDayShiftPeriod) ? $this->matchedRestDayShiftPeriod : null;
    }

    protected function getMatchedRestDay()
    {
        return $this->matchedRestDay;
        return [
            $this->matchedRestDay,
            $this->matchedRestDayShiftPeriod,
        ];
    }

    protected function getMatchedRestDayShiftPeriod()
    {
        return $this->matchedRestDayShiftPeriod;
        return [
            $this->matchedRestDay,
            $this->matchedRestDayShiftPeriod,
        ];
    }

    /**
     * getStartPeriod
     * 
     * Returns Carbon Date formate datetime value
     *
     * @return void
     */
    private function getStartPeriod($period = null)
    {
        if (!$period) {
            $period = $this->matchedPeriod;
        }

        if ($this->timelogObject->time_in) {
            if (is_array($period)) {
                $period = $period['start'];
            } else if (is_object($period)) {
                if (!$period instanceof Carbon) {
                    $period = $period->start;
                }
            }
            
            $startPeriod = $this->getHourMinute($period);
            return $this->timelogObject->time_in->copy()->hour($startPeriod[0])->minute($startPeriod[1]);
        }
        
        return null;
    }

    /**
     * getEndPeriod
     * 
     * Returns Carbon Date formate datetime value
     *
     * @param [Carbon/String/null] $period
     * @return void
     */
    private function getEndPeriod($period = null)
    {
        if (!$period) {
            $period = $this->matchedPeriod;
        }

        if ($this->timelogObject->time_out && $period) {
            if (is_array($period)) {
                $period = $period['end'];
            } else if (is_object($period)) {
                if (!$period instanceof Carbon) {
                    $period = $period->end;
                }
            }

            $endPeriod = $this->getHourMinute($period);
            return $this->timelogObject->time_out->copy()->hour($endPeriod[0])->minute($endPeriod[1]);
        }

        return null;
    }

    /**
     * getHourMinute
     * 
     * Returns Carbon Date formate datetime value
     *
     * @param [Carbon/String] $time
     * @return array
     */
    private function getHourMinute($time)
    {
        if (is_string($time)) {
            // $parsedTime =  explode(':', $time);
            $time = new Carbon($time);
            
            $parsedTime = [
                $time->hour,
                $time->minute,
            ];
        } else if ($time instanceof Carbon) {
            $parsedTime = [
                $time->hour,
                $time->minute,
            ];
        }

        return $parsedTime;
    }

    /**
     * addNote
     *
     * Easily concatenate strings to notes
     * 
     * @param [string] $note
     * @return void
     */
    private function addNote($note)
    {
        $this->timelogObject->notes .= ("> " . $note . "\n");
    }

    /**
     * isComplete (DEPRECATED)
     * 
     * Alias to isValidTime
     * Determines if the timelog is valid for caclulcations.
     * Checks time_in and time_out
     *
     * @param [type] $timelogObject
     * @return boolean
     */
    private function isComplete()
    {
        return $this->isValidTime();//(!$this->timelogObject->time_in || !$this->timelogObject->time_out) ? false : true;
    }

    /**
     * isValidTime
     * 
     * Determines if the timelog is valid for caclulcations.
     * Checks time_in and time_out
     *
     * @param [type] $timelogObject
     * @return boolean
     */
    public function isValidTime()
    {
        return ($this->timelogObject->time_in && $this->timelogObject->time_out
            && $this->timelogObject->time_in->lt($this->timelogObject->time_out)
            ) ? true : false;
    }

}