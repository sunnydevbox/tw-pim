<?php
namespace Sunnydevbox\TWPim\Core\Timelog;

use Carbon\Carbon;
use Sunnydevbox\TWPim\Repositories\Holiday\HolidayRepository;

trait HolidayTrait
{
    public function startHolidayCalculation()
    {
        $holidayRepository = app('\Sunnydevbox\TWPim\Repositories\Holiday\HolidayRepository');
        
        if ($this->timelogObject->shift_template_period) {
            $dateToCheck = $this->timelogObject->shift_template_period->start_object
                ->month($this->timelogObject->time_in->month)
                ->day($this->timelogObject->time_in->day)
                ->year($this->timelogObject->time_in->year);
        } else {
            $dateToCheck = $this->timelogObject->time_in;
        }
        
        // GET MATCHING HOLIDAY
        $mathedHoliday = $holidayRepository->findWhere([
            'start_date' => $dateToCheck->format('Y-m-d') 
        ])->first();

        $this->timelogObject->holiday_id = null;
        
        if ($mathedHoliday) {
            $this->timelogObject->holiday_id = $mathedHoliday->id;
        }
    }
}
