<?php
namespace Sunnydevbox\TWPim\Core\Timelog;

use \Carbon\Carbon;

trait ElapsedTimeTrait
{
    public function calculateElepsedTime()
    {
        if (!$this->isValidTime()) {
            return null;
        }

        if ($this->timelogObject->time_out) {
            return $this->timelogObject->time_in->diffInMinutes($this->timelogObject->time_out);
        }

        // DEBUG: 
        // WHAT's THIS
        return 0;
    }

    public function calculateTardyTime()
    {
        if (!$this->isValidTime()) {
            return null;
        }

        $startPeriod = $this->getStartPeriod();

        $isLate = $this->isLate($this->timelogObject->time_in, $startPeriod);
        
        return $isLate? $isLate : 0;
    }

    public function calculateBillableTime()
    {
        if (!$this->isValidTime()) {
            return null;
        }

        $startPeriod = $this->getStartPeriod($this->matchedPeriod);
        $endPeriod = $this->getEndPeriod($this->matchedPeriod);
        
        if ($this->timelogObject->time_out->lt($endPeriod)) {
            $endPeriod = $this->timelogObject->time_out;
        }
        
        $isLate = $this->calculateTardyTime($this->matchedPeriod);
        
        // in_array($startPeriod->dayOfWeek, $daysPeriod);

        return $startPeriod->diffInMinutes($endPeriod) - $isLate;
    }

    public function isLate(Carbon $timeIn, Carbon $startPeriod)
    {
        if ($this->matchedPeriod) {
            $gracePeriod = $startPeriod->copy()->addMinutes($this->matchedPeriod['grace_period']);
        } else {
            $gracePeriod = $startPeriod->copy();
        }

        if ($timeIn->gte($gracePeriod)) {
            return $gracePeriod->diffInMinutes($timeIn);
        }

        return false;
    }

}
