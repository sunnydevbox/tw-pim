<?php
namespace Sunnydevbox\TWPim\Core\Timelog;

use Carbon\Carbon;
use Sunnydevbox\TWPim\Services\LeaveApplicationService;
use Sunnydevbox\TWPim\Core\DateCalculator;

trait RestDayTrait
{
    public function startRestDayCalculation()
    {
        $this->timelogObject->is_rest_day = false;
        $this->timelogObject->restday_minutes = 0;
        // dd($this->getMatchedRestDay() ,
        // $this->getMatchedRestDayShiftPeriod());
        // GET MATCHED REST DAY FROM Application
        $leaveApplicationService = app(LeaveApplicationService::class);
        $application = $leaveApplicationService->getApplicationsByUser(
            $this->employee->id, 
            $this->timelogObject, 
            'rest_day'
        );
    
        $dateCalulator = new DateCalculator;
    
        // dd($application, $this->timelogObject->time_in);
        // Means employee worked on a rest day
        // NOTE:: assumming only 1 application per days
        if ($application->count()) {
            $application = $application[0];
            $this->timelogObject->is_rest_day = true;
            $this->timelogObject->ot_application_id = $application->id;

            $pair = $this->getEffectiveTime($application);
            $breakdownHours = $dateCalulator->hoursBreakDown($pair[0], $pair[1]);
        }
        /**
         * or is this a rest day based on shift template?
         */
        else if ($this->getMatchedRestDay() &&
            $this->getMatchedRestDayShiftPeriod()
        ) {
            // dd($this->getMatchedRestDay(), $this->getMatchedRestDayShiftPeriod());
            $a = (object) [
                'start' => $this->timelogObject->time_in->startOfDay(),
                'end'   => $this->timelogObject->time_in->endOfDay(),
            ];

            $breakdownHours = $dateCalulator->hoursBreakDown($this->timelogObject->time_in, $this->timelogObject->time_out);

            $this->timelogObject->shift_template_period_id = $this->matchedRestDayShiftPeriod->id;
        }

        if (isset($breakdownHours)) {
            $this->timelogObject->restday_minutes = $breakdownHours['total_regular']; // += $this->countRestDayMinutes($a);
            $this->timelogObject->is_rest_day = true;
            
            if ($breakdownHours['stats']['total_night_differential']) {
                $this->timelogObject->has_night_differential = true;
                $this->timelogObject->night_differential_minutes = $breakdownHours['stats']['total_night_differential'];
            }
        }
    }

    public function getEffectiveTime($application)
    {
        $timeStart = $this->timelogObject->time_in;
                
        if ($this->timelogObject->time_in->lt($application->start)) {
            $timeStart = $this->timelogObject->time_in;
        }

        $timeEnd = $this->timelogObject->time_out;
        if ($this->timelogObject->time_out->gt($application->end)) {
            $timeEnd = $this->timelogObject->time_out;
        }

        return [$timeStart, $timeEnd];
    }
}
