<?php
namespace Sunnydevbox\TWPim\Core;

use Carbon\Carbon;

class WorkDay
{
    protected $employee = null;
    protected $shiftTemplate = null;

    public function countByMonth($month = null, $year = null)
    {
        $totalDays = 0;
        
        if (!$year) {
            $year = Carbon::now()->year;
        }

        if (!$month) {
            $month = Carbon::now()->month;
        }

        $thisYear = Carbon::now()->year($year);

        $thisMonth = $thisYear->month($month);
        $startOfMonth = $thisMonth->copy()->startOfMonth();
        $endOfMonth = $thisMonth->copy()->endOfMonth();
        
        $workDays = $this->shiftTemplate->periods[0]->workDays;
        // var_dump($workDays);
        foreach(range($startOfMonth->day, $endOfMonth->day) as $day) {
            if ($workDays->contains($thisMonth->day($day)->dayOfWeekIso)) {
                // var_dump($thisMonth->day);
                $totalDays++;
            }
        } 

        return $totalDays;
    }

    public function countByYear($year = null)
    {
        // if ($this->getEmployee()->template->total_da    )

        $totalDays = 0;

        if (!$year) {
            $year = Carbon::now()->year;
        }
        
        $thisYear = Carbon::now()->year($year);
        
        $workDays = $this->shiftTemplate->periods[0]->workDays;
        foreach(range(1,12) as $month) {
            $thisMonth = $thisYear->month($month);
            $startOfMonth = $thisMonth->copy()->startOfMonth();
            $endOfMonth = $thisMonth->copy()->endOfMonth();
            
            foreach(range($startOfMonth->day, $endOfMonth->day) as $day) {
                if ($workDays->contains($thisMonth->day($day)->dayOfWeek)) {
                    $totalDays++;
                }
            } 
        }

        return $totalDays;
    }


    public function setShiftTemplate($shiftTemplateId = null)
    {
        $this->shiftTemplate = $shiftTemplateId;

        if (!is_object($shiftTemplateId)) { 
            $this->shiftTemplate = $this->shiftTemplateRepository->find($shiftTemplateId);    
        }

        return $this;
    }

    private function countWeeklyWorkDays()
    {
        // dd($this->shiftTemplate->periods());
    }

    public function setEmployee($employee)
    {
        $this->employee = $employee;

        return $this;
    }

    public function getEmployee()
    {
        return $this->employee;
    }

    public function __construct() {
        $this->shiftTemplateRepository = app('\Sunnydevbox\TWPim\Repositories\ShiftTemplate\ShiftTemplateRepository');
        // $this->shiftTemplateRepository = app('\Sunnydevbox\TWPim\Repositories\Employee\EmployeeTemplateRepository');
    }
}