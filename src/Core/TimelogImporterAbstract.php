<?php
namespace Sunnydevbox\TWPim\Core;

use Sunnydevbox\TWPim\Contracts\TimelogImporterInterface;
use Sunnydevbox\TWPim\Events\TimelogImportEvent;
use Sunnydevbox\TWPim\Models\Timelog;

abstract class TimelogImporterAbstract implements TimelogImporterInterface
{
    private $file = null;
    public $timelogImport = null;
    public $timelogRepository;
    public $timelogImportRepository;
    public $employeeRepository;
    private $initLog = null;

    public function process()
    {
        // event(new TimelogImportEvent('create'));
        $this->timelogImport = $this->timelogImportRepository->create([
            'user_id' => \Auth::user()->id,
            'status' => 'STARTED',
        ]);


        $this->initLog = 'Import #' . $this->timelogImport->id . ' ';
        \Log::info($this->initLog . 'STARTED');

        // STEP 1: verify that the format is valid
        \Log::info($this->initLog . 'VERIFYING');
        $this->verifier();
        \Log::info($this->initLog . 'VERIFIED');

        // STEP 2:
        // Create reference to the file
        $this->timelogImport->attach($this->file(), ['key' => 'dtr-file-import']);
        \Log::info($this->initLog . 'FILE IMPORT ATTACHED');
        
        // STEP 3:
        // Begin the parsing process
        $this->timelogImport->status = 'PROCESSING';

        // dd($this->timelogImport);
        $this->timelogImport->save();

        $this->parse();



        // LAST STEP:
        $this->timelogImport->status = 'DONE';
        $this->timelogImport->save();        
    }


    /**
     * OVERRIDE THIS
     *
     * @return void
     */
    public function verifier()
    {
        // if (false) {
        //     $this->timelogImport->status = 'INVALID_FORMAT';
        //     $this->timelogImport->save();
        //     return false;
        // }

        if (true) {
            $this->timelogImport->status = 'VALID_FORMAT';
            $this->timelogImport->save();
            return true;
        }
    }

    /**
     * OVERRIDE THIS
     *
     * @return void
     */
    public function parse()
    {
        $file = $this->timelogImport->attachments()->first(); 
    
        if ($file) {
            if ($file->disk == 'local') {
                $csvFile = storage_path('app/' . $file->filepath);
            }
        }
    
        $handle = fopen($csvFile, 'r');
        $header = true;
        
        while ($csvLine = fgetcsv($handle, 1000, ",")) {
            if ($header) {
                $header = false;
            } else {
                // 1) Validate EMPLOYEE ID (column 1)
                // 2) Check for duplicate entries
                $parsed = $this->parseRow($csvLine);
            }
        } 
    }

    /**
     * OVERRIDE THIS
     *
     * @return void
     */
    public function parseRow($row) {}

    public function setFile($file)
    {
        $this->file = $file;

        return $this;
    }

    public function saveRecord($employeeId, $timeIn, $timeOut)
    {
        $r = $this->timelogRepository->findWhere([
            'employee_id'   => $employeeId,
            'time_in'       => $timeIn,
            'time_out'      => $timeOut,
        ]);

        // DEBUG
        // if ($r->count()) {
        //     $r->each(function($item, $key) {
        //         $item->forceDelete();
        //     });
        // }

        $entry = $this->timelogRepository->firstOrCreate(
            [
                'employee_id'   => $employeeId,
                'time_in'       => $timeIn,
                'time_out'      => $timeOut,
            ]
        );

        return $entry;
    }

    public function file()
    {
        return $this->file;
    }

    public function getEmployee($id)
    {
        $model = $this->employeeRepository->makeModel()
                        ->active()
                        ->where('id', $id)
                        ->first();
        return $model;
    }

    public function __construct(
        \Sunnydevbox\TWPim\Repositories\Timelog\TimelogRepository $timelogRepository,
        \Sunnydevbox\TWPim\Repositories\Timelog\TimelogImportRepository $timelogImportRepository,
        \Sunnydevbox\TWPim\Repositories\Employee\EmployeeRepository $employeeRepository
    ) {
        $this->timelogRepository = $timelogRepository;
        $this->timelogImportRepository = $timelogImportRepository;
        $this->employeeRepository = $employeeRepository;
    }
}