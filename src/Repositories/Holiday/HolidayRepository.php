<?php 
namespace Sunnydevbox\TWPim\Repositories\Holiday;

use Sunnydevbox\TWCore\Repositories\TWBaseRepository;

class HolidayRepository extends TWBaseRepository
{
    //all, paginate, find, findByField, findWhere, getByCriteria
    //protected $cacheOnly = ['all', 'paginate'];
    protected $cacheExcept = [];




    public function getMonthList($start, $end)
    {
        return $this->makeModel()
            ->range($start, $end)
            ->with(['holiday_type'])
            ->get();
    }




    /**
     * Specify Validator class name
     *
     * @return mixed
     */
    public function validator()
    {
        return "\Sunnydevbox\TWPim\Validators\HolidayValidator";
    }

    /**
     * Specify Model class name
     *
     * @return string
     */
    function model()
    {
        return '\Sunnydevbox\TWPim\Models\Holiday';
    }

    public function boot()
    {   
        parent::boot();
        
        $this->pushCriteria(app('\Sunnydevbox\TWPim\Criteria\HolidayCriteria'));
    }
}

