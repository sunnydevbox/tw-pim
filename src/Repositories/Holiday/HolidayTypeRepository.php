<?php 
namespace Sunnydevbox\TWPim\Repositories\Holiday;

use Sunnydevbox\TWCore\Repositories\TWBaseRepository;

class HolidayTypeRepository extends TWBaseRepository
{
    //all, paginate, find, findByField, findWhere, getByCriteria
    //protected $cacheOnly = ['all', 'paginate'];
    protected $cacheExcept = [];

    /**
     * Specify Validator class name
     *
     * @return mixed
     */
    public function validator()
    {
        return "\Sunnydevbox\TWPim\Validators\HolidayTypeValidator";
    }

    /**
     * Specify Model class name
     *
     * @return string
     */
    function model()
    {
        return '\Sunnydevbox\TWPim\Models\HolidayType';
    }
}

