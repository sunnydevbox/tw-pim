<?php 
namespace Sunnydevbox\TWPim\Repositories\Employee;

use Sunnydevbox\TWCore\Repositories\TWBaseRepository;
use Sunnydevbox\TWPim\Factories\EmployeeDataFactory;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\File;
use Dingo\Api\Http\Request;

class EmployeeRepository extends TWBaseRepository
{
    //all, paginate, find, findByField, findWhere, getByCriteria
    //protected $cacheOnly = ['all', 'paginate'];
    protected $cacheExcept = [];
    
    protected $fieldSearchable = [
        'id',
        'first_name',
        'last_name',
        'gender',
        'nationality',
        'religion',
        'email',
        'employee_id_number',
        'date_hired',
        'date_dismissed',
    ];

    public function store(array $attributes)
    {   
        $type = $attributes['type'];

        $EmployeeDataFactory = EmployeeDataFactory::store($type, $attributes);
        
        if ($type == 'profile-picture') {
            return $EmployeeDataFactory;
        } else {
            return $EmployeeDataFactory->create($attributes);
        }
    }

    public function deactivate($employee)
    {
        $employee->employment_status_id = 1;
        $employee->save();
    }

    public function activate($employee)
    {
        $employee->employment_status_id = 2;
        $employee->save();
    }

    /**
     * Specify Validator class name
     *
     * @return mixed
     */
    public function validator()
    {
        return "\Sunnydevbox\TWPim\Validators\EmployeeValidator";
    }

    /**
     * Specify Model class name
     *
     * @return string
     */
    function model()
    {
        return config('tw-pim.models.employee');
    }

    public function boot()
    {
        parent::boot();

        $this->pushCriteria(\Sunnydevbox\TWPim\Criteria\QueryEmployeeCriteria::class);
    }
}

