<?php 
namespace Sunnydevbox\TWPim\Repositories\Employee;

use Sunnydevbox\TWPim\Repositories\Employee\EmployeeRepository;
use Sunnydevbox\TWPim\Repositories\Employee\EmployeeDataInterface;

class EmployeeDataBiofieldRepository extends EmployeeRepository implements EmployeeDataInterface
{

    public function saveData($Employee, $attributes)
    {
        return $Employee->saveMeta(['bio_id'=>$attributes['bio_id']]);
    }

    /**
     * Specify Validator class name
     *
     * @return mixed
     */
    public function validator()
    {
        return "\Sunnydevbox\TWPim\Validators\EmployeeDataEmploymentValidator";
    }
}

