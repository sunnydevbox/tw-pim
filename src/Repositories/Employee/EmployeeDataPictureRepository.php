<?php 
namespace Sunnydevbox\TWPim\Repositories\Employee;

use Sunnydevbox\TWPim\Repositories\Employee\EmployeeRepository;

class EmployeeDataPictureRepository extends EmployeeRepository
{  
    public function attachProfilePicture($employee_id, $picture)
    {
        $Employee = $this->find($employee_id);
        $pic = $Employee->attachment(config('tw-pim.attachmentKeys.profile_picture'));
        if (!is_null($pic)) {
            $pic->delete();
        }

        $Picture = $Employee->attach($picture, [
            'key' => config('tw-pim.attachmentKeys.profile_picture'),
        ]);

        $Employee->touch();
        
        return $Picture;
    }

    /**
     * Specify Validator class name
     *
     * @return mixed
     */
    public function validator()
    {
        return "\Sunnydevbox\TWPim\Validators\EmployeeDataEmploymentValidator";
    }
}

