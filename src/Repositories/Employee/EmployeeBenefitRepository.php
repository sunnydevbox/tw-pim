<?php
namespace Sunnydevbox\TWPim\Repositories\Employee;

use Sunnydevbox\TWCore\Repositories\TWBaseRepository;

class EmployeeBenefitRepository extends TWBaseRepository
{
    //all, paginate, find, findByField, findWhere, getByCriteria
    //protected $cacheOnly = ['all', 'paginate'];
    protected $cacheExcept = [];

    /**
     * Specify Validator class name
     *
     * @return mixed
     */
    public function validator()
    {
        return '\Sunnydevbox\TWPim\Validators\EmployeeBenefitValidator';
    }

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return '\Sunnydevbox\TWPim\Models\EmployeeBenefit';
    }
}