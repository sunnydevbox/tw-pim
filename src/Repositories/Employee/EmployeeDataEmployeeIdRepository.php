<?php 
namespace Sunnydevbox\TWPim\Repositories\Employee;

use Sunnydevbox\TWPim\Repositories\Employee\EmployeeRepository;
use Sunnydevbox\TWPim\Events\EmployeeUpdatedIdEvent;
use Sunnydevbox\TWPim\Repositories\Employee\EmployeeDataInterface;

class EmployeeDataEmployeeIdRepository extends EmployeeRepository implements EmployeeDataInterface
{

    public function saveData($Employee, $attibutes)
    {
        return $this->update($attibutes, $Employee->id);
    }

    public function generateId($employee)
    {  
        $service = config('tw-pim.id_generator');
        $s = new $service;
        $s->object = $employee;
        $id = $s->generate($employee);

        if (!is_bool($id)) {
            $employee->employee_id_number = $id;
            $employee->save();

            return true;
        }

        return false;
    }

    public function hasDuplicate($employee_id = null, $id = null)
    {
        if (!$id) {
            throw new \Exception('invalid_employee_id');
        }
    
        $model = $this->model
                    ->select('id')
                    ->where('employee_id_number', $employee_id);

        if ($id) {
            $model = $model->where('id', '!=', $id);
        }
            
        $result = $model->first();
        
        return $result ? true : false;
    }

    public function update(array $attributes, $id)
    { 
        // VALIDATE THE NEW EMP ID
        // EXCLUDE empty values - this is to auto-generate
        if (isset($attributes['employee_id_number']) 
            && $attributes['employee_id_number'] != ''
        ) {
            // 1.) Duplicates
            if ($this->hasDuplicate($attributes['employee_id_number'], $id)) {
                throw new \Exception('duplicate_employee_id');
            }
        }

        // 2.) ###
        
        $employee = parent::update($attributes, $id);

        event(new EmployeeUpdatedIdEvent($employee));

        return $employee;
    }



    /**
     * Specify Validator class name
     *
     * @return mixed
     */
    public function validator()
    {
        return "\Sunnydevbox\TWPim\Validators\EmployeeDataEmployeeIdValidator";
    }

}

