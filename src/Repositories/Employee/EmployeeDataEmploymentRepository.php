<?php 
namespace Sunnydevbox\TWPim\Repositories\Employee;

use Sunnydevbox\TWPim\Repositories\Employee\EmployeeRepository;
use Sunnydevbox\TWPim\Repositories\Employee\EmployeeDataInterface;

class EmployeeDataEmploymentRepository extends EmployeeRepository implements EmployeeDataInterface
{

    public function saveData($Employee, $attributes)
    {
        return $this->update($attributes, $Employee->id);
    }

    /**
     * Specify Validator class name
     *
     * @return mixed
     */
    public function validator()
    {
        return "\Sunnydevbox\TWPim\Validators\EmployeeDataEmploymentValidator";
    }
}

