<?php 
namespace Sunnydevbox\TWPim\Repositories\Employee;

use Sunnydevbox\TWCore\Repositories\TWBaseRepository;
use Sunnydevbox\TWPim\Repositories\Employee\EmployeeRepository;

class EmployeeTemplateRepository extends EmployeeRepository
{
    //all, paginate, find, findByField, findWhere, getByCriteria
    //protected $cacheOnly = ['all', 'paginate'];
    protected $cacheExcept = [];

    protected $fieldSearchable = [
        'id',
        'name',
    ];

    /**
     * Specify Validator class name
     *
     * @return mixed
     */
    public function validator()
    {
        return "\Sunnydevbox\TWPim\Validators\EmployeeTemplateValidator";
    }

    public function model()
    {
        return config('tw-pim.model_employee_template');
    }

}

