<?php 
namespace Sunnydevbox\TWPim\Repositories\Employee;

use Sunnydevbox\TWCore\Repositories\TWBaseRepository;
use Sunnydevbox\TWPim\Repositories\Employee\EmployeeRepository;
use Sunnydevbox\TWPim\Repositories\Employee\EmployeeDataInterface;

class EmployeeDataDeductionRepository extends TWBaseRepository implements EmployeeDataInterface
{
    //all, paginate, find, findByField, findWhere, getByCriteria
    //protected $cacheOnly = ['all', 'paginate'];
    protected $cacheExcept = [];

    protected $fieldSearchable = [
        'employee_id',
        'deduction_id',
    ];

    public function saveData($Employee, $attributes = [])
    {
        // var_dump($attributes);
       // dd($Employee->deductions()->pivot);//->updateOrCreate($attributes));
    }

    /**
     * Specify Validator class name
     *
     * @return mixed
     */
    public function validator()
    {
        return "\Sunnydevbox\TWPim\Validators\EmployeeDeductionValidator";
    }

    public function model()
    {
        return config('tw-pim.models.employee-deduction');
    }

    public function boot()
    {
        parent::boot();
        
        // dd($this->getCriteria());
    }
}

