<?php 
namespace Sunnydevbox\TWPim\Repositories\Employee;

use Sunnydevbox\TWCore\Repositories\TWBaseRepository;
use Sunnydevbox\TWPim\Repositories\Employee\EmployeeRepository;

class EmployeeLogRepository extends EmployeeRepository
{
    //all, paginate, find, findByField, findWhere, getByCriteria
    //protected $cacheOnly = ['all', 'paginate'];
    protected $cacheExcept = [];

    protected $fieldSearchable = [
        'employee_id'
    ];

    public function log($data = [])
    {
        if (count($data)) {

            // GET CURRENT ACTOR
            if ($user = \Auth::user()) {
                
                $data['actorable_id'] = $user->id;
                $data['actorable_type'] = get_class($user);
            }

            $this->makeModel()->create($data);
        }

    }

    /**
     * Specify Validator class name
     *
     * @return mixed
     */
    public function validator()
    {
        return "\Sunnydevbox\TWPim\Validators\EmployeeLogValidator";
    }

    public function model()
    {
        return config('tw-pim.models.employee-log');
    }

}

