<?php
namespace Sunnydevbox\TWPim\Repositories\Employee;

interface EmployeeDataInterface
{
    public function saveData($employee, $attributes);
}