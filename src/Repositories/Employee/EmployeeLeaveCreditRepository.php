<?php 
namespace Sunnydevbox\TWPim\Repositories\Employee;

use Sunnydevbox\TWCore\Repositories\TWBaseRepository;
use Sunnydevbox\TWPim\Repositories\Employee\EmployeeRepository;

class EmployeeLeaveCreditRepository extends EmployeeRepository
{
    //all, paginate, find, findByField, findWhere, getByCriteria
    //protected $cacheOnly = ['all', 'paginate'];
    protected $cacheExcept = [];

    protected $fieldSearchable = [
        'employee_id',
        'leave_type_id',
    ];

    /**
     * Specify Validator class name
     *
     * @return mixed
     */
    public function validator()
    {
        return "\Sunnydevbox\TWPim\Validators\EmployeeLeaveCreditValidator";
    }

    public function model()
    {
        return config('tw-pim.models.employee-leave-credit');
    }

}

