<?php 
namespace Sunnydevbox\TWPim\Repositories\Deduction;

use Sunnydevbox\TWCore\Repositories\TWBaseRepository;

class CashAdvanceLogRepository extends TWBaseRepository
{
    //all, paginate, find, findByField, findWhere, getByCriteria
    protected $cacheOnly = ['all', 'paginate', 'find', 'findByField', 'findWhere', 'getByCriteria'];
    // protected $cacheExcept = [];


    protected $fieldSearchable = [
        'cash_advance_id',
    ];

    public function addByStatus($cashAdvanceId, $notes)
    {
        return $this->create([
            'type'              => $this->model::TYPE_STATUS,
            'cash_advance_id'   => $cashAdvanceId,
            'notes'             => $notes,
        ]);
    }

    public function addByDebit($cashAdvanceId, $amountDebited, $notes)
    {
        return $this->create([
            'type'              => $this->model::TYPE_DEBIT,
            'cash_advance_id'   => $cashAdvanceId,
            'notes'             => $notes,
            'payment_date'      => \Carbon\Carbon::now(),
            'amount_debited'    => $amountDebited,
        ]);
    }




    /**
     * Specify Validator class name
     *
     * @return mixed
     */
    public function validator()
    {
        return "\Sunnydevbox\TWPim\Validators\CashAdvanceLogValidator";
    }

    /**
     * Specify Model class name
     *
     * @return string
     */
    function model()
    {
        return config('tw-pim.model_cash_advance_log');
    }

    public function boot(){
        parent::boot();
    }
    
}

