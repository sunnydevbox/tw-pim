<?php 
namespace Sunnydevbox\TWPim\Repositories\Deduction;

use Sunnydevbox\TWCore\Repositories\TWBaseRepository;

trait CashAdvanceStatusTrait
{
    public function isValidStatus($status)
    {
        if ($this->getStatusConstValue($status)) {
            return true;
        }

        return false;
    }

    public function getStatusConstantName($status)
    {
        return 'STATUS_' . $this->sanitizeStatus($status);
    }

    public function getStatusConstValue($status)
    {
        $refl = new \ReflectionClass($this->model);
        $i = $this->getStatusConstantName($status);
        
        if (isset($refl->getConstants()[$i])) {
            return $refl->getConstants()[$i];
        }

        return null;
    }

    public function sanitizeStatus($status)
    {
        return strtoupper(trim($status));
    }

    public function setStatus($id, $status)
    {
        return $this->update(['status' => $this->getStatusConstValue($status)], $id);
    }

    public function activate($id)
    {
        $this->_setStatus($id, $this->model::STATUS_ACTIVE);
    }

    public function approve($id)
    {
        $this->_setStatus($id, $this->model::STATUS_APPROVED);
    }

    public function disapprove($id)
    {
        $this->_setStatus($id, $this->model::STATUS_DISAPPROVED);
    }

    public function close($id)
    {
        $this->_setStatus($id, $this->model::STATUS_CLOSED);
    }


    public function debit($id)
    {
        // 1) update paid_amount
        $cashAdvance = $this->_getObjectFromId($id);
        $cashAdvance->paid_amount += $cashAdvance->payable_per_period;
        
        if ($cashAdvance->status == $this->model::STATUS_APPROVED) {
            $cashAdvance->status = $this->model::STATUS_ACTIVE;
        }

        $cashAdvance->save();
    }


    private function _setStatus($id, $status)
    {
        $cashAdvance = $this->_getObjectFromId($id);

        $cashAdvance->status = $status;
        $cashAdvance->save();
    }

    public function isActivatable($id, $strict = false)
    {
        $cashAdvance = $id;
        if (!is_object($id)) {
            $cashAdvance = $this->find($id);
        }

        // CA is only ACTIVATABLE 
        // when current status is APPROVED
        if ($cashAdvance->status == $this->model::STATUS_APPROVED) {
            return true;
        }

        if ($cashAdvance->status == $this->model::STATUS_ACTIVE && !$strict) {
            return true;
        }

        return false;
    }

    public function isApprovable($id, $strict = false)
    {
        $cashAdvance = $id;
        if (!is_object($id)) {
            $cashAdvance = $this->find($id);
        }

        // CA is only ACTIVATABLE 
        // when current status is PENDING
        if ($cashAdvance->status == $this->model::STATUS_PENDING) {
            return true;
        }

        if ($cashAdvance->status == $this->model::STATUS_APPROVED && !$strict) {
            return true;
        }

        return false;
    }

    public function isDebitable($id, $strict = false)
    {
        $cashAdvance = $id;
        if (!is_object($id)) {
            $cashAdvance = $this->find($id);
        }

        // CA is only DEBITABLE
        // when current status is APPROVED or ACTIVE
        // ***** AND there is still REMAINING BALANCE to be settled
        // ***** check date
        
        if (
            in_array( $cashAdvance->status, [$this->model::STATUS_APPROVED, $this->model::STATUS_ACTIVE])
            && $cashAdvance->hasStartDatePassed()
            && $cashAdvance->hasBalance()
        ) {
            return true;
        }

        return false;
    }
    
}

