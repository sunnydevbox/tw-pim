<?php 
namespace Sunnydevbox\TWPim\Repositories\Deduction;

use Sunnydevbox\TWCore\Repositories\TWBaseRepository;

class CashAdvanceRepository extends TWBaseRepository
{
    use \Sunnydevbox\TWPim\Repositories\Deduction\CashAdvanceStatusTrait;

    //all, paginate, find, findByField, findWhere, getByCriteria
    protected $cacheOnly = ['all', 'paginate', 'find', 'findByField', 'findWhere', 'getByCriteria'];
    // protected $cacheExcept = [];

    public function getEmployeeCashAdvances($employeeId)
    {
        return $this->findWhere(['employee_id' => $employeeId]);
    }

   
    public function makeDebit($id)
    {
        $cashAdvance = $this->find($id);
    }

    private function _getObjectFromId($id)
    {
        $cashAdvance = $id;
        if (!is_object($id)) {
            $cashAdvance = $this->find($id);
        }

        return $cashAdvance;
    }

    /**
     * Specify Validator class name
     *
     * @return mixed
     */
    public function validator()
    {
        return "\Sunnydevbox\TWPim\Validators\CashAdvanceValidator";
    }

    /**
     * Specify Model class name
     *
     * @return string
     */
    function model()
    {
        return config('tw-pim.model_cash_advance');
    }

    public function boot(){
        parent::boot();
    }
    
}

