<?php 
namespace Sunnydevbox\TWPim\Repositories\Deduction;

use Sunnydevbox\TWCore\Repositories\TWBaseRepository;

class DeductionRepository extends TWBaseRepository
{
    //all, paginate, find, findByField, findWhere, getByCriteria
    protected $cacheOnly = ['all', 'paginate', 'find', 'findByField', 'findWhere', 'getByCriteria'];
    // protected $cacheExcept = [];

    /**
     * Specify Validator class name
     *
     * @return mixed
     */
    public function validator()
    {
        return "\Sunnydevbox\TWPim\Validators\DeductionValidator";
    }

    /**
     * Specify Model class name
     *
     * @return string
     */
    function model()
    {
        return config('tw-pim.model_deduction');
    }

    public function boot(){
        parent::boot();
        $this->pushCriteria(\Sunnydevbox\TWPim\Criteria\ScopeCriteria::class);
    }
    
}

