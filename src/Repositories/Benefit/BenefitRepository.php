<?php
namespace Sunnydevbox\TWPim\Repositories\Benefit;

use Sunnydevbox\TWCore\Repositories\TWBaseRepository;

class BenefitRepository extends TWBaseRepository
{
    //all, paginate, find, findByField, findWhere, getByCriteria
    //protected $cacheOnly = ['all', 'paginate'];
    protected $cacheExcept = [];

    /**
     * Specify Validator class name
     *
     * @return mixed
     */
    public function validator()
    {
        return '\Sunnydevbox\TWPim\Validators\BenefitValidator';
    }

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return '\Sunnydevbox\TWPim\Models\Benefit';
    }
}