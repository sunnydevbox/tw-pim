<?php 
namespace Sunnydevbox\TWPim\Repositories\LeaveApplication;

use Sunnydevbox\TWCore\Repositories\TWBaseRepository;

class LeaveTypeRepository extends TWBaseRepository
{
    //all, paginate, find, findByField, findWhere, getByCriteria
    //protected $cacheOnly = ['all', 'paginate'];
    protected $cacheExcept = [];

    /**
     * Specify Validator class name
     *
     * @return mixed
     */
    public function validator()
    {
        return "\Sunnydevbox\TWPim\Validators\LeaveTypeValidator";
    }

    /**
     * Specify Model class name
     *
     * @return string
     */
    function model()
    {
        return config('tw-pim.models.leave-type');
    }
}

