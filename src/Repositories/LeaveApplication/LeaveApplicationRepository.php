<?php 
namespace Sunnydevbox\TWPim\Repositories\LeaveApplication;

use Sunnydevbox\TWCore\Repositories\TWBaseRepository;

class LeaveApplicationRepository extends TWBaseRepository
{
    //all, paginate, find, findByField, findWhere, getByCriteria
    //protected $cacheOnly = ['all', 'paginate'];
    protected $cacheExcept = [];

    public function setStatus($leaveApplicationId, $action)
    {
        $action = strtolower(trim($action));

        switch ($action) {
            case 'approve': 
                $result = $this->update([
                    'status'    => $this->makeModel()::STATUS_APPROVED,
                    'is_approved' => true
                ], $leaveApplicationId);
            break;

            default:
                // VERIFY FIRST IF ACTION IS VALID
                $const = 'STATUS_' . strtoupper($action);
                $class = get_class($this->makeModel());
                $statusAction = constant("$class::$const");
                
                $result = $this->update([
                    'status' => $statusAction,
                ], $leaveApplicationId);
            break;
        }

        return $result;
    }

    // public function findMatch($employeeId, $start, $end = null)
    // {
    //     $query = $this->makeModel()
    //         ->findMatch($start, $end)
    //         ->setEmployee($employeeId)
    //         ;

    //     dd($query->toSql(), $query->getBindings());

    //     return $query->get();
    // }


    /**
     * Specify Validator class name
     *
     * @return mixed
     */
    public function validator()
    {
        return "\Sunnydevbox\TWPim\Validators\LeaveApplicationValidator";
    }

    /**
     * Specify Model class name
     *
     * @return string
     */
    function model()
    {
        return config('tw-pim.models.leave-application');
    }

    public function boot()
    {
        parent::boot();

        $this->pushCriteria(\Sunnydevbox\TWPim\Criteria\QueryLeaveApplication::class);
    }
}

