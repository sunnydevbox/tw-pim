<?php
namespace Sunnydevbox\TWPim\Repositories\Table;

use Sunnydevbox\TWCore\Repositories\TWBaseRepository;

class TaxRepository extends TWBaseRepository
{
    protected $fieldSearchable = [
        'compensation_level_period',
        'salary_range_from',
        'salary_range_to',
        'withholding_tax',
        'withholding_percentage',
    ];


    public function getRange($period, $salary)
    {

        return $this->makeModel()->getRange($period, $salary)->first();
    }

    public function model()
    {
        return config('tw-pim.model_table_tax');
    }

    public function boot()
    {
        parent::boot();

        $this->pushCriteria(\Sunnydevbox\TWPim\Criteria\TaxTableCriteria::class);
    }
}
