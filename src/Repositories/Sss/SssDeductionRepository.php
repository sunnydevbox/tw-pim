<?php
namespace Sunnydevbox\TWPim\Repositories\Sss;

use Sunnydevbox\TWCore\Repositories\TWBaseRepository;

class SssDeductionRepository extends TWBaseRepository
{
    protected $fieldSearchable = [
        'email'
    ];


    public function getRange($salary)
    {
        return $this->makeModel()->getRange($salary)->first();
    }

    public function model()
    {
        return \Sunnydevbox\TWPim\Models\SssDeduction::class;
    }
}
