<?php
namespace Sunnydevbox\TWPim\Repositories\ShiftTemplate;

use Sunnydevbox\TWCore\Repositories\TWBaseRepository;

class ShiftTemplateRepository extends TWBaseRepository
{
    public function validator()
    {
        return \Sunnydevbox\TWPim\Validators\ShiftTemplateValidator::class;
    }

    function model()
    {
        return \Sunnydevbox\TWPim\Models\ShiftTemplate::class;
    }
}
