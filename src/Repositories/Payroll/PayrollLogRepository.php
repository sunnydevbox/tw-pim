<?php 
namespace Sunnydevbox\TWPim\Repositories\Payroll;

use Sunnydevbox\TWCore\Repositories\TWBaseRepository;

class PayrollLogRepository extends TWBaseRepository
{
    //all, paginate, find, findByField, findWhere, getByCriteria
    //protected $cacheOnly = ['all', 'paginate'];
    protected $cacheExcept = [];

    protected $fieldSearchable = [
        'id',
        'payroll_template_id',
        'status',
    ];

    public function init($payrollTemplateId)
    {
        return $this->fristOrCreate([
            'payroll_template_id' => $payrollTemplateId,
            'status' => $this->makeModel()::STATUS_INIT,
        ]);
    }
    

    public function setStatus($payrollLogId, $action)
    {
        // $this->find($payrollLogId)
        //     // ->
    }



    /**
     * Specify Validator class name
     *
     * @return mixed
     */
    public function validator()
    {
        return "\Sunnydevbox\TWPim\Validators\PayrollLogValidator";
    }

    /**
     * Specify Model class name
     *
     * @return string
     */
    function model()
    {
        // dd(config('tw-pim.models'));
        return config('tw-pim.models.payroll-log');
    }

    public function boot()
    {   
        parent::boot();
        $this->pushCriteria(\Sunnydevbox\TWPim\Criteria\PayrollLogCriteria::class);
    }
}

