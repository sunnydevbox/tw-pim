<?php 
namespace Sunnydevbox\TWPim\Repositories\Payroll;

use Sunnydevbox\TWCore\Repositories\TWBaseRepository;

class PayrollRepository extends TWBaseRepository
{
    //all, paginate, find, findByField, findWhere, getByCriteria
    //protected $cacheOnly = ['all', 'paginate'];
    protected $cacheExcept = [];

    protected $fieldSearchable = [
        'id',
        'payroll_log_id',
        'employee_id',
        'company_id',
        'name',
    ];


    public function init($payrollLog, $employee)
    {
        $object = $this->firstOrNew([
            'employee_id'   => $employee->id,
            'payroll_log_id' => $payrollLog->id
        ]);

        if (!$object->id) {
            $jobPosition = (!$employee->job_position)? null : $employee->job_position->name;
            $department = (!$employee->department)? null : $employee->department->name;

            $object->fill([
                'name' => $employee->name,
                'job_position' => $jobPosition,
                'department' => $department,
                'status' => $this->makeModel()::STATUS_INIT,
            ])->save();
        }
        return $object;

        return $this->create([
            'employee_id' => $employee->id,
            'name' => $employee->name,
            'job_position' => $employee->job_position->name,
            'department' => $employee->department->name,
            'status' => $this->makeModel()::STATUS_INIT,
        ]);
    }

    public function exists($employeeId, $payrollLogId)
    {
        return $this->findWhere([
            'employee_id' => $employeeId,
            'payroll_log_id' => $payrollLogId
        ])->count() ? true : false; 
    }


    /**
     * Specify Validator class name
     *
     * @return mixed
     */
    public function validator()
    {
        return "\Sunnydevbox\TWPim\Validators\PayrollValidator";
    }

    /**
     * Specify Model class name
     *
     * @return string
     */
    function model()
    {
        // dd(config('tw-pim.models'));
        return config('tw-pim.model_payroll');
    }

    public function boot()
    {   
        parent::boot();
        $this->pushCriteria(\Sunnydevbox\TWPim\Criteria\PayrollCriteria::class);
    }
}

