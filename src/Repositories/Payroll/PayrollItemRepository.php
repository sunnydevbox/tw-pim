<?php 
namespace Sunnydevbox\TWPim\Repositories\Payroll;

use Sunnydevbox\TWCore\Repositories\TWBaseRepository;

class PayrollItemRepository extends TWBaseRepository
{
    //all, paginate, find, findByField, findWhere, getByCriteria
    //protected $cacheOnly = ['all', 'paginate'];
    protected $cacheExcept = [];

    /**
     * Specify Validator class name
     *
     * @return mixed
     */
    public function validator()
    {
        return "\Sunnydevbox\TWPim\Validators\PayrollItemValidator";
    }

    /**
     * Specify Model class name
     *
     * @return string
     */
    function model()
    {
        return config('tw-pim.models.payroll-item');
    }

    public function boot()
    {   
        parent::boot();
    }
}

