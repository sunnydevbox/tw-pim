<?php
namespace Sunnydevbox\TWPim\Repositories\Payroll;
use Carbon\Carbon;

trait PayrollTemplatePeriodParserTrait
{
    public function generateSelectablePeriods($templateId = null, $month = null, $year = null)
    {
        if ($templateId && $template = $this->find($templateId)) {
            
            $periods = [];
            foreach($template->month_days as $month_days) {
                $set = explode('-', $month_days);
                
                $start  = $end = null;

                if (is_numeric($set[0])) {
                    $start = Carbon::create($year, $month, $set[0]);
                } else {
                    $start = $this->validateStringDay($set[0], $year, $month)->subMonth();
                }

                if (is_numeric($set[1])) {
                    $end = Carbon::create($year, $month, $set[1]);
                } else {
                    $end = $this->validateStringDay($set[1], $year, $month);
                }

                if (is_numeric($set[0]) && is_numeric($set[1])) {
                    // This means $set[0] refers to a date from previous month
                    if ($set[0] > $set[1]) {
                        $start = $start->subMonth(1);
                    }
                } 

                $periods[] = [
                    $start->startOfDay(), $end->endOfDay()
                ];
            }

            return $periods;
        }
    }

    public static function validateStringDay($day, $year = null, $month = null)
    {
        $day = strtolower($day);
        

        if ($month && $year) {
            switch($day) {
                case 'last_day':
                    return Carbon::create($year, $month, 1)->endOfMonth();
                break;

                case 'day_before_last_day':
                    return Carbon::create($year, $month, 1)->endOfMonth()->subDay();
                break;

                default: 
                    return null;
                break;
            }
        }
    }
}