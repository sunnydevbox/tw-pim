<?php 
namespace Sunnydevbox\TWPim\Repositories\Payroll;

use Sunnydevbox\TWCore\Repositories\TWBaseRepository;
use Carbon\Carbon;
use Sunnydevbox\TWPim\Repositories\Payroll\PayrollTemplatePeriodParserTrait;

class PayrollTemplateRepository extends TWBaseRepository
{
    use PayrollTemplatePeriodParserTrait;

    //all, paginate, find, findByField, findWhere, getByCriteria
    //protected $cacheOnly = ['all', 'paginate'];
    protected $cacheExcept = [];



    



    /**
     * Specify Validator class name
     *
     * @return mixed
     */
    public function validator()
    {
        return "\Sunnydevbox\TWPim\Validators\PayrollValidator";
    }

    /**
     * Specify Model class name
     *
     * @return string
     */
    function model()
    {
        // dd(config('tw-pim.models'));
        return config('tw-pim.models.payroll-template');
    }

    public function boot()
    {   
        parent::boot();
    }
}

