<?php 
namespace Sunnydevbox\TWPim\Repositories\CivilStatus;

use Sunnydevbox\TWCore\Repositories\TWBaseRepository;

class CivilStatusRepository extends TWBaseRepository
{
    //all, paginate, find, findByField, findWhere, getByCriteria
    //protected $cacheOnly = ['all', 'paginate'];
    protected $cacheExcept = [];

    /**
     * Specify Validator class name
     *
     * @return mixed
     */
    public function validator()
    {
        return "\Sunnydevbox\TWPim\Validators\CivilStatusValidator";
    }

    /**
     * Specify Model class name
     *
     * @return string
     */
    function model()
    {
        return config('tw-pim.model_civil_status');
    }
}

