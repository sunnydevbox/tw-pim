<?php 
namespace Sunnydevbox\TWPim\Repositories\SystemVariable;

use Sunnydevbox\TWCore\Repositories\TWBaseRepository;

class SystemVariableRepository extends TWBaseRepository
{
    //all, paginate, find, findByField, findWhere, getByCriteria
    //protected $cacheOnly = ['all', 'paginate'];
    protected $cacheExcept = [];

    public function var($key, $dafaultValue = null)
    {
        $result = $this->find($key);

        if ($result) {
            return $result->value;
        }

        return $defaltValue;
    }


    /**
     * Specify Validator class name
     *
     * @return mixed
     */
    public function validator()
    {
        return "\Sunnydevbox\TWPim\Validators\SystemVariableValidator";
    }

    /**
     * Specify Model class name
     *
     * @return string
     */
    function model()
    {
        return config('tw-pim.models.system-variable');
    }
}

