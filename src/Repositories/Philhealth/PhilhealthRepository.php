<?php
namespace Sunnydevbox\TWPim\Repositories\Philhealth;

use Sunnydevbox\TWCore\Repositories\TWBaseRepository;

class PhilhealthRepository extends TWBaseRepository
{
    protected $fieldSearchable = [];


    public function getRange($salary)
    {
        $q = $this->makeModel()->getRange($salary);

        return $q->first();
    }

    public function model()
    {
        return config('tw-pim.model_philhealth');
    }
}
