<?php 
namespace Sunnydevbox\TWPim\Repositories\Timelog;

use Sunnydevbox\TWCore\Repositories\TWBaseRepository;
use Sunnydevbox\TWPim\Traits\HasTimelogCalculationTrait;

class TimelogImportRepository extends TWBaseRepository
{
    //all, paginate, find, findByField, findWhere, getByCriteria
    //protected $cacheOnly = ['all', 'paginate'];
    protected $cacheExcept = [];


    public function setVerified()
    {
        
    }


    /**
     * Specify Validator class name
     *
     * @return mixed
     */
    public function validator()
    {
        return "\Sunnydevbox\TWPim\Validators\TimelogValidator";
    }

    /**
     * Specify Model class name
     *
     * @return string
     */
    function model()
    {
        return config('tw-pim.models.timelog-import');
    }
}

