<?php 
namespace Sunnydevbox\TWPim\Repositories\Timelog;

use Sunnydevbox\TWCore\Repositories\TWBaseRepository;
use Sunnydevbox\TWPim\Core\Timelog\CoreTimelogCalculationTrait;

class TimelogRepository extends TWBaseRepository
{
    use CoreTimelogCalculationTrait;

    //all, paginate, find, findByField, findWhere, getByCriteria
    //protected $cacheOnly = ['all', 'paginate'];
    protected $cacheExcept = [];

    protected $fieldSearchable = [
        'id', 'employee_id',
    ];


    public function setStatus($timelogId, $action)
    {
        $action = strtolower(trim($action));
        $timelog = $this->find($timelogId);

        switch ($action) {
            case 'approve': 
            case 'approved':
                $result = $timelog->fill([
                    'status'    => $this->makeModel()::STATUS_APPROVED,
                    'is_approved' => true
                ])->save();
            break;

            default:
                // VERIFY FIRST IF ACTION IS VALID
                $const = 'STATUS_' . strtoupper($action);
                $class = get_class($this->makeModel());
                $statusAction = constant("$class::$const");

                $result = $timelog->fill([
                    'status'    => $statusAction,
                ])->save();
            break;
        }

        return $result;
    }



    /**
     * Specify Validator class name
     *
     * @return mixed
     */
    public function validator()
    {
        return "\Sunnydevbox\TWPim\Validators\TimelogValidator";
    }

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return config('tw-pim.models.timelog');
    }

    public function boot()
    {
        parent::boot();

        $this->pushCriteria(\Sunnydevbox\TWPim\Criteria\QueryTimelogCriteria::class);
    }
}

