<?php 
namespace Sunnydevbox\TWPim\Repositories\JobPosition;

use Sunnydevbox\TWCore\Repositories\TWBaseRepository;

class JobPositionRepository extends TWBaseRepository
{
    //all, paginate, find, findByField, findWhere, getByCriteria
    //protected $cacheOnly = ['all', 'paginate'];
    protected $cacheExcept = [];

    /**
     * Specify Validator class name
     *
     * @return mixed
     */
    public function validator()
    {
        return "\Sunnydevbox\TWPim\Validators\JobPositionValidator";
    }

    /**
     * Specify Model class name
     *
     * @return string
     */
    function model()
    {
        return '\Sunnydevbox\TWPim\Models\JobPosition';
    }
}

