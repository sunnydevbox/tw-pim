<?php
namespace Sunnydevbox\TWPim\Repositories\ShiftTemplatePeriod;

use Sunnydevbox\TWCore\Repositories\TWBaseRepository;

class ShiftTemplatePeriodRepository extends TWBaseRepository
{
    public function validator()
    {
        return \Sunnydevbox\TWPim\Validators\ShiftTemplatePeriodValidator::class;
    }

    function model()
    {
        return \Sunnydevbox\TWPim\Models\ShiftTemplatePeriod::class;
    }
}
